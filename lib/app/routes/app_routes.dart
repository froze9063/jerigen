part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const SPLASH_SCREEN = '/splash-screen';
  static const CHOOSE_LOCATION = "/choose_location";
  static const BOARDING = '/boarding';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const AUTHENTICATION = '/authentication';
  static const DASHBOARD = '/dashboard';
  static const SCANNER = '/scanner';
  static const SCAN_INPUT = '/scan-input';
  static const SCAN_SUBMISSION = '/scan-submission';
  static const PICKUP_INPUT = '/trx-input';
  static const PICKUP_FINDING = '/trx-finding';
  static const PICKUP_FOUND = '/trx-found';
  static const PICKUP_TRANSPORTER = '/trx-transporter';
  static const PICKUP_DELIVERED = '/trx-delivered';
  static const SEARCH_DEPO = '/search-depo';
  static const ADD_ADDRESS = '/add-address';
  static const PROFILE = '/profile';
  static const ACTIVITY = '/activity';
  static const POINTS = '/points';
  static const MESSAGES = '/messages';
  static const TODAY_PRICE = '/today-price';
  static const SETTING_NOTIFICATION = '/setting-notification';
  static const SETTING_TNC = '/setting-tnc';
  static const SETTING_PROFILE = '/setting-profile';
  static const INFORMATION_DETAIL = '/information-detail';
  static const POINT_NEW = '/point-new';
  static const ACCOUNT_NUMBER = '/account-number';
}
