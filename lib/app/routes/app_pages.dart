import 'package:get/get.dart';

import 'package:jerrycan_master/app/modules/authentication/bindings/authentication_binding.dart';
import 'package:jerrycan_master/app/modules/authentication/views/authentication_view.dart';
import 'package:jerrycan_master/app/modules/boarding/bindings/boarding_binding.dart';
import 'package:jerrycan_master/app/modules/boarding/views/boarding_view.dart';
import 'package:jerrycan_master/app/modules/choose_location/bindings/choose_location_binding.dart';
import 'package:jerrycan_master/app/modules/choose_location/views/choose_location_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/dashboard_screen.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/scan/scan_input/bindings/scan_input_binding.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/scan/scan_input/views/scan_input_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/scan/scan_submission/bindings/scan_submission_binding.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/scan/scan_submission/views/scan_submission_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/scan/scanner_view.dart';
import 'package:jerrycan_master/app/modules/login/bindings/login_binding.dart';
import 'package:jerrycan_master/app/modules/login/views/login_view.dart';
import 'package:jerrycan_master/app/modules/register/bindings/register_binding.dart';
import 'package:jerrycan_master/app/modules/register/views/register_view.dart';
import 'package:jerrycan_master/app/modules/splash_screen/bindings/splash_screen_binding.dart';
import 'package:jerrycan_master/app/modules/splash_screen/views/splash_screen_view.dart';

import '../modules/dashboard/activity/bindings/activity_binding.dart';
import '../modules/dashboard/activity/views/activity_view.dart';
import '../modules/dashboard/add_address/bindings/add_address_binding.dart';
import '../modules/dashboard/add_address/views/add_address_view.dart';
import '../modules/dashboard/home/features/information_detail/bindings/information_detail_binding.dart';
import '../modules/dashboard/home/features/information_detail/views/information_detail_view.dart';
import '../modules/dashboard/home/features/pickup/pickup_delivered/bindings/pickup_delivered_binding.dart';
import '../modules/dashboard/home/features/pickup/pickup_delivered/views/pickup_delivered_view.dart';
import '../modules/dashboard/home/features/pickup/pickup_finding/bindings/pickup_finding_binding.dart';
import '../modules/dashboard/home/features/pickup/pickup_finding/views/pickup_finding_view.dart';
import '../modules/dashboard/home/features/pickup/pickup_found/bindings/pickup_found_binding.dart';
import '../modules/dashboard/home/features/pickup/pickup_found/views/pickup_found_view.dart';
import '../modules/dashboard/home/features/pickup/pickup_input/bindings/pickup_input_binding.dart';
import '../modules/dashboard/home/features/pickup/pickup_input/views/pickup_input_view.dart';
import '../modules/dashboard/home/features/pickup/pickup_transporter/bindings/pickup_transporter_binding.dart';
import '../modules/dashboard/home/features/pickup/pickup_transporter/views/pickup_transporter_view.dart';
import '../modules/dashboard/home/features/search_depo/bindings/search_depo_binding.dart';
import '../modules/dashboard/home/features/search_depo/views/search_depo_view.dart';
import '../modules/dashboard/home/features/today_price/bindings/today_price_binding.dart';
import '../modules/dashboard/home/features/today_price/views/today_price_view.dart';
import '../modules/dashboard/messages/bindings/messages_binding.dart';
import '../modules/dashboard/messages/views/messages_view.dart';
import '../modules/dashboard/points/bindings/points_binding.dart';
import '../modules/dashboard/points/features/account_number/bindings/account_number_binding.dart';
import '../modules/dashboard/points/features/account_number/views/account_number_view.dart';
import '../modules/dashboard/points/features/point_new/bindings/point_new_binding.dart';
import '../modules/dashboard/points/features/point_new/views/point_new_view.dart';
import '../modules/dashboard/points/views/points_view.dart';
import '../modules/dashboard/profile/bindings/profile_binding.dart';
import '../modules/dashboard/profile/features/setting_notification/bindings/setting_notification_binding.dart';
import '../modules/dashboard/profile/features/setting_notification/views/setting_notification_view.dart';
import '../modules/dashboard/profile/features/setting_profile/bindings/setting_profile_binding.dart';
import '../modules/dashboard/profile/features/setting_profile/views/setting_profile_view.dart';
import '../modules/dashboard/profile/features/setting_tnc/bindings/setting_tnc_binding.dart';
import '../modules/dashboard/profile/features/setting_tnc/views/setting_tnc_view.dart';
import '../modules/dashboard/profile/views/profile_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH_SCREEN;

  static final routes = [
    GetPage(
      name: Routes.SPLASH_SCREEN,
      page: () => SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: Routes.DASHBOARD,
      page: () => DashboardScreen(),
    ),
    GetPage(
      name: Routes.AUTHENTICATION,
      page: () => AuthenticationView(),
      binding: AuthenticationBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.REGISTER,
      page: () => RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: Routes.BOARDING,
      page: () => BoardingView(),
      binding: BoardingBinding(),
    ),
    GetPage(
      name: Routes.CHOOSE_LOCATION,
      page: () => ChooseLocationView(),
      binding: ChooseLocationBinding(),
    ),
    GetPage(name: Routes.SCANNER, page: () => ScannerView()),
    GetPage(
      name: Routes.SCAN_INPUT,
      page: () => ScanInputView(),
      binding: ScanInputBinding(),
    ),
    GetPage(
      name: Routes.SCAN_SUBMISSION,
      page: () => ScanSubmissionView(),
      binding: ScanSubmissionBinding(),
    ),
    GetPage(
      name: Routes.PICKUP_INPUT,
      page: () => PickupInputView(),
      binding: PickupInputBinding(),
    ),
    GetPage(
      name: Routes.PICKUP_FINDING,
      page: () => PickupFindingView(),
      binding: PickupFindingBinding(),
    ),
    GetPage(
      name: Routes.PICKUP_FOUND,
      page: () => PickupFoundView(),
      binding: PickupFoundBinding(),
    ),
    GetPage(
      name: Routes.PICKUP_TRANSPORTER,
      page: () => PickupTransporterView(),
      binding: PickupTransporterBinding(),
    ),
    GetPage(
      name: Routes.PICKUP_DELIVERED,
      page: () => PickupDeliveredView(),
      binding: PickupDeliveredBinding(),
    ),
    GetPage(
      name: Routes.SEARCH_DEPO,
      page: () => SearchDepoView(),
      binding: SearchDepoBinding(),
    ),
    GetPage(
      name: Routes.ADD_ADDRESS,
      page: () => AddAddressView(),
      binding: AddAddressBinding(),
    ),
    GetPage(
      name: Routes.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: Routes.ACTIVITY,
      page: () => ActivityView(),
      binding: ActivityBinding(),
    ),
    GetPage(
      name: Routes.POINTS,
      page: () => PointsView(),
      binding: PointsBinding(),
    ),
    GetPage(
      name: Routes.MESSAGES,
      page: () => MessagesView(),
      binding: MessagesBinding(),
    ),
    GetPage(
      name: Routes.TODAY_PRICE,
      page: () => TodayPriceView(),
      binding: TodayPriceBinding(),
    ),
    GetPage(
      name: Routes.SETTING_NOTIFICATION,
      page: () => SettingNotificationView(),
      binding: SettingNotificationBinding(),
    ),
    GetPage(
      name: Routes.SETTING_TNC,
      page: () => SettingTncView(),
      binding: SettingTncBinding(),
    ),
    GetPage(
      name: Routes.SETTING_PROFILE,
      page: () => SettingProfileView(),
      binding: SettingProfileBinding(),
    ),
    GetPage(
      name: Routes.INFORMATION_DETAIL,
      page: () => InformationDetailView(),
      binding: InformationDetailBinding(),
    ),
    GetPage(
      name: Routes.POINT_NEW,
      page: () => PointNewView(),
      binding: PointNewBinding(),
    ),
    GetPage(
      name: Routes.ACCOUNT_NUMBER,
      page: () => AccountNumberView(),
      binding: AccountNumberBinding(),
    ),
  ];
}
