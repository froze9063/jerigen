// Created by ferdyhaspin on 03/12/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';

import '../utils/constant.dart';

final String typeAdd = "ADD";
final String typeMinus = "MINUS";

FocusScope defaultForm(GlobalKey controller, List<Widget> children) {
  return FocusScope(
    debugLabel: 'Scope',
    autofocus: true,
    child: Form(
      key: controller,
      child: Column(
        children: children,
      ),
    ),
  );
}

Focus defaultTextFormField(
  String hint,
  TextEditingController controller,
  ValueChanged<String>? onChanged,
  FocusNode? _focusNode,
  FocusNode? nextFocus, {
  TextInputType inputType = TextInputType.text,
  TextInputAction action = TextInputAction.next,
  bool obscureText = false,
}) {
  return Focus(
    debugLabel: 'hint',
    child: Builder(
      builder: (BuildContext context) {
        final FocusNode focusNode = Focus.of(context);
        final bool hasFocus = focusNode.hasFocus;

        return Container(
          height: 45,
          child: TextFormField(
            obscureText: obscureText,
            keyboardType: inputType,
            textInputAction: action,
            controller: controller,
            onChanged: onChanged,
            focusNode: _focusNode,
            onFieldSubmitted: (value) {
              if (nextFocus != null)
                FocusScope.of(context).requestFocus(nextFocus);
            },
            decoration: defaultInputDecoration(hint, hasFocus: hasFocus),
          ),
        );
      },
    ),
  );
}

InputDecoration defaultInputDecoration(
  String hint, {
  bool hasFocus = false,
}) {
  return InputDecoration(
    labelText: hint,
    labelStyle: TextStyle(
      color: Colors.grey,
      fontSize: 16,
    ),
    fillColor: hasFocus ? Colors.white : COLOR_BORDER.withOpacity(0.7),
    filled: true,
    border: OutlineInputBorder(
      borderSide: BorderSide(color: COLOR_BORDER),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: COLOR_PRIMARY_DARK),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
  );
}

Widget generalCalculateTextForm(
  TextEditingController controller,
  ValueChanged<String>? onChanged, {
  bool donate = false,
  double fontSize = 14,
  FontWeight fontWeight = FontWeight.w500,
}) {
  double width = donate ? 70 : 50;
  return Row(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      ConstrainedBox(
        constraints: BoxConstraints(minWidth: width),
        child: IntrinsicWidth(
          child: TextFormField(
            maxLength: donate ? 8 : 5,
            // maxLengthEnforcement: MaxLengthEnforcement.,
            textAlign: TextAlign.center,
            textAlignVertical: TextAlignVertical.center,
            style: TextStyle(
              fontSize: fontSize,
              fontWeight: FontWeight.w500,
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            controller: controller,
            onChanged: onChanged,
            decoration: InputDecoration(
              counterText: "",
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
      ),
      if (!donate)
        Text(
          'Kg',
          style: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
    ],
  );
}

void toEndCursor(TextEditingController controller) {
  controller.selection = TextSelection.fromPosition(
    TextPosition(offset: controller.text.length),
  );
}

void unFocus(BuildContext context) {
  FocusScope.of(context).unfocus();
}

Text defaultText(
    String text, {
      double size = 16,
      FontWeight weight = FontWeight.w600,
      Color color = COLOR_PRIMARY_TEXT,
      TextAlign textAlign: TextAlign.start,
    }) {
  return Text(
    text,
    textAlign: textAlign,
    style: TextStyle(
      fontSize: size,
      fontWeight: weight,
      color: color,
    ),
  );
}