// Created by ferdyhaspin on 26/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';

class ProgressLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Container(
            margin: EdgeInsets.all(5),
            child: CircularProgressIndicator(
              strokeWidth: 3.0,
            ),
          ),
        ),
      ],
    );
  }
}

class ProgressLoadingWithValue extends StatelessWidget {
  final double? value;

  const ProgressLoadingWithValue({Key? key, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Container(
            margin: EdgeInsets.all(5),
            child: CircularProgressIndicator(
              value: value,
              strokeWidth: 3.0,
            ),
          ),
        ),
      ],
    );
  }
}
