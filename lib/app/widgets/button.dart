// Created by ferdyhaspin on 09/03/21.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class TextButtonPrimary extends StatelessWidget {
  final bool isEnable;
  final String text;
  final dynamic onPressed;

  const TextButtonPrimary({
    Key? key,
    required this.isEnable,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      child: TextButton(
        child: Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        style: isEnable
            ? TextButton.styleFrom(
                backgroundColor: COLOR_PRIMARY,
                enableFeedback: true,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              )
            : TextButton.styleFrom(
                backgroundColor: Colors.white,
                enableFeedback: true,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                  side: BorderSide(width: 1, color: Colors.black),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
        onPressed: isEnable ? onPressed : null,
      ),
    );
  }
}

class TextButtonGrey extends StatelessWidget {
  final String text;
  final dynamic onPressed;

  const TextButtonGrey({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      child: TextButton(
        child: Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          shape: RoundedRectangleBorder(
            side: BorderSide(width: 1, color: Colors.black),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class TextButtonPrimaryWrap extends StatelessWidget {
  final bool isEnable;
  final String text;
  final dynamic onPressed;

  const TextButtonPrimaryWrap({
    Key? key,
    required this.isEnable,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        text,
        style: TextStyle(
          color: Colors.black,
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      style: isEnable
          ? TextButton.styleFrom(
              backgroundColor: COLOR_PRIMARY,
              enableFeedback: true,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            )
          : TextButton.styleFrom(
              backgroundColor: Colors.white,
              enableFeedback: true,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              shape: RoundedRectangleBorder(
                side: BorderSide(width: 1, color: Colors.black),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
      onPressed: isEnable ? onPressed : null,
    );
  }
}

class TextButtonGreyWrap extends StatelessWidget {
  final String text;
  final dynamic onPressed;

  const TextButtonGreyWrap({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        text,
        style: TextStyle(
          color: Colors.black,
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      style: TextButton.styleFrom(
        backgroundColor: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: Colors.black),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      onPressed: onPressed,
    );
  }
}

class TextButtonWhiteWrap extends StatelessWidget {
  final String text;
  final dynamic onPressed;

  const TextButtonWhiteWrap({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        text,
        style: TextStyle(
          color: COLOR_PRIMARY_TEXT,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      style: TextButton.styleFrom(
        backgroundColor: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: COLOR_PRIMARY),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      onPressed: onPressed,
    );
  }
}
