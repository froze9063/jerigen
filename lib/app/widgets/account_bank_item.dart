// Created by ferdyhaspin on 25/01/22.
// Copyright (c) 2022 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class AccountBankItem extends StatelessWidget {
  final String title;
  final String label;
  final String type;
  final Function(String) callback;

  const AccountBankItem({
    Key? key,
    required this.title,
    required this.label,
    required this.callback,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8),
          child: InkWell(
            onTap: () {
              Get.back();
              callback(type);
            },
            child: TextFormField(
              // expands: true,
              maxLines: 3,
              minLines: 1,
              initialValue: label,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(16),
                enabled: false,
                labelText: title,
                labelStyle: TextStyle(color: COLOR_PRIMARY_DARK, fontSize: 12),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: COLOR_PRIMARY_DARK),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 0,
          top: 0,
          bottom: 0,
          child: IconButton(
              onPressed: () {
                Get.back();
                callback(ACCOUNT_BANK_EDIT);
              },
              icon: Image.asset(
                "assets/icons/ic_edit.png",
                width: 28,
              )),
        ),
      ],
    );
  }
}
