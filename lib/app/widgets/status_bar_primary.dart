// Created by ferdyhaspin on 26/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../utils/constant.dart';

class StatusBarPrimary extends StatelessWidget {
  final Widget child;

  const StatusBarPrimary({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      key: Key('my-widget-key'),
      onVisibilityChanged: (visibilityInfo) {
        num visiblePercentage = visibilityInfo.visibleFraction * 100;
        debugPrint(
            'Widget ${visibilityInfo.key} is ${visiblePercentage}% visible');
        if (visiblePercentage == 100) {
          SystemChrome.setSystemUIOverlayStyle(
            SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: COLOR_PRIMARY,
            ),
          );
        } else {
          SystemChrome.setSystemUIOverlayStyle(
            SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: Colors.white,
            ),
          );
        }
      },
      child: child,
    );
  }
}
