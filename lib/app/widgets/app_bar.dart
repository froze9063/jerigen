// Created by ferdyhaspin on 24/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

AppBar defaultAppBar(String title, callback, {double elevation = 0}) {
  if (callback != null) {
    return AppBar(
      elevation: elevation,
      leading: GestureDetector(
        child: Center(
          child: Image.asset("assets/icons/ic_left_back.png", width: 28),
        ),
        onTap: callback,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: COLOR_PRIMARY_TEXT,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  } else {
    return AppBar(
      elevation: elevation,
      title: Text(
        title,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: COLOR_PRIMARY_TEXT,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
    );
  }
}
