// Created by ferdyhaspin on 21/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';

class ComingSoonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'Coming Soon...',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
