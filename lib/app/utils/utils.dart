// Created by ferdyhaspin on 24/11/21. 
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:intl/intl.dart';

class Utils{

  static String toIDR(data){
    final currencyFormatter = NumberFormat('#,##0', 'ID');
    return currencyFormatter.format(data);
  }
}