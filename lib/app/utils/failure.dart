abstract class Failure {
  String? code;
}

class RequestException extends Failure {
  final code;

  RequestException(this.code);
}
