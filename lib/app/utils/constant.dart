// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

const COLOR_PRIMARY = Color(0xFF5CE1E6);
const COLOR_PRIMARY_DARK = Color(0xFF44A3A7);
const COLOR_PRIMARY_EXTRA_DARK = Color(0xFF2C3F58);
const COLOR_PRIMARY_LIGHT = Color(0xFF90E2FF);

const COLOR_PRIMARY_DARK_TEXT = Color(0xFF2C3F58);
const COLOR_PRIMARY_EXTRA_DARK_TEXT = Color(0xFF2A2C32);
const COLOR_PRIMARY_TEXT = Color(0xFF646F7E);
const COLOR_SECONDARY_TEXT = Color(0xFF4A5768);

const COLOR_INDICATOR = Color(0xFF200E32);
const COLOR_BORDER = Color(0xFFECECEC);
const COLOR_DANGER = Color(0xFFFF7373);

//CONFIG REQUEST

const String BASE_URL = "https://jerigen-api.herokuapp.com/";
const String FCM_HOST = "https://fcm.googleapis.com/";
const CONNECT_TIME_OUT = 30000;
const READ_TIME_OUT = 30000;
const FILE_SIZE_LIMIT = 2000000;
const FCM_SERVER_KEY =
    "key=AAAAI165sMA:APA91bEtA-4BQgAQIdUytFFtlqSK2PnQaxq-tW12TIrd3eZU-JXeBPPR0tBK35PRND6atxQCc991I3XI7Cbv6Nhb_oK9nsPwS_0jvmKL8Ve4oIaexjwp8006Jj7kdaDZUUFFZ_TE5TqN";

const DEFAULT_ERROR = "Permintaan tidak dapat di proses,\nTerjadi kesalahan.";
const ERROR_TIMEOUT = "Permintaan kehabisan waktu.";
const RESPONSE_OK = 200;

//PATH
const PATH_CREATE_USER = "user/create";
const PATH_UPDATE_USER = "/user/update/";
const PATH_LOGIN = "/auth/login";
const PATH_REFRESH_TOKEN = "/auth/refresh-token";
const PATH_LIST_CITY = "/master/city/list";
const PATH_LIST_TRANSPORTER = "/services/transportation/list";
const PATH_LIST_DEPO = "/services/depo/list";
const PATH_SCAN_DEPO = "/services/depo/scan-code/";
const PATH_CALCULATE_POINT = "/services/transaction/calculate-points";
const PATH_CREATE_TRX = "/services/transaction/create-transaction";
const PATH_MEDIA_UPLOAD_FILE = "/media/upload-file";
const PATH_BANK_ACCOUNT_CREATE = "/master/bank-account/create";
const PATH_BANK_ACCOUNT_UPDATE = "/master/bank-account/update/";
const PATH_BANK_ACCOUNT_LIST = "/master/bank-account/list";
const PATH_ADDRESS_CREATE = "/master/address/create";
const PATH_ADDRESS_UPDATE = "/master/address/update/";
const PATH_ADDRESS_LIST = "/master/address/list";

//SHARED PREFERENCES
const PREFS_LOGIN_DATA = "prefs_login_data";
const PREFS_LOCATION = "prefs_location";
const PREFS_DO_SHARE_REFERRAL = "prefs_do_share_referral";

//ACCOUNT BANK TYPE CALLBACK
const ACCOUNT_BANK_CHOOSE = "choose";
const ACCOUNT_BANK_EDIT = "edit";

class ConstConfig {
  static PrettyDioLogger dioPrettyLog() {
    return PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90);
  }

  static BaseOptions dioBaseOptions() {
    return BaseOptions(
      baseUrl: BASE_URL,
      connectTimeout: CONNECT_TIME_OUT,
      receiveTimeout: READ_TIME_OUT,
    );
  }

  static BaseOptions dioFCMBaseOptions() {
    return BaseOptions(
      baseUrl: FCM_HOST,
      connectTimeout: CONNECT_TIME_OUT,
      receiveTimeout: READ_TIME_OUT,
    );
  }
}
