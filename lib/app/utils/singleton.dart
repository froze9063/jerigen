// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/auth/login_response.dart';
import 'package:jerrycan_master/app/data/model/choose_location/choose_location_response.dart';
import 'package:jerrycan_master/app/data/model/master/address.dart';

import '../data/model/base_response.dart';
import '../data/repository/master_repository.dart';
import '../widgets/custom_toast.dart';
import 'constant.dart';
import 'failure.dart';

class Singleton {
  static late LoginResponse loginResponse;
  static late ChooseListLocation location;

  static List<Address> addresses = [];

  static final masterRepository = MasterRepository();

  static void getAddressList() async {
    try {
      Singleton.addresses.clear();
      var data = await masterRepository.getAddresses();
      Singleton.addresses.addAll(data);
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    }
  }

  static void showMessage(String message) {
    CustomToast.showToast(message);
  }
}
