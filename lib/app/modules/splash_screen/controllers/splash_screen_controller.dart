import 'dart:async';
import 'dart:convert';

import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/auth/login_response.dart';
import 'package:jerrycan_master/app/data/repository/auth_repository.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreenController extends GetxController {
  final count = 0.obs;

  final AuthRepository _repository = AuthRepository();

  @override
  void onInit() {
    super.onInit();
    setSplash();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  setSplash() {
    String routes = Routes.BOARDING;
    Timer(Duration(seconds: 2), () async {
      // Get.to(MyApp());
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String data = preferences.getString(PREFS_LOGIN_DATA) ?? "";
      if (data.isNotEmpty) {
        Singleton.loginResponse = LoginResponse.fromJson(jsonDecode(data));
        _repository.refreshToken();

        routes = Routes.DASHBOARD;
      } else {
        String location = preferences.getString(PREFS_LOCATION) ?? "";
        if (location.isEmpty) {
          routes = Routes.CHOOSE_LOCATION;
        }
      }
      Get.offAllNamed(routes);
    });
  }
}
