import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/login/views/login_view.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/widgets/button.dart';
import 'package:jerrycan_master/app/widgets/general.dart';

import '../controllers/register_controller.dart';

class RegisterView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final RegisterController _controller = Get.put(RegisterController());
  final _focus1 = FocusNode();
  final _focus2 = FocusNode();
  final _focus3 = FocusNode();
  final _focus4 = FocusNode();
  final _focus5 = FocusNode();
  final _focus6 = FocusNode();
  final _focus7 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: Center(
            child: Image.asset(
              "assets/icons/ic_left_back.png",
              width: 28,
              height: 28,
            ),
          ),
          onTap: () {
            Get.back();
          },
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(24),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    "Daftar Akun Baru",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
              SizedBox(height: 24),
              _buildForm(context),
              SizedBox(height: 24),
              Obx(() => _controller.isLoading.value
                  ? CircularProgressIndicator()
                  : Obx(() => TextButtonPrimary(
                        isEnable: _controller.isPressing.value,
                        text: "Daftar Sekarang",
                        onPressed: _controller.doRegister,
                      ))),
              SizedBox(height: 16),
              RichText(
                text: TextSpan(
                  text: 'Sudah memiliki akun? ',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Masuk',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: COLOR_PRIMARY,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => Get.to(() => LoginView()),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _onChange(value) {
    _controller.checkEnable();
  }

  Widget _buildForm(BuildContext context) {
    return defaultForm(_controller.formKey, [
      defaultTextFormField(
        "Username",
        _controller.id,
        _onChange,
        _focus1,
        _focus2,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Nama Lengkap",
        _controller.name,
        _onChange,
        _focus2,
        _focus3,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Email",
        _controller.email,
        _onChange,
        _focus3,
        _focus4,
        inputType: TextInputType.emailAddress,
      ),
      SizedBox(height: 16),
      Row(
        children: [
          Container(
            padding: EdgeInsets.only(left: 6, right: 6, top: 12, bottom: 12),
            decoration: BoxDecoration(
                color: Color.fromRGBO(92, 225, 230, 1.0),
                borderRadius: BorderRadius.all(Radius.circular(6))),
            child: Row(
              children: [
                Image.asset(
                  "assets/icons/ic_indonesia.png",
                  width: 29,
                  height: 21,
                ),
                SizedBox(width: 4),
                Text(
                  "+62",
                  style: TextStyle(
                    height: 1.5,
                    fontSize: 14,
                    color: Color.fromRGBO(44, 63, 88, 1.0),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: defaultTextFormField(
              "Nomor Handphone",
              _controller.hp,
              _onChange,
              _focus4,
              _focus5,
              inputType: TextInputType.number,
            ),
          )
        ],
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Password",
        _controller.password,
        _onChange,
        _focus5,
        _focus6,
        obscureText: true,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Kode Referral",
        _controller.referral,
        _onChange,
        _focus6,
        _focus7,
        action: TextInputAction.done,
      ),
    ]);
  }
}
