import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/base_response.dart';
import 'package:jerrycan_master/app/data/model/register/register_request.dart';
import 'package:jerrycan_master/app/data/repository/user_repository.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/failure.dart';
import 'package:jerrycan_master/app/widgets/custom_toast.dart';

class RegisterController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final TextEditingController id = TextEditingController();
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController hp = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController referral = TextEditingController();

  final UserRepository _userRepository = UserRepository();

  RxBool isPressing = false.obs;
  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    hp.dispose();
    id.dispose();
    name.dispose();
    email.dispose();
    password.dispose();
    referral.dispose();
  }

  void checkEnable() {
    isPressing.value = id.value.text.isNotEmpty &&
        name.value.text.isNotEmpty &&
        email.value.text.isNotEmpty &&
        hp.value.text.isNotEmpty &&
        password.value.text.isNotEmpty;
  }

  void doRegister() async {
    try {
      _updateIsLoading(true);

      RegisterRequest body = RegisterRequest(
          idNumber: id.value.text,
          fullName: name.value.text,
          userEmail: email.value.text,
          phoneNumber: hp.value.text,
          role: 1,
          referralCode: referral.value.text,
          password: base64Encode(utf8.encode(password.value.text)));

      await _userRepository.postRegister(body).then((response) {
        if (response) {
          showMessage("Pendaftaran Berhasil");
          Get.offAllNamed(Routes.LOGIN);
        } else {
          showMessage(DEFAULT_ERROR);
        }
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }
}
