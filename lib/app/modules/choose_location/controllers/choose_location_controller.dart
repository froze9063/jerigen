import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/base_response.dart';
import 'package:jerrycan_master/app/data/model/choose_location/choose_location_response.dart';
import 'package:jerrycan_master/app/data/repository/location_repository.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/failure.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:jerrycan_master/app/widgets/custom_toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChooseLocationController extends GetxController {
  RxBool isLoading = false.obs;
  RxBool isCityClicked = false.obs;
  RxList<ChooseListLocation> list = <ChooseListLocation>[].obs;

  var selected;

  final LocationRepository _repository = LocationRepository();

  @override
  void onReady() {
    super.onReady();
    _getListCity();
  }

  @override
  void onClose() {}

  Future<void> _getListCity() async {
    try {
      _updateIsLoading(true);

      List<ChooseListLocation> data = await _repository.getListCity();
      list.value = data;
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }

  chooseCity(dynamic selected) async {
    this.selected = selected;
    _saveCity(selected);

    Get.offAllNamed(Routes.BOARDING);
  }

  Future<void> _saveCity(ChooseListLocation data) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(PREFS_LOCATION, jsonEncode(data));
    Singleton.location = data;
  }
}

// class ChooseLocationController extends GetxController {
//   RxBool isLoading = false.obs;
//   RxList<DropdownMenuItem<Object?>> list = <DropdownMenuItem<Object?>>[].obs;
//
//   var selected;
//
//   final LocationRepository _repository = LocationRepository();
//
//   @override
//   void onReady() {
//     super.onReady();
//     _getListCity();
//   }
//
//   @override
//   void onClose() {}
//
//   Future<void> _getListCity() async {
//     try {
//       _updateIsLoading(true);
//
//       List<ChooseListLocation> data = await _repository.getListCity();
//       list.value = _buildDropdownItems(data);
//     } on DioError catch (e) {
//       DioErrorType errorType = e.type;
//       if (errorType == DioErrorType.connectTimeout ||
//           errorType == DioErrorType.receiveTimeout ||
//           errorType == DioErrorType.sendTimeout) {
//         showMessage(ERROR_TIMEOUT);
//       } else {
//         BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
//         showMessage(baseResponse.message!);
//       }
//     } on Exception catch (e) {
//       showMessage(e.toString());
//     } on RequestException catch (e) {
//       showMessage(e.code!);
//     } catch (e) {
//       showMessage(DEFAULT_ERROR);
//     } finally {
//       _updateIsLoading(false);
//     }
//   }
//
//   List<DropdownMenuItem<Object?>> _buildDropdownItems(List data) {
//     List<DropdownMenuItem<Object?>> items = [];
//     selected = data[0];
//
//     for (var i in data) {
//       items.add(DropdownMenuItem(value: i, child: Text(i.cityName)));
//     }
//     return items;
//   }
//
//   void _updateIsLoading(bool currentStatus) {
//     isLoading.value = currentStatus;
//   }
//
//   void showMessage(String message) {
//     CustomToast.showToast(message);
//   }
//
//   chooseCity(dynamic selected) async {
//     this.selected = selected;
//     _saveCity(selected);
//
//     Get.offAllNamed(Routes.BOARDING);
//   }
//
//   Future<void> _saveCity(ChooseListLocation data) async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     preferences.setString(PREFS_LOCATION, jsonEncode(data));
//
//     Singleton.location = data;
//   }
// }
