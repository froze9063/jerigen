import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/choose_location/controllers/choose_location_controller.dart';
import 'package:jerrycan_master/app/widgets/general.dart';

import '../../../data/model/choose_location/choose_location_response.dart';
import '../../../utils/constant.dart';

class ChooseLocationView extends GetView<ChooseLocationController> {
  final _controller = Get.put(ChooseLocationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Obx(
                () => _controller.isLoading.value
                ? CircularProgressIndicator()
                : !_controller.isCityClicked.value
                ? GestureDetector(
              child: Container(
                width: double.maxFinite,
                padding: EdgeInsets.all(12),
                child: Row(
                  children: [
                    SizedBox(width: 8),
                    Container(
                      width: 16,
                      height: 16,
                    ),
                    Expanded(
                        child: Text(
                          "Pilih Lokasi Anda",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                        flex: 1),
                    Container(
                      width: 24,
                      height: 24,
                      child: Center(
                        child: SvgPicture.asset(
                            "assets/icons/ic_blue_dropdown.svg",
                            width: 24,
                            height: 24),
                      ),
                    ),
                    SizedBox(width: 8)
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                    BorderRadius.all(Radius.circular(8)),
                    border:
                    Border.all(color: Colors.black, width: 1)),
              ),
              onTap: () {
                _controller.isCityClicked.value = true;
              },
            )
                : Container(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  ChooseListLocation chooseLocation =
                  _controller.list[index];
                  return Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () =>
                          _controller.chooseCity(chooseLocation),
                      focusColor: COLOR_PRIMARY,
                      hoverColor: COLOR_PRIMARY,
                      customBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      splashColor: COLOR_PRIMARY,
                      highlightColor: COLOR_PRIMARY,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 16,
                            ),
                            child: defaultText(
                              chooseLocation.cityName ?? "",
                            ),
                          ),
                          Divider(height: 1, color: Colors.black),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: _controller.list.length,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(color: Colors.black, width: 1)),
            ),
          ),
        ),
      ),
    );
  }
}