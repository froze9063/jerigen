import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/boarding.dart';
import 'package:jerrycan_master/app/modules/boarding/views/widget/boarding_item.dart';
import 'package:jerrycan_master/app/modules/boarding/views/widget/boarding_item_2.dart';
import 'package:jerrycan_master/app/modules/login/views/login_view.dart';
import 'package:jerrycan_master/app/modules/register/views/register_view.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../routes/app_pages.dart';
import '../controllers/boarding_controller.dart';

class BoardingView extends GetView<BoardingController> {
  final BoardingController boardingController = Get.put(BoardingController());
  final PageController pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: PageView(
                controller: pageController,
                children: [
                  BoardingItem(
                    boarding: Boarding(
                      image: "assets/images/img_boarding_1.png",
                      title: "Selamat datang Agent of Change!",
                      description: "Berpartisipasilah dalam perubahan dunia!\n"
                          "Setiap minyak goreng bekas yang anda kumpulkan, kami daur ulang menjadi produk dan energi terbarukan.\n"
                          "Demi bumi yang lebih baik!",
                    ),
                  ),
                  BoardingItem(
                    boarding: Boarding(
                      image: "assets/images/img_boarding_2.png",
                      title: "Kumpulkan minyak goreng bekas",
                      description:
                          "Kumpulkan minyak goreng bekas pemakaian anda di dalam jerigen."
                          " Kami akan memberikan jerigen kepada anda gratis, hanya perlu mengajak 1 orang untuk bergabung.",
                    ),
                  ),
                  BoardingItem(
                    boarding: Boarding(
                      image: "assets/images/img_boarding_3.png",
                      title: "Jual kepada kami",
                      description:
                          "Jual minyak goreng bekas yang telah anda kumpulkan kepada kami, dengan datang langsung ke depo kami.",
                    ),
                  ),
                  BoardingItem2(
                    boarding: Boarding(
                      image: "assets/images/img_boarding_4.png",
                      title: "Dijemput Transporter",
                      description:
                          "Atau Transporter kami akan datang menjemput dengan menggunakan fitur Pick Up di aplikasi ini.",
                    ),
                  ),
                  BoardingItem(
                    boarding: Boarding(
                      image: "assets/images/img_boarding_5.png",
                      title: "Hasilkan uang",
                      description:
                          "Dapatkan poin dari setiap minyak goreng bekas yang anda jual dan tukarkan dengan uang. Anda juga dapat melakukan donasi bagi mereka yang membutuhkan.",
                    ),
                  ),
                ],
              ),
              flex: 1,
            ),
            SmoothPageIndicator(
              controller: pageController,
              count: 5,
              effect: WormEffect(
                dotWidth: 8.0,
                dotHeight: 8.0,
                spacing: 8,
                activeDotColor: COLOR_PRIMARY,
                dotColor: Color.fromRGBO(230, 230, 230, 1.0),
              ), // your preferred effect
            ),
            Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 46,
                    child: TextButton(
                      child: Text(
                        "Masuk",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      style: TextButton.styleFrom(
                        backgroundColor: COLOR_PRIMARY,
                        enableFeedback: true,
                        padding: EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 8,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      onPressed: () {
                        Get.to(() => LoginView());
                      },
                    ),
                  ),
                  SizedBox(height: 16),
                  Container(
                    width: double.infinity,
                    height: 46,
                    child: TextButton(
                      child: Text(
                        "Daftar",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                        enableFeedback: true,
                        padding: EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 8,
                        ),
                        shape: RoundedRectangleBorder(
                          side: BorderSide(width: 1, color: Colors.black),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      onPressed: () {
                        Get.to(() => RegisterView());
                      },
                    ),
                  ),
                  SizedBox(height: 16),
                  InkWell(
                    onTap: () => Get.toNamed(Routes.SETTING_TNC),
                    child: RichText(
                      text: TextSpan(
                        text:
                            'Dengan Masuk dan Daftar berarti anda setuju dengan',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' Syarat',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: COLOR_PRIMARY,
                              )),
                          TextSpan(
                            text: ' dan',
                          ),
                          TextSpan(
                              text: ' Ketentuan',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: COLOR_PRIMARY,
                              )),
                          TextSpan(
                            text: ' kami',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
