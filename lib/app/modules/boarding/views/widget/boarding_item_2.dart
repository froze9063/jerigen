// Created by ferdyhaspin on 21/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/boarding.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class BoardingItem2 extends StatelessWidget {
  final Boarding boarding;

  const BoardingItem2({Key? key, required this.boarding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: Container()),
        Image.asset(boarding.image, height: 210),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Text(
            boarding.title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w600,
              color: Color(0xFF2C3F58),
            ),
          ),
        ),
        SizedBox(height: 8),
        Container(
          height: 115,
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          child: Text(
            boarding.description,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: COLOR_SECONDARY_TEXT,
            ),
          ),
        ),
        SizedBox(height: 8),
      ],
    );
  }
}
