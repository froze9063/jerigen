import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:jerrycan_master/app/widgets/custom_toast.dart';

import '../../../../data/model/base_response.dart';
import '../../../../data/model/master/address.dart';
import '../../../../utils/constant.dart';
import '../../../../utils/failure.dart';

class AddAddressController extends GetxController {
  final TextEditingController addressDetailController = TextEditingController();
  final TextEditingController searchController = TextEditingController();

  String address = "";
  double lat = 0.0;
  double long = 0.0;

  final _repository = Singleton.masterRepository;

  final enable = false.obs;
  final isRequestIdle = true.obs;
  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null && Get.arguments is Address) {
      Address data = Get.arguments;
      List<String> addressDesc = (data.addressDesc ?? " - ").split(" - ");
      print(addressDesc.toString());
      address = addressDesc[0];
      addressDetailController.text = addressDesc[1];
      lat = data.latitude ?? 0.0;
      long = data.longitude ?? 0.0;
      enable.value = true;
    }
  }

  @override
  void onClose() {
    addressDetailController.dispose();
    super.onClose();
  }

  void doSubmit() async {
    if (Get.arguments != null && Get.arguments is Address) {
      _doUpdate();
    } else {
      _doCreate();
    }
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }

  void _doCreate() async {
    try {
      _updateIsLoading(true);
      print("$lat, $long");
      await _repository
          .createAddress(
              lat, long, address + " - " + addressDetailController.text)
          .then((response) async {
        Singleton.getAddressList();
        showMessage("Data berhasil disimpan");
        Get.back();
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _doUpdate() async {
    try {
      _updateIsLoading(true);
      print("$lat, $long");
      await _repository
          .updateAddress(Get.arguments.id, lat, long,
              address + " - " + addressDetailController.text)
          .then((response) async {
        Singleton.getAddressList();
        showMessage("Data berhasil diperbaharui");
        Get.back();
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void checkEnable() {
    enable.value = addressDetailController.value.text.isNotEmpty;
  }

  Future<void> getAddress() async {
    isRequestIdle.value = false;
    Future.delayed(Duration(seconds: 1), () async {
      try {
        if (isRequestIdle.value) {
          List<Placemark> place = await placemarkFromCoordinates(lat, long);
          Placemark first = place.first;
          isRequestIdle.value = true;
          address = '${first.name ?? ''}, ${first.subLocality ?? ''}'
              ', ${first.street ?? ''}, ${first.locality ?? ''}'
              ', ${first.subAdministrativeArea ?? ''}, ${first.administrativeArea ?? ''} '
              ', ${first.postalCode ?? ''}';
        }
      } catch (e) {
        CustomToast.showToast('Gagal mendapatkan lokasi');
        isRequestIdle.value = true;
        e.printError();
      }
    });
  }
}
