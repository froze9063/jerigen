import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/dashboard/add_address/views/widget/add_address_map.dart';

import '../../../../utils/constant.dart';
import '../../../../widgets/app_bar.dart';
import '../../../../widgets/button.dart';
import '../../../../widgets/progress_loading.dart';
import '../controllers/add_address_controller.dart';

class AddAddressView extends GetView<AddAddressController> {
  final AddAddressController _controller = Get.put(AddAddressController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (Get.arguments is bool) {
          return Get.arguments;
        } else {
          return true;
        }
      },
      child: Scaffold(
        appBar: defaultAppBar('Alamat', null),
        body: LayoutBuilder(
          builder: (context, constraint) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(minHeight: constraint.maxHeight),
                child: IntrinsicHeight(
                  child: Column(
                    children: [
                      // _buildSearch(context),
                      Expanded(child: AddAddressMap()),
                      _buildInput(context),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildInput(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Container(
            width: 57,
            height: 4,
            decoration: BoxDecoration(
              color: Color(0xFFDEDEDE),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
          SizedBox(height: 32),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: COLOR_PRIMARY),
            ),
            child: Obx(
              () => _controller.isRequestIdle.value
                  ? Text(
                      _controller.address,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: COLOR_PRIMARY_DARK_TEXT),
                    )
                  : ProgressLoading(),
            ),
          ),
          SizedBox(height: 16),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: COLOR_PRIMARY),
            ),
            child: Row(
              children: [
                Text(
                  "Detail",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: COLOR_PRIMARY_DARK_TEXT),
                ),
                SizedBox(width: 16),
                Container(width: 1, height: 45, color: COLOR_PRIMARY),
                SizedBox(width: 16),
                Expanded(
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    controller: _controller.addressDetailController,
                    onChanged: (value) {
                      _controller.checkEnable();
                    },
                    decoration: InputDecoration.collapsed(
                      hintText: 'Masukkan detail alamat',
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 16),
          Obx(() => TextButtonPrimary(
                isEnable: _controller.enable.value,
                text: "Submit",
                onPressed: _controller.doSubmit,
              )),
        ],
      ),
    );
  }
}
