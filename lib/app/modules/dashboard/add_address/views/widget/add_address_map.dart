// Created by ferdyhaspin on 26/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jerrycan_master/app/modules/dashboard/add_address/controllers/add_address_controller.dart';

import '../../../../../widgets/location_utils.dart';

class AddAddressMap extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddAddressMapState();
}

class _AddAddressMapState extends State<AddAddressMap> {
  final AddAddressController _controllerAddAddress = Get.find();
  static final CameraPosition _initCameraPosition = CameraPosition(
    target: LatLng(-6.200000, 106.816666),
    zoom: 14.4746,
  );

  Completer<GoogleMapController> _controller = Completer();
  final List<Marker> _markers = [];
  late GoogleMapController mapController;

  @override
  initState() {
    super.initState();
    getLocation();
    // listenSearch();
  }

  Future<void> getLocation() async {
    if (_controllerAddAddress.lat != 0.0) {
      _updateCamera(_controllerAddAddress.lat, _controllerAddAddress.long);
    } else {
      Position position = await LocationUtil.determinePosition();
      _updateCamera(position.latitude, position.longitude);
    }
  }

  // Future<void> listenSearch() async {
  //   _controllerAddAddress.location.listen((location) {
  // _updateCamera(location.latitude, location.longitude);
  //   });
  // }

  _updateCamera(double latitude, double longitude) {
    _goToAddress(latitude, longitude);
    // _onCameraMove(CameraPosition(target: LatLng(latitude, longitude)));
    // _controllerAddAddress.getAddress();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GoogleMap(
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          onMapCreated: _onMapCreated,
          zoomControlsEnabled: false,
          markers: Set<Marker>.of(_markers),
          initialCameraPosition: _initCameraPosition,
          onCameraMove: _onCameraMove,
          onCameraIdle: _onCameraIdle,
        ),
        Positioned(
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          child: Icon(Icons.place, size: 36, color: Colors.red),
        ),
      ],
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);
    _markers.clear();
  }

  Future<void> addMarker(LatLng latlng, String title, String desc) async {
    _markers.add(LocationUtil.addMarker(latlng, title, desc));
  }

  Future<void> _onCameraMove(CameraPosition position) async {
    _controllerAddAddress.lat = position.target.latitude;
    _controllerAddAddress.long = position.target.longitude;
  }

  Future<void> _goToAddress(double lat, double long) async {
    print("do update camera : $lat $long");
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 14.4746),
      ),
    );
  }

  void _onCameraIdle() {
    if (_controller.isCompleted) {
      _controllerAddAddress.getAddress();
    }
  }
}
