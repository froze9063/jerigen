import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';

import '../../../../../../widgets/button.dart';
import '../../../../../../widgets/general.dart';
import '../controllers/account_number_controller.dart';

class AccountNumberView extends GetView<AccountNumberController> {
  final AccountNumberController _controller =
      Get.put(AccountNumberController());
  final _focus1 = FocusNode();
  final _focus2 = FocusNode();
  final _focus3 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Rekening Bank', () => Get.back()),
      body: _body(),
    );
  }

  Widget _body() {
    return LayoutBuilder(
      builder: (context, constraint) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(24),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                        ),
                      ),
                      child: Column(
                        children: [
                          _buildForm(),
                          Expanded(
                            child: SizedBox(height: 16),
                          ),
                          Obx(() => _controller.isLoading.value
                              ? CircularProgressIndicator()
                              : Obx(() => TextButtonPrimary(
                                    isEnable: _controller.isPressing.value,
                                    text: "Submit",
                                    onPressed: _controller.doSubmit,
                                  ))),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildForm() {
    return defaultForm(_controller.formKey, [
      defaultTextFormField(
        "Nama Lengkap",
        _controller.name,
        _onChanged,
        _focus1,
        _focus2,
        inputType: TextInputType.text,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Nama Bank",
        _controller.bank,
        _onChanged,
        _focus2,
        _focus3,
        inputType: TextInputType.text,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Nomor Rekening",
        _controller.account,
        _onChanged,
        _focus3,
        null,
        inputType: TextInputType.number,
        action: TextInputAction.done,
      ),
    ]);
  }

  void _onChanged(value) {
    _controller.checkEnable();
  }
}
