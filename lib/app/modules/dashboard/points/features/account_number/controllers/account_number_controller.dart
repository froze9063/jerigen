import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/trx/account.dart';
import 'package:jerrycan_master/app/data/repository/master_repository.dart';

import '../../../../../../data/model/base_response.dart';
import '../../../../../../utils/constant.dart';
import '../../../../../../utils/failure.dart';
import '../../../../../../widgets/custom_toast.dart';

class AccountNumberController extends GetxController {
  final formKey = GlobalKey<FormState>();

  final TextEditingController name = TextEditingController();
  final TextEditingController account = TextEditingController();
  final TextEditingController bank = TextEditingController();

  final _repository = MasterRepository();

  RxBool isPressing = false.obs;
  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      Account data = Get.arguments;
      name.text = data.accountName ?? "";
      account.text = data.accountNo ?? "";
      bank.text = data.bankName ?? "";
      isPressing.value = true;
    }
  }

  @override
  void onClose() {
    name.dispose();
    account.dispose();
    super.onClose();
  }

  void doSubmit() async {
    if (Get.arguments != null) {
      _doUpdate();
    } else {
      _doCreate();
    }
  }

  void checkEnable() {
    isPressing.value = name.value.text.isNotEmpty &&
        account.value.text.isNotEmpty &&
        bank.value.text.isNotEmpty;
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }

  void _doCreate() async {
    try {
      _updateIsLoading(true);
      await _repository
          .accountDoCreate(name.text, account.text, bank.text)
          .then((response) async {
        showMessage("Data berhasil disimpan");
        Get.back();
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _doUpdate() async {
    try {
      _updateIsLoading(true);
      await _repository
          .accountDoUpdate(Get.arguments.id, name.text, account.text, bank.text)
          .then((response) async {
        showMessage("Data berhasil diperbaharui");
        Get.back();
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }
}
