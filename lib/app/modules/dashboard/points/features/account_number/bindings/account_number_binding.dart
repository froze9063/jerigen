import 'package:get/get.dart';

import '../controllers/account_number_controller.dart';

class AccountNumberBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AccountNumberController>(
      () => AccountNumberController(),
    );
  }
}
