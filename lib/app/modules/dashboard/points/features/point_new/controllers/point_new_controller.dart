import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/trx/account.dart';

import '../../../../../../data/model/base_response.dart';
import '../../../../../../data/repository/master_repository.dart';
import '../../../../../../utils/constant.dart';
import '../../../../../../utils/failure.dart';
import '../../../../../../widgets/custom_toast.dart';

class PointNewController extends GetxController {
  final _repository = MasterRepository();

  RxList<Account> accountList = <Account>[].obs;
  Rx<Account> accountChoose = Account().obs;

  RxBool isPressing = false.obs;
  RxBool isLoading = false.obs;

  void getAccountList() async {
    try {
      _updateIsLoading(true);
      List<Account> data = await _repository.getAccountList();
      accountList.value = data;
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }
}
