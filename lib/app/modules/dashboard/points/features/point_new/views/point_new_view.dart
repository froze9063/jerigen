import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:jerrycan_master/app/widgets/general.dart';
import 'package:jerrycan_master/app/widgets/progress_loading.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../../../data/model/trx/account.dart';
import '../../../../../../utils/singleton.dart';
import '../../../../../../utils/utils.dart';
import '../../../../../../widgets/account_bank_item.dart';
import '../../../../../../widgets/button.dart';
import '../../../views/point_trx.dart';
import '../controllers/point_new_controller.dart';

class PointNewView extends GetView<PointNewController> {
  final _controller = Get.put(PointNewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Poin', null, elevation: 1),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4),
                child: buildPoint(context),
              ),
              SizedBox(height: 8),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4),
                child: buildAction(context),
              ),
              SizedBox(height: 8),
              InkWell(
                onTap: () => _showWithDraw(context),
                child: CachedNetworkImage(
                  imageUrl:
                      "https://drive.google.com/uc?id=1cAMY7d4t93SpvNW9EOqUwcPR-eR7y0t1",
                ),
              ),
              InkWell(
                onTap: () => _showDonation(context),
                child: CachedNetworkImage(
                  imageUrl:
                      "https://drive.google.com/uc?id=1Ouw68xUEy5q2EdNR_ajnkfBA_P4hF-Ez",
                  width: Get.width,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPoint(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      decoration: BoxDecoration(
        color: COLOR_PRIMARY,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                defaultText(
                  'Poin Anda',
                  size: 16,
                  weight: FontWeight.w500,
                ),
                SizedBox(height: 4),
                Row(
                  children: [
                    Text(
                      Utils.toIDR(Singleton.loginResponse.user!.points ?? 0),
                      style: TextStyle(
                        color: COLOR_PRIMARY_DARK_TEXT,
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                      ),
                    ),
                    SizedBox(width: 8),
                    Image.asset("assets/icons/ic_coin.png", height: 24),
                  ],
                )
              ],
            ),
          ),
          SvgPicture.asset(
            "assets/icons/ic_point_dashboard.svg",
            height: 55,
          ),
        ],
      ),
    );
  }

  Widget buildAction(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: Get.width / 2 - 25,
          height: 44,
          child: TextButtonWhiteWrap(
            text: 'Daftar Rekening',
            onPressed: () => _showAccountNumber(context, ""),
          ),
        ),
        SizedBox(
          width: Get.width / 2 - 25,
          height: 44,
          child: TextButtonWhiteWrap(
            text: 'Riwayat Poin',
            onPressed: () => Get.toNamed(Routes.POINTS),
          ),
        ),
      ],
    );
  }

  Future<void> _showDonation(context) async {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Form(
          key: key,
          child: SingleChildScrollView(
            child: AnimatedPadding(
              padding: MediaQuery.of(context).viewInsets,
              duration: const Duration(milliseconds: 100),
              curve: Curves.decelerate,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 57,
                      height: 4,
                      decoration: BoxDecoration(
                        color: Color(0xFFDEDEDE),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                    ),
                    SizedBox(height: 32),
                    TextFormField(
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                      decoration: InputDecoration(
                        labelText: 'Jumlah Donasi',
                        labelStyle:
                            TextStyle(color: COLOR_PRIMARY_DARK, fontSize: 12),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: COLOR_PRIMARY),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    TextButtonPrimary(
                      isEnable: true,
                      text: 'Donasi',
                      onPressed: () {
                        Get.back();
                        Get.to(PointTrx(
                          title: 'Donasi',
                          desc:
                              'Permintaan kami proses. Dana akan segera kami salurkan bagi mereka yang membutuhkan.',
                        ));
                      },
                    ),
                    SizedBox(height: 16),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showWithDraw(context) async {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Form(
          key: key,
          child: SingleChildScrollView(
            child: AnimatedPadding(
              padding: MediaQuery.of(context).viewInsets,
              duration: const Duration(milliseconds: 100),
              curve: Curves.decelerate,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 57,
                      height: 4,
                      decoration: BoxDecoration(
                        color: Color(0xFFDEDEDE),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                    ),
                    SizedBox(height: 32),
                    TextFormField(
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                      decoration: InputDecoration(
                        labelText: 'Jumlah Poin',
                        labelStyle:
                            TextStyle(color: COLOR_PRIMARY_DARK, fontSize: 12),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: COLOR_PRIMARY),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    InkWell(
                      onTap: () {
                        Get.back();
                        _showAccountNumber(context, ACCOUNT_BANK_CHOOSE);
                      },
                      child: Container(
                        width: Get.width,
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          border: Border.all(color: COLOR_PRIMARY),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/icons/ic_pickup_add.png",
                              width: 19,
                            ),
                            SizedBox(width: 8),
                            Obx(() {
                              Account data = _controller.accountChoose.value;
                              String text = data.accountName != null
                                  ? '${data.accountName}\n${data.bankName} - ${data.accountNo}'
                                  : "Pilih Nomor Rekening";

                              return Text(
                                text,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: COLOR_PRIMARY_TEXT,
                                ),
                              );
                            }),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    TextButtonPrimary(
                      isEnable: true,
                      text: 'Tarik Tunai',
                      onPressed: () {
                        Get.back();
                        Get.to(PointTrx(
                          title: 'Tarik Tunai',
                          desc:
                              'Permintaan kami proses. Dana akan masuk dalam 1x24 jam ke rekening anda.',
                        ));
                      },
                    ),
                    SizedBox(height: 16),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showAccountNumber(BuildContext context, String type) async {
    _controller.getAccountList();

    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: SingleChildScrollView(
          child: AnimatedPadding(
            padding: MediaQuery.of(context).viewInsets,
            duration: const Duration(milliseconds: 100),
            curve: Curves.decelerate,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: 57,
                    height: 4,
                    decoration: BoxDecoration(
                      color: Color(0xFFDEDEDE),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                  ),
                  SizedBox(height: 32),
                  Obx(() {
                    return _controller.isLoading.value
                        ? ProgressLoading()
                        : _controller.accountList.length == 0
                            ? Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 16),
                                  child: defaultText("Tidak ada data Rekening Bank"),
                                ),
                              )
                            : Column(
                                children: [
                                  ...List.generate(
                                      _controller.accountList.length, (index) {
                                    Account data =
                                        _controller.accountList[index];
                                    return AccountBankItem(
                                      title:
                                          'Rekening Bank Tersimpan ${index + 1}',
                                      label:
                                          '${data.accountName}\n${data.bankName} - ${data.accountNo}',
                                      type: ACCOUNT_BANK_CHOOSE,
                                      callback: (type) {
                                        switch (type) {
                                          case ACCOUNT_BANK_CHOOSE:
                                            _controller.accountChoose.value =
                                                data;
                                            _showWithDraw(context);
                                            break;
                                          case ACCOUNT_BANK_EDIT:
                                            Get.toNamed(
                                              Routes.ACCOUNT_NUMBER,
                                              arguments: data,
                                            );
                                            break;
                                        }
                                      },
                                    );
                                  })
                                ],
                              );
                  }),
                  SizedBox(height: 32),
                  TextButtonPrimary(
                    isEnable: true,
                    text: "Tambah Rekening Bank",
                    onPressed: () {
                      Get.back();
                      Get.toNamed(Routes.ACCOUNT_NUMBER);
                    },
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
