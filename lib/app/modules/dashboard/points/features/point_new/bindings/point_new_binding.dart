import 'package:get/get.dart';

import '../controllers/point_new_controller.dart';

class PointNewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PointNewController>(
      () => PointNewController(),
    );
  }
}
