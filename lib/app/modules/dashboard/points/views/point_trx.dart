// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/widgets/button.dart';

import '../../../../utils/constant.dart';

class PointTrx extends StatelessWidget {
  final String title;
  final String desc;

  const PointTrx({Key? key, required this.title, required this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            Expanded(child: SizedBox()),
            Text(
              '$title Sukses!',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: COLOR_PRIMARY_DARK_TEXT,
                fontWeight: FontWeight.w500,
                fontSize: 30,
              ),
            ),
            SizedBox(height: 16),
            Image.asset("assets/icons/ic_coin.png", width: 74),
            SizedBox(height: 16),
            Text(
              desc,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: COLOR_PRIMARY_TEXT,
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
            Expanded(child: SizedBox()),
            TextButtonPrimary(
              isEnable: true,
              text: 'Selesai',
              onPressed: () {
                Get.back();
              },
            ),
          ],
        ),
      ),
    );
  }
}
