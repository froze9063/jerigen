import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/dashboard/points/views/point_trx.dart';
import 'package:jerrycan_master/app/modules/dashboard/points/views/widget/point_content.dart';
import 'package:jerrycan_master/app/modules/dashboard/points/views/widget/point_data.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../utils/constant.dart';
import '../../../../widgets/app_bar.dart';
import '../../../../widgets/button.dart';
import '../../home/features/pickup/pickup_input/views/widget/saved_address_item.dart';
import '../controllers/points_controller.dart';

class PointsView extends GetView<PointsController> {
  final PointsController _controller = Get.put(PointsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Riwayat Poin', () => Get.back()),
      body: Column(
        children: [
          PointData(
            onChangeDate: () {
              _controller.changeDate(context);
            },
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: SingleChildScrollView(
                child: PointContent(),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
