// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';

import '../../../../../utils/constant.dart';
import '../../../../../utils/singleton.dart';
import '../../../../../utils/utils.dart';

class PointData extends StatelessWidget {
  final GestureTapCallback onChangeDate;

  const PointData({Key? key, required this.onChangeDate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 3,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8),
        child: Column(
          children: [
            Image.asset("assets/icons/ic_coin.png", width: 74),
            Text(
              Utils.toIDR(Singleton.loginResponse.user!.points ?? 0),
              style: TextStyle(
                color: COLOR_PRIMARY_DARK_TEXT,
                fontWeight: FontWeight.w600,
                fontSize: 24,
              ),
            ),
            SizedBox(height: 12),
            _buildData(
              context,
              "ic_coin_blue.png",
              'Total poin didapat',
              '12.400',
            ),
            _buildData(
              context,
              "ic_pickup.png",
              'Total tips untuk Transporter',
              '3.000',
            ),
            _buildData(
              context,
              "ic_point_donation.png",
              'Total poin didonasikan',
              '1.000',
            ),
            _buildData(
              context,
              "ic_withdraw.png",
              'Total poin dicairkan',
              '6.000',
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                  onTap: onChangeDate,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      border: Border.all(color: COLOR_PRIMARY),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Row(
                      children: [
                        Image.asset("assets/icons/ic_calendar.png", width: 16),
                        SizedBox(width: 8),
                        Text(
                          'Tanggal',
                          style: TextStyle(
                            fontSize: 14,
                            color: COLOR_PRIMARY_TEXT,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildData(BuildContext context, String s, String t, String u) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset("assets/icons/$s", width: 16),
              SizedBox(width: 8),
              Expanded(
                child: Text(
                  t,
                  style: TextStyle(
                    fontSize: 14,
                    color: COLOR_PRIMARY_TEXT,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Text(
                u,
                style: TextStyle(
                  fontSize: 14,
                  color: COLOR_PRIMARY_DARK_TEXT,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic,
                ),
              )
            ],
          ),
          SizedBox(height: 8),
          Container(height: 1, color: Colors.grey),
        ],
      ),
    );
  }
}
