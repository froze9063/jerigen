// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/modules/dashboard/points/views/widget/point_item.dart';

class PointContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PointItem(),
        PointItem(),
        PointItem(),
      ],
    );
  }
}
