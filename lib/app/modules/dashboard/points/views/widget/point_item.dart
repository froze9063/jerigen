// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';

import '../../../../../utils/constant.dart';

class PointItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 0),
      child: Column(
        children: [
          Text(
            '30 November 2021',
            style: TextStyle(
              fontSize: 14,
              color: COLOR_PRIMARY_DARK_TEXT,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 8),
          Card(
            elevation: 5,
            child: Column(
              children: [
                _buildItem(),
                _buildItem(),
                _buildItem(),
                _buildItem(),
                _buildItem()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem() {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(width: 8),
            Text(
              '10.15',
              style: TextStyle(
                fontSize: 16,
                color: COLOR_PRIMARY_TEXT,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(width: 8),
            Container(width: 1, color: COLOR_BORDER, height: 30),
            SizedBox(width: 8),
            Expanded(
              child: Text(
                '18 Kilogram terjual',
                style: TextStyle(
                  fontSize: 16,
                  color: COLOR_PRIMARY_TEXT,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Container(width: 1, color: COLOR_BORDER, height: 30),
            SizedBox(width: 8),
            Text(
              '180',
              style: TextStyle(
                fontSize: 16,
                color: COLOR_PRIMARY_TEXT,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic,
              ),
            ),
            SizedBox(width: 8),
          ],
        ),
        Container(height: 1, color: COLOR_BORDER),
      ],
    );
  }
}
