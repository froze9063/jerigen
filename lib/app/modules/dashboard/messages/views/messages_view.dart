import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/modules/dashboard/messages/views/widget/chat_item.dart';
import 'package:jerrycan_master/app/modules/dashboard/messages/views/widget/notification_item.dart';

import '../../../../data/model/home/activity.dart';
import '../../../../utils/constant.dart';
import '../../../../widgets/app_bar.dart';

class MessagesView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MessagesViewState();
}

class _MessagesViewState extends State<MessagesView>
    with SingleTickerProviderStateMixin {
  late TabController controller;

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Messages', null, elevation: 0),
      body: Column(
        children: [
          _buildTabs(context),
          Expanded(
            child: _buildTabBarViews(context),
          ),
        ],
      ),
    );
  }

  Widget _buildTabs(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 2,
      child: TabBar(
        labelColor: COLOR_PRIMARY_DARK_TEXT,
        unselectedLabelColor: COLOR_PRIMARY_TEXT,
        indicatorColor: COLOR_INDICATOR,
        labelStyle: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w300,
        ),
        controller: controller,
        tabs: <Widget>[
          Tab(text: 'Chat'),
          Tab(text: 'Notifikasi'),
        ],
      ),
    );
  }

  Widget _buildTabBarViews(BuildContext context) {
    return TabBarView(
      controller: controller,
      children: <Widget>[
        _buildContent(context, [
          ChatItem(),
          ChatItem(),
          ChatItem(),
          ChatItem(),
        ]),
        _buildContent(context, [
          NotificationItem(),
          NotificationItem(),
          NotificationItem(),
          NotificationItem(),
        ]),
      ],
    );
  }

  Widget _buildContent(BuildContext context, List<Widget> children) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          children: children,
        ),
      ),
    );
  }
}
