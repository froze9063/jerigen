// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class ChatItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 115,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(height: 1, color: COLOR_PRIMARY),
          SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/images/img_example_profile.png",
                width: 56,
                color: COLOR_PRIMARY,
              ),
              SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Puja Raditya    ',
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT.withOpacity(0.5),
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Motor - F 2022 PX',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: COLOR_PRIMARY_DARK_TEXT,
                              fontSize: 12,
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 12),
                    Text(
                      'Oke otw Pak!',
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        color: COLOR_PRIMARY_EXTRA_DARK_TEXT,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(width: 12),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 12),
          Text(
            '15.10',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: COLOR_PRIMARY_TEXT,
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ),
          ),
          SizedBox(height: 12),
        ],
      ),
    );
  }
}
