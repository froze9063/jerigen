// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class StorySection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) => Container(
        margin: EdgeInsets.only(
            left: index == 0 ? 16 : 8, right: index == 4 ? 16 : 0),
        color: Colors.transparent,
        width: 178,
        child: Card(
          color: Colors.transparent,
          shadowColor: Colors.transparent,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/images/dummy_story.png",
                width: 178,
                fit: BoxFit.fill,
              ),
              SizedBox(height: 5),
              Text(
                'Jadi Bahan Bakar Alternatif, Begini Cara Pembuatan Biodiesel',
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 12,
                  color: COLOR_PRIMARY_TEXT,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
      ),
    );
  }
}
