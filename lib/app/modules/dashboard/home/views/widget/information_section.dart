// Created by ferdyhaspin on 23/01/22.
// Copyright (c) 2022 Jerigen All rights reserved.

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class InformationSection extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _InformationSectionState();
}

class _InformationSectionState extends State<InformationSection> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  final details = [
    '{"title":"Tentang Kami","data":[{"section":"Latar Belakang","content":[{"image":"https://drive.google.com/uc?id=1nDH1jrt2h6PrRZVBTrqVKeUriUPOZYQn","description":"Sampah telah menjadi masalah tak terkendali yang sangat sulit untuk dipecahkan, tak terkecuali sampah minyak goreng bekas. Kami hadir dengan misi menciptakan ekosistem berbasis masyarakat untuk pengelolaan sampah minyak goreng bekas yang melibatkan mereka secara proaktif serta membuka mata pencaharian baru bagi masyarakat lainnya."},{"image":"https://drive.google.com/uc?id=1rxcUfETLjbLsyoSvwIrn2oV3N4d-dgEs","description":"<b>1. Ketidakpedulian</b></br></br>Ketidakpedulian Kesadaran dan pengetahuan masyarakat yang kurang terhadap pengelolaan sampah dan proses daur ulangnya."},{"image":"https://drive.google.com/uc?id=1uw_E9rPAzw9Vg26Tbn8KNey8IsQ9AG0X","description":"<b>2. Melelahkan</b></br></br>Pemerintah maupun pihak-pihak terkait kewalahan dengan sampah minyak goreng bekas yg tercecer dan dibuang ke saluran air sehingga menyebabkan penyumbatan."},{"image":"https://drive.google.com/uc?id=1Cmp0wO1bobXhgc7ZfRMSJG9d497yvjYp","description":"<b>3. Tercampur Aduk</b></br></br>Terkadang sampah minyak goreng bekas dibuang ke tong sampah, sehingga tercampur aduk, bau, dan akhirnya menjadi menumpuk dan mencemari lingkungan."}]},{"section":"Utopia","content":[{"image":"https://drive.google.com/uc?id=1A3hytbXsc-P46lFGUUhRp6llkiL-zKml","description":"Kita tidak bisa menyelesaikan masalah sampah ini secara keseluruhan, akan tetapi kita semua bisa aktif membantu untuk menguranginya. Kami mengadaptasi filosofi ekonomi sirkular dengan membuat sistem tempat pembuang, penjemput, dan pengepul sampah minyak goreng bekas terintegrasi di dalam satu platform, Jerigen."},{"image":"https://drive.google.com/uc?id=1FMvCWP6EjYvxqo1UN_AtAvGjZsWlZ9dE","description":"<b>Pemilahan Sampah</b></br></br>Mendorong masyarakat untuk memisahkan sampah minyak goreng bekas menggunakan jerigen atau medium serupa lainnya."},{"image":"https://drive.google.com/uc?id=1jyWF4JNdAMIpum3A-yS5anKftlyebt8x","description":"<b>2. Daur Ulang</b></br></br>Mengolah kembali sampah minyak goreng bekas menjadi energi terbarukan BioDiesel. BioDiesel digunakan sebagai energi alternatif pengganti Bahan Bakar Minyak untuk jenis diesel/solar pada alat transportasi, seperti bus, truk, dan kapal laut, maupun pada pabrik-pabrik."},{"image":"https://drive.google.com/uc?id=10ZssOhRz0eKCz_QGNkWTrdtqJDYjJFm_","description":"<b>3. Menguntungkan</b></br></br> Memberikan kompensasi dalam bentuk yang bisa dirupiahkan kepada mereka yang mengumpulkan sampah minyak goreng bekas."}]},{"section":"Visi dan Misi","content":[{"image":"https://drive.google.com/uc?id=1CdL0KPYa-hc2vFVG3KVarCW1G87TmXkA","description":"<b>Visi</b></br></br>Menjadi agen perubahan dunia dengan amanah mulia dalam melestarikan lingkungan yang terus berkelanjutan dengan menerapkan gerakan ekonomi sirkular sepenuh hati."},{"image":"https://drive.google.com/uc?id=12MbckxHMellCLL7ntjzPezvcEaGkeEp5","description":"<b>Misi</b></br></br>Menciptakan ekosistem yang mengintegrasikan pengelolaan sampah, teknologi digital, dan nilai ekonomi untuk mengubah perilaku manusia menuju lingkungan yang lebih baik dan hijau."}]},{"section":"Dunia Kami","content":[{"image":"https://drive.google.com/uc?id=1NH4e5SJ43v4csFyxrnW2RX79RQ5J3JTT","description":"<b>Agent of Change</b></br></br> Kami memanggil pengguna aplikasi Jerigen dengan sebutan Agent of Change, atau Agen Perubahan. Di tangan mereka lah visi dan misi mulia Jerigen teremban. Demi bumi yang lebih baik."},{"image":"https://drive.google.com/uc?id=1tE1KegtRrb7Gb_sj4vSEuGmhShGC9ZWl","description":"<b>Transporter</b></br></br>Transporter bertugas menjemput minyak goreng bekas dari Agent of Change menuju Depo Jerigen. Transporter juga menerima Agent of Change yang datang menghampiri dan melakukan scan barcode. Minyak goreng bekas disimpan di kontainer Transporter untuk kemudian dibawa ke Depo Jerigen."},{"image":"https://drive.google.com/uc?id=1utzdQ-K_GOGHXycdh8bp4IkZvMm1LqOE","description":"<b>Depo Agent</b></br></br>Depo Agent adalah mereka yang memiliki ruangan untuk dijadikan Depo Jerigen.  Depo Agent menerima Agent of Change dan Transporter yang datang langsung ke Depo dan melakukan scan barcode. Minyak goreng bekas disimpan di Depo untuk kemudian dibawa ke Warehouse Jerigen."}]}]}',
    '{"title":"Cara Menggunakan Jerigen","data":[{"section":"Pick Up","content":[{"image":"https://drive.google.com/uc?id=1PVfCFfjwQyzncgnj5mW57WE5bnQaPTqi","description":"Agent of Change yang telah mengumpulkan minyak goreng bekas minimal 1 jerigen ukuran 5 liter dapat melakukan request penjemputan melalui fitur Pick Up di halaman utama aplikasi Jerigen."},{"image":"https://drive.google.com/uc?id=1_gKSVtouIcf4offo40vY9kDCXgWhmUCq","description":"<b>Pilih Alamat</b></br></br>Pilih alamat penjemputan yang sudah anda daftarkan. Apabila menginginkan alamat baru, pilih fitur tambah alamat untuk mendaftarkan alamat baru."},{"image":"https://drive.google.com/uc?id=1lT0AfVPLFjbP6vNcVWhUsfDqEisfzQVc","description":"<b>Waktu Penjemputan</b></br></br>Anda dapat melakukan request penjemputan hingga H+2. Transporter akan datang antara 1 jam sebelum dan 1 jam sesudah waktu pilihan anda."},{"image":"https://drive.google.com/uc?id=1CR9woM4IlOePZA9eNkKRRai2W2p3JE_1","description":"<b>Jumlah Kilogram</b></br></br>Masukkan estimasi jumlah Kilogram minyak goreng bekas yang telah anda kumpulkan."},{"image":"https://drive.google.com/uc?id=1F-Mq-VTwzpd3dUnWBO1PY8PwL3lOn03D","description":"<b>Potensi Poin dan Donasi</b></br></br>Setiap Kilogram yang dimasukkan akan memunculkan potensi poin yang akan didapat berdasarkan harga poin per Kilogram yang terdapat di halaman utama aplikasi. Potensi poin yang didapat bisa didonasikan melalui fitur donasi. Donasi akan disalurkan melalui badan amal dan zakat terkemuka di Indonesia."},{"image":"https://drive.google.com/uc?id=1lqgAg1z-FYDdj1bioG3y2OL_fBJH1u3w","description":"<b>Mencari Transporter Terdekat</b></br></br>Setelah mengklik tombol request, Jerigen akan mencari Transporter untuk menuju ke alamat anda pada waktu yang anda pilih. Anda bisa berkomunikasi melalui fitur chat maupun telepon langsung kepada Transporter yang datang."},{"image":"https://drive.google.com/uc?id=1TAgD8SN5vTgT6qf-RPnHaBbzcSepDkN_","description":"<b>Penjemputan Selesai</b></br></br>Setelah Transporter tiba, minyak goreng bekas yang telah terkumpul akan diukur ulang dan dimasukkan ke dalam jerigen Transporter. Transporter akan memasukkan jumlah Kilogram yang telah diukur dan menyelesaikan proses penjemputan di aplikasi. Poin akan secara otomatis masuk ke dalam aplikasi Agent of Change sesuai dengan jumlah Kilogram yang sesuai."}]},{"section":"Scan","content":[{"image":"https://drive.google.com/uc?id=1NkgXVMfEy3rDQ_dWDivvJ98NUEDodh9Q","description":"Anda dapat menggunakan fitur Scan apabila berpapasan langsung dengan Transporter di dekat anda, atau ketika anda mengunjungi Depo Jerigen terdekat. Lokasi Depo Jerigen dapat dicari melalui fitur search di halaman utama aplikasi Jerigen. Fitur Scan tidak memiliki jumlah minimal Kilogram sampah minyak goreng bekas sehingga berapapun jumlah Kilogram yang anda miliki akan diterima."},{"image":"https://drive.google.com/uc?id=10ZPESuvxhjKiyBF4ey7qXG4upXpuT--Z","description":"<b>Scan Barcode</b></br></br>Transporter dan Depo Agent Jerigen memiliki barcode unik. Anda perlu scan barcode tersebut yang kemudian diikuti dengan memasukkan pin angka yang muncul di layar aplikasi Transporter dan Depo Agent untuk menghubungkan akun anda."},{"image":"https://drive.google.com/uc?id=1CR9woM4IlOePZA9eNkKRRai2W2p3JE_1","description":"<b>Jumlah Kilogram</b></br></br>Masukkan estimasi jumlah Kilogram minyak goreng bekas yang telah anda kumpulkan."},{"image":"https://drive.google.com/uc?id=1F-Mq-VTwzpd3dUnWBO1PY8PwL3lOn03D","description":"<b>Potensi Poin dan Donasi</b></br></br>Seperti fitur Pick Up, setiap Kilogram yang dimasukkan akan memunculkan potensi poin yang akan didapat berdasarkan harga poin per Kilogram yang terdapat di halaman utama aplikasi. Potensi poin yang didapat bisa didonasikan melalui fitur donasi. Donasi akan disalurkan melalui badan amal dan zakat terkemuka di Indonesia."},{"image":"https://drive.google.com/uc?id=1TAgD8SN5vTgT6qf-RPnHaBbzcSepDkN_","description":"<b>Scan Selesai</b></br></br>Minyak goreng bekas yang telah terkumpul akan ditimbang ulang dan dimasukkan ke dalam tangki Depo Agent. Depo Agent akan memasukkan jumlah liter yang telah diukur dan menyelesaikan proses scan di aplikasi. Poin akan secara otomatis masuk ke dalam aplikasi Agent of Change sesuai dengan jumlah Kilogram yang sesuai."}]}]}',
    '{"title":"Spesifikasi Minyak Goreng Bekas","data":[{"section":"","content":[{"image":"https://drive.google.com/uc?id=1xIVRsp-2UH0NnEhEUzGiw5pN4nujI_Zh","description":"<b>Murni dan Disaring</b></br></br>Minyak goreng bekas yang anda kumpulkan harus murni bekas minyak goreng yang terpakai dan telah disaring dari kotoran dan sisa makanan, sebelum disimpan di jerigen atau wadah sejenis lainnya."},{"image":"https://drive.google.com/uc?id=1t0cfNpCEOSH-ZqvtKHTrfahCEqbepAtO","description":"<b>Wadah Tertutup </b></br></br>Wadah penyimpanan minyak goreng bekas sebaiknya tidak terpapar sinar matahari secara langsung dan tertutup dengan rapat."},{"image":"https://drive.google.com/uc?id=1h8wNCyNBJWX2ZhXA6wNXT1L75TcCyZNC","description":"<b>Tidak Tercampur Air</b></br></br>Minyak goreng bekas yang dikumpulkan tidak boleh tercampur atau terkontaminasi oleh air maupun zat kimia, seperti bensin, oli, fame, glycerin, minyak hewani, PAO, CPO dan lainnya, baik secara tekstur maupun bau."},{"image":"https://drive.google.com/uc?id=1BDSzvCo9pg5fvLnGaReNjKHuShiS_fnx","description":"<b>Penolakan</b></br></br>Apabila minyak goreng bekas yang telah terkumpul tidak sesuai dengan standar yang kami telah sebutkan di atas, maka Jerigen berhak untuk menolak minyak goreng bekas anda."}]}]}',
    '{"title":"Wilayah Layanan","data":[{"section":"","content":[{"image":"https://drive.google.com/uc?id=1BzaD55m2UFs-KNsfd1rNKVnH7KujM37S","description":"<b>Jakarta Greater Area</b></br></br>Area Untuk saat ini, Jerigen baru beroperasi di wilayah Kota Jakarta Timur dan Kota Bekasi. Untuk wilayah Jabodetabek dan kota-kota lainnya akan segera menyusul dalam waktu dekat."}]}]}',
    '{"title":"Cara Tarik Tunai dan Donasi","data":[{"section":"Tarik Tunai","content":[{"image":"https://drive.google.com/uc?id=1hK4EzvFqlFuG2dCVSmc30vpfggDMJFbV","description":"<b>Menu Poin</b></br></br>Masuk ke menu Poin lalu pilih fitur Tarik Tunai. Minimum penarikan adalah 50.000 Poin."},{"image":"https://drive.google.com/uc?id=12_5i79xod1Ul7ykoClWDjH6OqEQHaQ-g","description":"<b>Detail Rekening</b></br></br>Masukkan jumlah poin yang akan ditarik, nama pemilik rekening beserta nama Bank dan nomor rekening."},{"image":"https://drive.google.com/uc?id=1VINOGN19OjjjjqHGyeiW1gaum5F2t-WK","description":"<b>Proses Tarik Tunai</b></br></br>Admin akan menerima permintaan penarikan tunai dan akan memproses maksimal 1x24 jam."}]},{"section":"Donasi","content":[{"image":"https://drive.google.com/uc?id=1hK4EzvFqlFuG2dCVSmc30vpfggDMJFbV","description":"<b>Menu Poin</b></br></br>Masuk ke menu Poin lalu pilih fitur Donasi. Selain melalui menu Poin, fitur Donasi juga muncul setiap kali anda melakukan transaksi Pick Up maupun Scan."},{"image":"https://drive.google.com/uc?id=1JPCpVMtwVSojbMLg8xcWTq9pf2ntco8l","description":"<b>Jumlah Donasi</b></br></br>Masukkan jumlah poin yang akan anda donasikan."},{"image":"https://drive.google.com/uc?id=18JH9x-xXn5EfgAtgxudgtxcFD-4d0Nd5","description":"<b>Penyaluran Donasi</b></br></br>Admin akan menerima permintaan donasi anda dan akan menyalurkan donasi anda melalui badan amal dan zakat terkemuka di Indonesia."}]}]}',
  ];

  final imgList = [
    'https://drive.google.com/uc?id=1uQEpfrVxNJ8DQT6MtbLeDRUTqKccNPRR',
    'https://drive.google.com/uc?id=17M45U6G8TYiuLya_JQqw-9HvDr6BFPqx',
    'https://drive.google.com/uc?id=15wY40di2oUDHlAoE6x0lSx9cYeMwGrcc',
    'https://drive.google.com/uc?id=1zjm1xgZpz9CBUJcBe2CYY9o01aSpQIAN',
    'https://drive.google.com/uc?id=1pL9hfh2399-fL7RStKrkmd_VFWcUa0Ja',
  ];

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = imgList
        .map(
          (item) => InkWell(
            onTap: () => Get.toNamed(
              Routes.INFORMATION_DETAIL,
              arguments: details[imgList.indexOf(item)],
            ),
            child: CachedNetworkImage(
              imageUrl: item,
            ),
          ),
        )
        .toList();

    return Container(
      height: 170,
      width: double.infinity,
      child: Column(
        children: [
          CarouselSlider(
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
              height: 160,
              enlargeCenterPage: true,
              viewportFraction: 0.7,
              autoPlay: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
            ),
          ),
          AnimatedSmoothIndicator(
            activeIndex: _current,
            count: imgList.length,
            effect: WormEffect(
              dotWidth: 8.0,
              dotHeight: 8.0,
              spacing: 8,
              activeDotColor: COLOR_PRIMARY,
              dotColor: Color.fromRGBO(230, 230, 230, 1.0),
            ),
          ),
        ],
      ),
    );
  }
}
