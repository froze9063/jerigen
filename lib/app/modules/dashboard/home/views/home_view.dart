import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/views/widget/information_section.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/views/widget/story_section.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:jerrycan_master/app/utils/utils.dart';

import '../../../../widgets/status_bar_primary.dart';
import '../controllers/home_controller.dart';

typedef GoToCallback = void Function(int index);

class HomeView extends GetView<HomeController> with WidgetsBindingObserver {
  final HomeController homeController = Get.put(HomeController());
  final GoToCallback goToCallback;

  HomeView(this.goToCallback);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 35),
      width: Get.width,
      height: Get.height,
      color: COLOR_PRIMARY,
      child: StatusBarPrimary(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: 400,
                color: Colors.white,
                margin: EdgeInsets.fromLTRB(0, 175, 0, 0),
              ),
              Container(
                child: Column(
                  children: [
                    SizedBox(height: 14),
                    _buildProfile(context),
                    SizedBox(height: 16),
                    _buildPoint(context),
                    _buildSearch(context),
                    _buildFeatures(context),
                    InformationSection(),
                    SizedBox(height: 8),
                    _buildStory(context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildProfile(BuildContext context) {
    String pic = Singleton.loginResponse.user?.profilePictUrl ?? "";
    return InkWell(
      onTap: () => goToCallback(4),
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: [
            pic.isNotEmpty
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(10000),
                    child: CachedNetworkImage(
                      height: 56,
                      width: 56,
                      fit: BoxFit.fill,
                      imageUrl: pic,
                    ),
                  )
                : Image.asset(
                    "assets/images/img_default.png",
                    height: 56,
                    color: Colors.white,
                  ),
            SizedBox(width: 16),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${Singleton.loginResponse.user!.fullName}",
                  style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(44, 63, 88, 1.0),
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 4),
                Text(
                  "${Singleton.loginResponse.user!.idNumber}",
                  style: TextStyle(
                    fontSize: 10,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(100, 111, 126, 1.0),
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "CLIMATE WARRIOR",
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(100, 111, 126, 1.0),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPoint(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 42),
      child: Row(
        children: [
          Expanded(
            child: InkWell(
              onTap: () => goToCallback(2),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.5),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Poin",
                      style: TextStyle(
                        color: Color.fromRGBO(100, 111, 126, 1.0),
                        fontWeight: FontWeight.w300,
                        fontSize: 10,
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${Utils.toIDR(Singleton.loginResponse.user!.points)} Poin",
                            style: TextStyle(
                              color: Color.fromRGBO(100, 111, 126, 1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                            ),
                          ),
                          flex: 1,
                        ),
                        Image.asset("assets/icons/ic_coin.png", height: 14),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(width: 24),
          Expanded(
            child: InkWell(
              onTap: () => Get.toNamed(Routes.TODAY_PRICE),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.5),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Harga per Kg",
                      style: TextStyle(
                        color: Color.fromRGBO(100, 111, 126, 1.0),
                        fontWeight: FontWeight.w300,
                        fontSize: 10,
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                              "${Utils.toIDR(5000)} Poin",
                              style: TextStyle(
                                color: Color.fromRGBO(100, 111, 126, 1.0),
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                              ),
                            ),
                            flex: 1),
                        Image.asset("assets/icons/ic_dollar.png", height: 14),
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSearch(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.SEARCH_DEPO);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          child: Container(
            height: 55,
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                Image.asset("assets/icons/ic_search.png", height: 18),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    "Cari depo Jerigen terdekat",
                    style: TextStyle(
                      fontSize: 14,
                      color: Color.fromRGBO(182, 182, 182, 1.0),
                    ),
                  ),
                  flex: 1,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFeatures(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Material(
          color: Colors.transparent,
          child: InkWell(
            customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            splashColor: COLOR_PRIMARY.withOpacity(0.2),
            highlightColor: COLOR_PRIMARY.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  Image.asset("assets/icons/ic_pickup.png",
                      height: 80, width: 80),
                  Text(
                    'Pick Up',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: COLOR_PRIMARY_TEXT,
                    ),
                  )
                ],
              ),
            ),
            onTap: () {
              if (Singleton.addresses.isEmpty) {
                Get.toNamed(Routes.ADD_ADDRESS);
              } else {
                Get.toNamed(Routes.PICKUP_INPUT);
              }
            },
          ),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            splashColor: COLOR_PRIMARY.withOpacity(0.2),
            highlightColor: COLOR_PRIMARY.withOpacity(0.2),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset("assets/icons/ic_scan.png",
                        height: 60, width: 60),
                  ),
                  Text(
                    'Scan',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: COLOR_PRIMARY_TEXT,
                    ),
                  ),
                ],
              ),
            ),
            onTap: () => {
              // Get.to(ScannerPinView(), arguments: 'D000002')
              Get.toNamed(Routes.SCANNER)
            },
          ),
        ),
      ],
    );
  }

  Widget _buildStory(BuildContext context) {
    return SizedBox(
      height: 218,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xFF44A3A7), Color(0xFF5CE1E6)],
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              child: Text(
                'Berita, informasi, dan cerita menarik lainnya..',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: StorySection(),
            ),
          ],
        ),
      ),
    );
  }
}
