import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/home/information.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:jerrycan_master/app/widgets/general.dart';
import 'package:jerrycan_master/app/widgets/progress_loading.dart';

import '../../../../../../utils/constant.dart';
import '../controllers/information_detail_controller.dart';

class InformationDetailView extends GetView<InformationDetailController> {
  final InformationDetailController _controller =
      Get.put(InformationDetailController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => _controller.information.value.data != null
        ? Scaffold(
            appBar: defaultAppBar(
              _controller.information.value.title!,
              () => Get.back(),
              elevation: 1,
            ),
            body: _buildData(_controller.information.value.data!),
          )
        : Scaffold(body: ProgressLoading()));
  }

  Widget _buildData(List<Data> data) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: SingleChildScrollView(
        child: Column(
          children: [
            ...List.generate(data.length, (index) {
              Data section = data[index];
              String title = section.section!;
              return Column(
                children: [
                  title.isNotEmpty
                      ? Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: defaultText(title),
                        )
                      : Container(),
                  ...List.generate(section.content!.length, (index) {
                    Content content = section.content![index];
                    return Column(
                      children: [
                        _buildContent(content),
                      ],
                    );
                  }),
                ],
              );
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildContent(Content content) {
    String image = content.image!.isNotEmpty
        ? content.image!
        : "https://drive.google.com/uc?id=1uQEpfrVxNJ8DQT6MtbLeDRUTqKccNPRR";

    String desc = content.description ?? "";
    double height = desc.contains("Depo Agent adalah") ||
            desc.contains("Transporter bertugas")
        ? 80
        : 120;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        border: Border.all(color: COLOR_PRIMARY),
      ),
      child: Column(
        children: [
          CachedNetworkImage(
            imageUrl: image,
            height: height,
          ),
          SizedBox(height: 16),
          HtmlWidget(
            "<center class='content'>$desc<center/>",
            customStylesBuilder: (element) {
              if (element.classes.contains('content')) {
                return {
                  'color': '#4A5768',
                };
              }

              return null;
            },
          ),
        ],
      ),
    );
  }
}
