import 'dart:convert';

import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/home/information.dart';

class InformationDetailController extends GetxController {
  Rx<Information> information = Information(title: '', data: null).obs;

  @override
  void onInit() {
    super.onInit();
    _parseData();
  }

  void _parseData() {
    Future.delayed(Duration(seconds: 1), () {
      information.value = Information.fromJson(jsonDecode(Get.arguments));
    });
  }
}
