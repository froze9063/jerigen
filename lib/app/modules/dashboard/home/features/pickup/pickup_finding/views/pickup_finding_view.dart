import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../utils/constant.dart';
import '../controllers/pickup_finding_controller.dart';

class PickupFindingView extends GetView<PickupFindingController> {

  final PickupFindingController _controller = Get.put(PickupFindingController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            SizedBox(height: 100),
            Text(
              '${Get.arguments['liter']} Kilogram',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w600,
                color: COLOR_PRIMARY_TEXT,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/icons/ic_pickup.png", width: 90),
                  SizedBox(height: 16),
                  Text(
                    'Mencari Transporter',
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.normal,
                      color: COLOR_PRIMARY_TEXT,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                'Pencarian bisa memakan waktu hingga 1 jam\n\nJerigen akan memberikan notifikasi pada anda apabila Transporter ditemukan',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  color: COLOR_PRIMARY_TEXT,
                ),
              ),
            ),
            SizedBox(height: 30),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              height: 50,
              child: TextButton(
                child: Text(
                  'Batalkan',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: COLOR_DANGER,
                  enableFeedback: true,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                onPressed: _controller.cancel,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
