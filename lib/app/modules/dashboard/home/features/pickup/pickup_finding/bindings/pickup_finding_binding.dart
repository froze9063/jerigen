import 'package:get/get.dart';

import '../controllers/pickup_finding_controller.dart';

class PickupFindingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PickupFindingController>(
      () => PickupFindingController(),
    );
  }
}
