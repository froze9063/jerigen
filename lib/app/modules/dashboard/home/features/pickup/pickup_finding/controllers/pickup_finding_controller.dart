import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/widgets/custom_toast.dart';

import '../../../../../../../data/model/base_response.dart';
import '../../../../../../../data/model/trx/transporter_response.dart';
import '../../../../../../../data/repository/transaction_repository.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../../../utils/constant.dart';
import '../../../../../../../utils/failure.dart';

class PickupFindingController extends GetxController {
  TransactionRepository transactionRepository = TransactionRepository();

  late double _liter;
  late double _latitude;
  late double _longitude;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  void _init() {
    _liter = Get.arguments['liter'] ?? 0.0;
    _latitude = Get.arguments['latitude'] ?? 0.0;
    _longitude = Get.arguments['longitude'] ?? 0.0;

    _getTransportationList();
  }

  void cancel() {
    transactionRepository.cancelRequest();
    Get.back();
  }

  Future<void> _getTransportationList() async {
    try {
      List<TransporterResponse> data =
          await transactionRepository.getTransporterList(
        _liter.toString(),
        '1000000000000000000000',
        _latitude.toString(),
        _longitude.toString(),
      );

      _goToNext(jsonEncode(data));

    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    }
  }

  _goToNext(String transporterList) {
    Get.toNamed(Routes.PICKUP_FOUND, arguments: transporterList);
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }
}
