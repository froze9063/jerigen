import 'package:get/get.dart';

import '../controllers/pickup_transporter_controller.dart';

class PickupTransporterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PickupTransporterController>(
      () => PickupTransporterController(),
    );
  }
}
