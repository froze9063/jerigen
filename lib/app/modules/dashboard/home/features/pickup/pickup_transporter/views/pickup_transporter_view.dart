import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jerrycan_master/app/data/model/trx/transporter_response.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/pickup/pickup_transporter/views/widget/transporter_item.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../../../../utils/constant.dart';
import '../../../../../../../widgets/app_bar.dart';
import '../../../../../../../widgets/location_utils.dart';
import '../controllers/pickup_transporter_controller.dart';

class PickupTransporterView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PickupTransporterViewState();
}

class _PickupTransporterViewState extends State<PickupTransporterView> {
  final PickupTransporterController _pickupTransporterController =
      Get.put(PickupTransporterController());

  static const LatLng _center = const LatLng(-6.200000, 106.816666);
  static final CameraPosition _initCameraPosition = CameraPosition(
    target: LatLng(-6.200000, 106.816666),
    zoom: 14.4746,
  );

  LatLng _lastMapPosition = _center;
  Completer<GoogleMapController> _controller = Completer();
  final List<Marker> _markers = [];
  late GoogleMapController mapController;
  late BitmapDescriptor marker;

  @override
  void initState() {
    super.initState();

    BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      'assets/icons/ic_transporter.png',
    ).then((value) => marker = value);

    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => _openTransporterList());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Activity', () {
        Get.back();
      }),
      body: _buildMaps(context),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: COLOR_PRIMARY,
        onPressed: _openTransporterList,
        label: Text('Transporter List'),
        icon: Image.asset(
          "assets/icons/ic_pickup.png",
          width: 24,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _buildMaps(BuildContext context) {
    return GoogleMap(
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      onMapCreated: _onMapCreated,
      zoomControlsEnabled: false,
      markers: Set<Marker>.of(_markers),
      initialCameraPosition: _initCameraPosition,
      onCameraMove: _onCameraMove,
    );
  }

  // Future<void> getLocation() async {
  //   Position position = await LocationUtil.determinePosition();
  //   addMarker(
  //       LatLng(position.latitude, position.longitude), "My Location", "-");
  //   _updateCamera();
  // }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);

    _markers.clear();
    addMarker(LatLng(-6.330380, 106.816666), "Jagakarsa", "Jagakarsa");
    addMarker(LatLng(-6.402484, 106.794243), "Depok", "Depok");

    _updateCamera();
  }

  Future<void> addMarker(
    LatLng mLatLng,
    String mTitle,
    String mDescription,
  ) async {
    _markers.add(LocationUtil.addMarker(
      mLatLng,
      mTitle,
      mDescription,
      icon: marker,
    ));
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _updateCamera() {
    setState(() {
      LatLngBounds bound = LocationUtil.getBounds(_markers);
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);

      this.mapController.animateCamera(u2).then((void v) {
        LocationUtil.check(u2, this.mapController);
      });
    });
  }

  void _openTransporterList() {
    showCupertinoModalBottomSheet(
      context: context,
      // expand: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Obx(() {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 57,
                  height: 4,
                  decoration: BoxDecoration(
                    color: Color(0xFFDEDEDE),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                ),
                SizedBox(height: 32),
                ...List.generate(
                    _pickupTransporterController.listTransporter.length,
                    (index) {
                  TransporterResponse data =
                      _pickupTransporterController.listTransporter[index];
                  return TransporterItem(
                    transporter: data,
                    callback: () {
                      Get.back();
                      Get.toNamed(Routes.PICKUP_DELIVERED,
                          arguments: jsonEncode(data));
                    },
                    index: index + 1,
                  );
                }),
                SizedBox(height: 32),
                Text(
                  'Sisa',
                  style: TextStyle(
                    color: COLOR_PRIMARY_DARK_TEXT,
                    backgroundColor: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 24,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  '36 Kilogram',
                  style: TextStyle(
                    color: COLOR_PRIMARY_DARK_TEXT,
                    backgroundColor: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 32),
                Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      margin: const EdgeInsets.only(top: 8),
                      padding: const EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: COLOR_PRIMARY),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/icons/ic_pickup.png", width: 46),
                          SizedBox(width: 16),
                          Text(
                            'Mencari Transporter...',
                            style: TextStyle(
                              color: COLOR_PRIMARY_DARK_TEXT,
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 10,
                      child: Text(
                        '  Transporter ${_pickupTransporterController.listTransporter.length + 1}  ',
                        style: TextStyle(
                          color: COLOR_PRIMARY_TEXT,
                          backgroundColor: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 32),
              ],
            );
          }),
        ),
      ),
    );
  }
}
