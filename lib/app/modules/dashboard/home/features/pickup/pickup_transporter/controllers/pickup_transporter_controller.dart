import 'dart:convert';

import 'package:get/get.dart';

import '../../../../../../../data/model/trx/transporter_response.dart';

class PickupTransporterController extends GetxController {
  RxList<TransporterResponse> listTransporter = <TransporterResponse>[].obs;

  @override
  void onReady() {
    super.onReady();
    parseData();
  }

  void parseData() {
    listTransporter.value = (jsonDecode(Get.arguments) as List)
        .map((item) => TransporterResponse.fromJson(item))
        .toList();
  }
}
