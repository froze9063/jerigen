// Created by ferdyhaspin on 25/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/data/model/trx/transporter_response.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

import '../../../../../../../../data/model/trx/transporter.dart';

class TransporterItem extends StatelessWidget {
  final TransporterResponse transporter;
  final GestureTapCallback? callback;
  final int index;

  const TransporterItem({
    Key? key,
    required this.transporter,
    required this.callback,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callback,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 8),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: COLOR_PRIMARY),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/img_example_profile.png",
                  width: 56,
                  color: COLOR_PRIMARY,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        transporter.fullName ?? "",
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        '${transporter.type == 1 ? 'Motor' : 'Mobil'} - B 8613 UGH',
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.w700,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '(${transporter.pickupLiter} Kilogram)',
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        '${transporter.currentLiter} Kilogram Transported',
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/icons/ic_call.png",
                          width: 28,
                          color: COLOR_PRIMARY,
                        ),
                        SizedBox(width: 8),
                        Image.asset(
                          "assets/icons/ic_messages.png",
                          width: 28,
                          color: COLOR_PRIMARY,
                        ),
                      ],
                    ),
                    SizedBox(height: 16),
                    Text(
                      'Arriving in 10 minutes',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 8,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 10,
            child: Text(
              '  Transporter $index  ',
              style: TextStyle(
                color: COLOR_PRIMARY_TEXT,
                backgroundColor: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class TransporterItem2 extends StatelessWidget {
  final Transporter transporter;
  final GestureTapCallback? callback;

  const TransporterItem2({
    Key? key,
    required this.transporter,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callback,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 8),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: COLOR_PRIMARY),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/img_example_profile.png",
                  width: 56,
                  color: COLOR_PRIMARY,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        transporter.name,
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        transporter.type,
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.w700,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        transporter.pick,
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        transporter.total,
                        style: TextStyle(
                          color: COLOR_PRIMARY_DARK_TEXT,
                          fontWeight: FontWeight.normal,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/icons/ic_call.png",
                          width: 28,
                          color: COLOR_PRIMARY,
                        ),
                        SizedBox(width: 8),
                        Image.asset(
                          "assets/icons/ic_messages.png",
                          width: 28,
                          color: COLOR_PRIMARY,
                        ),
                      ],
                    ),
                    SizedBox(height: 16),
                    Text(
                      transporter.status,
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 8,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            right: 10,
            child: Text(
              '  ' + transporter.queue + '  ',
              style: TextStyle(
                color: COLOR_PRIMARY_TEXT,
                backgroundColor: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
