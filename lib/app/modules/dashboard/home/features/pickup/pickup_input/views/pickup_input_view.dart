import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/pickup/pickup_input/views/widget/pickup_input_map.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/utils.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:jerrycan_master/app/widgets/general.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../../../../data/model/master/address.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../../../utils/singleton.dart';
import '../../../../../../../widgets/button.dart';
import '../controllers/pickup_input_controller.dart';
import 'widget/saved_address_item.dart';

class PickupInputView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PickupInputViewState();
}

class _PickupInputViewState extends State<PickupInputView> {
  final PickupInputController _controller = Get.put(PickupInputController());

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) => _showInfoLocation());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Pick Up', () {
        Get.back();
      }),
      body: LayoutBuilder(
        builder: (context, constraint) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: IntrinsicHeight(
                child: Column(
                  children: [
                    _buildInput(context),
                    Expanded(child: PickupInputMap()),
                    Obx(() {
                      if (_controller.isRequest.value) {
                        return _buildRequest(context);
                      } else {
                        return Container();
                      }
                    }),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildInput(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey.withOpacity(0.5)),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              height: 60,
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 12),
              child: TextButton.icon(
                style: ButtonStyle(alignment: Alignment.centerLeft),
                icon: Image.asset(
                  "assets/icons/ic_pickup_add.png",
                  width: 19,
                ),
                label: Obx(
                  () {
                    String text = "Pilih Alamat";
                    if (_controller.addressData.value.addressDesc != null) {
                      List<String> addressDesc =
                          (_controller.addressData.value.addressDesc ?? " - ")
                              .split(" - ");
                      text = '${addressDesc[0]}\nDetail : ${addressDesc[1]}'
                          .trim();
                    }
                    return Text(
                      text,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: COLOR_PRIMARY_TEXT,
                      ),
                    );
                  },
                ),
                onPressed: _showDialogAddress,
              ),
            ),
            Container(
              width: double.infinity,
              height: 1,
              color: Colors.grey.withOpacity(0.5),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 12),
                  child: TextButton.icon(
                    style: ButtonStyle(alignment: Alignment.topLeft),
                    icon: Image.asset(
                      "assets/icons/ic_pickup_calendar.png",
                      width: 19,
                    ),
                    label: Obx(
                      () => Text(
                        _controller.pickupTime.value,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: COLOR_PRIMARY_TEXT,
                        ),
                      ),
                    ),
                    onPressed: () {
                      _controller.setPickUp(context);
                    },
                  ),
                ),
                Container(
                  width: 1,
                  height: 60,
                  color: Colors.grey.withOpacity(0.5),
                ),
                Expanded(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(width: 5),
                      Image.asset(
                        "assets/icons/ic_pickup_jerigen.png",
                        width: 19,
                      ),
                      IconButton(
                        icon: Icon(Icons.remove, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller
                              .calculate(PickupInputController.typeMinus);
                        },
                      ),
                      generalCalculateTextForm(_controller.controllerLiter,
                          (value) {
                        double data = value.isEmpty ? 0 : double.parse(value);
                        _controller.liter.value = data;
                        _controller.checkSubmit();
                      }),
                      IconButton(
                        icon: Icon(Icons.add, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller.calculate(PickupInputController.typeAdd);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRequest(context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      child: Column(
        children: [
          Container(
            width: 57,
            height: 4,
            decoration: BoxDecoration(
              color: Color(0xFFDEDEDE),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
          SizedBox(height: 16),
          Text(
            'Potensi Poin',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12),
          ),
          SizedBox(height: 8),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(() => Text(
                    "${Utils.toIDR(_controller.point.value)} Poin",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF2C3F58)),
                  )),
              SizedBox(width: 20),
              Image.asset("assets/icons/ic_coin.png", width: 24, height: 24),
            ],
          ),
          SizedBox(height: 8),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: COLOR_PRIMARY),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Text(
                    "Donasi",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF092E36)),
                  ),
                ),
                Container(width: 1, height: 55, color: COLOR_PRIMARY),
                Expanded(
                  flex: 4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(Icons.remove, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller
                              .calculateDonate(PickupInputController.typeMinus);
                        },
                      ),
                      generalCalculateTextForm(_controller.controllerDonation,
                          (value) {
                        double data = value.isEmpty ? 0 : double.parse(value);
                        _controller.donate.value = data;
                        _controller.checkSubmit();
                      }, donate: true),
                      IconButton(
                        icon: Icon(Icons.add, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller
                              .calculateDonate(PickupInputController.typeAdd);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 16),
          Obx(() => TextButtonPrimary(
                isEnable: _controller.canSubmit.value,
                text: 'Submit',
                onPressed: _controller.doRequest,
              )),
        ],
      ),
    );
  }

  void _showDialogAddress() {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 57,
                height: 4,
                decoration: BoxDecoration(
                  color: Color(0xFFDEDEDE),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
              SizedBox(height: 32),
              ...List.generate(Singleton.addresses.length, (index) {
                Address data = Singleton.addresses[index];
                List<String> addressDesc =
                    (data.addressDesc ?? " - ").split(" - ");
                return SavedAddressItem(
                  title: 'Alamat Tersimpat ${index + 1}',
                  label: '${addressDesc[0]}\nDetail : ${addressDesc[1]}'.trim(),
                  callback: () => _controller.setAddress(data),
                );
              }),
              SizedBox(height: 20),
              TextButtonPrimary(
                isEnable: true,
                text: "Tambah Alamat",
                onPressed: () {
                  Get.toNamed(Routes.ADD_ADDRESS);
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showInfoLocation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.all(0),
          content: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: Get.width,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  border: Border.all(color: COLOR_PRIMARY),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      "assets/icons/ic_pickup_popup.png",
                      height: 160,
                    ),
                    SizedBox(height: 16),
                    HtmlWidget(
                      """<center class='content'><b>Jakarta Greater Area</b></br></br>Untuk saat ini, Jerigen baru beroperasi di wilayah Kota Jakarta Timur dan Kota Bekasi. Untuk wilayah Jabodetabek dan kota-kota lainnya akan segera menyusul dalam waktu dekat.<center/>""",
                      customStylesBuilder: (element) {
                        if (element.classes.contains('content')) {
                          return {
                            'color': '#4A5768',
                          };
                        }

                        return null;
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButtonPrimary(
              isEnable: true,
              text: 'Lanjut',
              onPressed: () => Get.back(),
            )
          ],
        );
      },
    );
  }
}
