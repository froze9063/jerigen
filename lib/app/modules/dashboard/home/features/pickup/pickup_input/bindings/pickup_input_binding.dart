import 'package:get/get.dart';

import '../controllers/pickup_input_controller.dart';

class PickupInputBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PickupInputController>(
      () => PickupInputController(),
    );
  }
}
