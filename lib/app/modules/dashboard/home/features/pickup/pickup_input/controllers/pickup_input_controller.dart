import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jerrycan_master/app/data/model/master/address.dart';

import '../../../../../../../routes/app_pages.dart';
import '../../../../../../../widgets/custom_toast.dart';
import '../../../../../../../widgets/general.dart';

class PickupInputController extends GetxController {
  static String typeAdd = "ADD";
  static String typeMinus = "MINUS";

  final TextEditingController controllerLiter =
      TextEditingController(text: "0");
  final TextEditingController controllerDonation =
      TextEditingController(text: "0");

  var pickupTime = "Waktu Jemput".obs;
  var addressData = Address().obs;

  RxDouble liter = 0.0.obs;
  RxDouble point = 0.0.obs;
  RxDouble donate = 0.0.obs;

  var isRequest = false.obs;
  var canSubmit = false.obs;

  void checkSubmit() {
    if (!isRequest.value) isRequest.value = true;

    point.value = liter.value * 5000;
    canSubmit.value = liter.value >= 5 &&
        addressData.value.addressDesc != null &&
        pickupTime.value != "Waktu Jemput";

    if (point.value <= donate.value) {
      donate.value = point.value;
      controllerDonation.text = '${donate.value}';
      toEndCursor(controllerDonation);
    }
  }

  void calculate(String type) {
    if (type == typeAdd) {
      liter.value++;
    } else {
      if (liter > 0) liter.value--;
    }
    controllerLiter.text = '${liter.value}';
    checkSubmit();
  }

  void calculateDonate(String type) {
    if (type == typeAdd) {
      if (point.value <= donate.value) {
        CustomToast.showToast(
            'Mohon maaf poin anda tidak cukup untuk melakukan donasi.');
      } else {
        donate.value = donate.value + 1000;
      }
    } else {
      if (donate > 0) donate.value = donate.value - 1000;
    }
    if (donate.value < 0) donate.value = 0;
    controllerDonation.text = '${donate.value}';
  }

  Future<void> setPickUp(BuildContext context) async {
    String _addLeadingZeroIfNeeded(int value) {
      if (value < 10) return '0$value';
      return value.toString();
    }

    var current = DateTime.now();
    var dayMin = current.day;
    var timeMin = current.minute < 6 ? 6 : current.minute;
    if (timeMin > 18) {
      dayMin = dayMin + 1;
      timeMin = 6;
    }

    var minDate = DateTime(current.year, current.month, dayMin, timeMin);
    var maxDate = DateTime(current.year, current.month, dayMin + 2, 18);

    DatePicker.showDateTimePicker(context,
        showTitleActions: true,
        minTime: minDate,
        maxTime: maxDate, onChanged: (date) {
      print('change $date in time zone ' +
          date.timeZoneOffset.inHours.toString());
    }, onConfirm: (date) {
      if (date.hour < 6 || date.hour > 18) {
        CustomToast.showToast('Waktu penjemputan pukul 06:00 - 18:00');
        return;
      }
      checkSubmit();
      var nextHour = date.add(Duration(hours: 3));
      pickupTime.value = '${DateFormat('E, dd/MM').format(date)}\n'
          '${_addLeadingZeroIfNeeded(date.hour)}.${_addLeadingZeroIfNeeded(date.minute)} - '
          '${_addLeadingZeroIfNeeded(nextHour.hour)}.${_addLeadingZeroIfNeeded(nextHour.minute)}';
    }, locale: LocaleType.id);
  }

  setAddress(Address data) {
    addressData.value = data;
  }

  void doRequest() {
    var data = {
      "liter": liter.value,
      "donation": donate.value * 1000,
      "points": point.value,
      "latitude": addressData.value.longitude,
      "longitude": addressData.value.longitude,
    };

    Get.toNamed(Routes.PICKUP_FINDING, arguments: data);
  }
}
