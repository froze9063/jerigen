// Created by ferdyhaspin on 25/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class SavedAddressItem extends StatelessWidget {
  final String title;
  final String label;
  final Function callback;

  const SavedAddressItem({
    Key? key,
    required this.title,
    required this.label,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: InkWell(
        onTap: () {
          Get.back();
          callback();
        },
        child: TextFormField(
          // expands: true,
          maxLines: 3,
          minLines: 1,
          initialValue: label,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),

          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(16),
            enabled: false,
            labelText: title,
            labelStyle: TextStyle(color: COLOR_PRIMARY_DARK, fontSize: 12),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: COLOR_PRIMARY_DARK),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
        ),
      ),
    );
  }
}
