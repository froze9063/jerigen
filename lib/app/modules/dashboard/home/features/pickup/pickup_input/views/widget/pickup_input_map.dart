// Created by ferdyhaspin on 29/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';

import '../../../../../../../../data/model/master/address.dart';
import '../../../../../../../../widgets/location_utils.dart';
import '../../controllers/pickup_input_controller.dart';

class PickupInputMap extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PickupInputMapState();
}

class _PickupInputMapState extends State<PickupInputMap> {
  final PickupInputController _controllerWidget = Get.find();
  bool _moveCamera = true;

  Completer<GoogleMapController> _controller = Completer();
  final List<Marker> _markers = [];
  late GoogleMapController mapController;

  @override
  void initState() {
    super.initState();
    getLocation();
    _goToAddress();
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
        new Factory<OneSequenceGestureRecognizer>(
          () => new EagerGestureRecognizer(),
        ),
      ].toSet(),
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      onMapCreated: _onMapCreated,
      zoomControlsEnabled: false,
      markers: Set<Marker>.of(_markers),
      initialCameraPosition: LocationUtil.initCameraPosition,
      onCameraMove: _onCameraMove,
    );
  }

  Future<void> getLocation() async {
    // Position position = await LocationUtil.determinePosition();
    // addMarker(
    //     LatLng(position.latitude, position.longitude), "My Location", "-");
    _updateCamera();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);

    _markers.clear();
    Singleton.addresses.forEach((element) {
      List<String> desc = (element.addressDesc ?? " - ").split(" - ");
      addMarker(
        LatLng(element.latitude ?? 0.0, element.longitude ?? 0.0),
        desc[1],
        desc[0],
      );
    });
    _updateCamera();
  }

  void addMarker(LatLng mLatLng, String mTitle, String mDescription) {
    _markers
        .add(LocationUtil.addMarker(mLatLng, mTitle, mDescription, onTap: () {
      _moveCamera = false;
      _controllerWidget.addressData.value = Address(
        latitude: mLatLng.longitude,
        longitude: mLatLng.longitude,
        addressDesc: "$mDescription - $mTitle",
      );
    }));
  }

  void _onCameraMove(CameraPosition position) {}

  Future<void> _goToAddress() async {
    _controllerWidget.addressData.listen((p0) async {
      _controllerWidget.checkSubmit();
      if (_moveCamera) {
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(p0.latitude ?? 0.0, p0.longitude ?? 0.0),
                zoom: 19.151926040649414),
          ),
        );
      }
    });
  }

  void _updateCamera() {
    setState(() {
      LatLngBounds bound = LocationUtil.getBounds(_markers);
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);

      this.mapController.animateCamera(u2).then((void v) {
        LocationUtil.check(u2, this.mapController);
      });
    });
  }
}
