import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/src/widgets/editable_text.dart';
import 'package:get/get.dart';

import '../../../../../../../data/model/base_response.dart';
import '../../../../../../../data/model/trx/create_trx_request.dart';
import '../../../../../../../data/model/trx/create_trx_response.dart';
import '../../../../../../../data/model/trx/transporter_response.dart';
import '../../../../../../../data/repository/transaction_repository.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../../../utils/constant.dart';
import '../../../../../../../utils/failure.dart';
import '../../../../../../../widgets/custom_toast.dart';
import '../../../../../../../widgets/general.dart';

class PickupDeliveredController extends GetxController {
  final TransactionRepository transactionRepository = TransactionRepository();

  final TextEditingController controllerDonation =
      TextEditingController(text: "0");

  var liter = 0.0.obs;
  var point = 0.0.obs;
  var donate = 0.0.obs;

  var isLoading = false.obs;

  var transporter = TransporterResponse().obs;

  void checkSubmit() {
    if (point.value <= donate.value) {
      donate.value = point.value;
      controllerDonation.text = '${donate.value}';
      toEndCursor(controllerDonation);
    }
  }

  @override
  void onReady() {
    super.onReady();
    parseData();
  }

  void parseData() {
    transporter.value = TransporterResponse.fromJson(jsonDecode(Get.arguments));
    liter.value = transporter.value.pickupLiter?.toDouble() ?? 0.0;
    point.value = liter.value * 5000;
  }

  void calculateDonate(String type) {
    if (type == typeAdd) {
      if (point.value <= donate.value) {
        CustomToast.showToast(
            'Mohon maaf poin anda tidak cukup untuk melakukan donasi.');
      } else {
        donate.value = donate.value + 1000;
      }
    } else {
      if (donate > 0) donate.value = donate.value - 1000;
    }
    if (donate.value < 0) donate.value = 0;
    controllerDonation.text = '${donate.value}';
  }

  Future<void> createTrx() async {
    try {
      _updateIsLoading(true);
      CreateTrxRequest body = CreateTrxRequest(
        countLiter: liter.value,
        points: point.value,
        pointsDonation: donate.value * 1000,
        transportationId: ['${transporter.value.id}'],
      );
      CreateTrxResponse data = await transactionRepository.createTrx(body);
      print(data);

      Get.offAllNamed(Routes.DASHBOARD);
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }
}
