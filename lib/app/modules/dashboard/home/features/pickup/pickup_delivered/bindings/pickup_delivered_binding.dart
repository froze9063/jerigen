import 'package:get/get.dart';

import '../controllers/pickup_delivered_controller.dart';

class PickupDeliveredBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PickupDeliveredController>(
      () => PickupDeliveredController(),
    );
  }
}
