import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/utils.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:jerrycan_master/app/widgets/progress_loading.dart';

import '../../../../../../../utils/constant.dart';
import '../../../../../../../widgets/button.dart';
import '../../../../../../../widgets/general.dart';
import '../controllers/pickup_delivered_controller.dart';

class PickupDeliveredView extends GetView<PickupDeliveredController> {
  final _controller = Get.put(PickupDeliveredController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('', () {
        Get.back();
      }),
      body: Obx(() {
        if(_controller.isLoading.value){
          return ProgressLoading();
        }else{
          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  Image.asset(
                    "assets/images/img_example_profile.png",
                    width: 56,
                    color: COLOR_PRIMARY,
                  ),
                  SizedBox(height: 16),
                  Text(
                    '${_controller.transporter.value.fullName}',
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '${_controller.transporter.value.type == 1 ? 'Motor' : 'Mobil'} - B 8613 UGH',
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      fontWeight: FontWeight.w700,
                      fontSize: 10,
                    ),
                  ),
                  Text(
                    '(${_controller.liter} Kilogram)',
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      fontWeight: FontWeight.normal,
                      fontSize: 10,
                    ),
                  ),
                  Text(
                    '${_controller.transporter.value.currentLiter} Kilogram Transported',
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      fontWeight: FontWeight.normal,
                      fontSize: 10,
                    ),
                  ),
                  SizedBox(height: 16),
                  Text(
                    '${_controller.liter} Kilogram',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      backgroundColor: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 16),
                  Text(
                    'Penjemputan Sukses',
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      backgroundColor: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(height: 16),
                  Image.asset("assets/icons/ic_pickup.png", width: 72),
                  SizedBox(height: 16),
                  SizedBox(height: 8),
                  SizedBox(height: 16),
                  Text(
                    'Total Poin',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  SizedBox(height: 16),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '${Utils.toIDR(_controller.point.value)}',
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF2C3F58)),
                      ),
                      SizedBox(width: 20),
                      Image.asset("assets/icons/ic_coin.png",
                          width: 24, height: 24),
                    ],
                  ),
                  SizedBox(height: 16),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(width: 1, color: COLOR_PRIMARY),
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Text(
                            "Tip",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFF092E36)),
                          ),
                        ),
                        Container(width: 1, height: 45, color: COLOR_PRIMARY),
                        Expanded(
                          flex: 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: Icon(Icons.remove, size: 12),
                                onPressed: () {
                                  unFocus(context);
                                  _controller.calculateDonate(typeMinus);
                                },
                              ),
                              generalCalculateTextForm(
                                _controller.controllerDonation,
                                    (value) {
                                  double data = value.isEmpty ? 0 : double.parse(value);
                                  _controller.donate.value = data;
                                  controller.checkSubmit();
                                },
                                donate: true,
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                              IconButton(
                                icon: Icon(Icons.add, size: 12),
                                onPressed: () {
                                  unFocus(context);
                                  _controller.calculateDonate(typeAdd);
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  TextButtonPrimary(
                    isEnable: true,
                    text: 'Konfirmasi',
                    onPressed: () => _controller.createTrx(),
                  ),
                  SizedBox(height: 16),
                  Text(
                    'Terimakasih sudah membantu menyelamatkan bumi, Agen Perubahan!\n\nStaf Depo kami akan menimbang minyak jelantah anda secara manual. Poin akan bertambah otomatis ke akun anda. ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                      color: COLOR_PRIMARY_TEXT,
                    ),
                  ),
                ],
              ),
            ),
          );
        }
      }),
    );
  }
}
