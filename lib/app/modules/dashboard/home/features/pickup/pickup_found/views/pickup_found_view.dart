import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../utils/constant.dart';
import '../controllers/pickup_found_controller.dart';

class PickupFoundView extends GetView<PickupFoundController> {
  final PickupFoundController _controller = Get.put(PickupFoundController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Transporter Ditemukan!',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.normal,
                color: COLOR_PRIMARY_DARK_TEXT,
              ),
            ),
            SizedBox(height: 16),
            Image.asset("assets/icons/ic_pickup.png", width: 90),
          ],
        ),
      ),
    );
  }
}
