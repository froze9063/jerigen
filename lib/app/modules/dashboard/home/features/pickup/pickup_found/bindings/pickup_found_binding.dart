import 'package:get/get.dart';

import '../controllers/pickup_found_controller.dart';

class PickupFoundBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PickupFoundController>(
      () => PickupFoundController(),
    );
  }
}
