import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../routes/app_pages.dart';

class PickupFoundController extends GetxController {
  @override
  void onReady() {
    super.onReady();
    Timer(Duration(seconds: 2), () async {
      Get.offNamedUntil(
        Routes.PICKUP_TRANSPORTER,
        ModalRoute.withName(Routes.DASHBOARD),
        arguments: Get.arguments,
      );
    });
  }
}
