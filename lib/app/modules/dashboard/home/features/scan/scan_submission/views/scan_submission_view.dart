import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../widgets/button.dart';
import '../controllers/scan_submission_controller.dart';

class ScanSubmissionView extends GetView<ScanSubmissionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Scan Succeed!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 16),
                  Image.asset("assets/icons/ic_scan.png", width: 62),
                  SizedBox(height: 16),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text(
                      'Terimakasih sudah membantu menyelamatkan bumi, Agen Perubahan! \n\nStaf Depo kami akan menimbang minyak jelantah anda secara manual. Poin akan bertambah otomatis ke akun anda. ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextButtonPrimary(
                  isEnable: true,
                  text: 'Selesai',
                  onPressed: () {
                    Get.back();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
