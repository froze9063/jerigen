import 'package:get/get.dart';

import '../controllers/scan_submission_controller.dart';

class ScanSubmissionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScanSubmissionController>(
      () => ScanSubmissionController(),
    );
  }
}
