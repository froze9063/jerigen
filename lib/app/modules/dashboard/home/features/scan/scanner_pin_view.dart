// Created by ferdyhaspin on 04/12/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:pinput/pin_put/pin_put.dart';

import '../../../../../routes/app_pages.dart';

class ScannerPinView extends StatefulWidget {
  @override
  _ScannerPinViewState createState() => _ScannerPinViewState();
}

class _ScannerPinViewState extends State<ScannerPinView> {
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final BoxDecoration pinPutDecoration = BoxDecoration(
      color: const Color.fromRGBO(235, 236, 237, 1),
      borderRadius: BorderRadius.circular(5.0),
    );
    return Scaffold(
      appBar: defaultAppBar('Scan Barcode', () => Get.back(), elevation: 2),
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Masukkan PIN',
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
            SizedBox(height: 30),
            PinPut(
              validator: (s) {
                if ((s ?? "1").contains('1')) return null;
                return 'NOT VALID';
              },
              onSubmit: (String pin) {
                Get.toNamed(Routes.SCAN_INPUT, arguments: Get.arguments);
              },
              autovalidateMode: AutovalidateMode.always,
              withCursor: true,
              fieldsCount: 4,
              fieldsAlignment: MainAxisAlignment.center,
              textStyle: const TextStyle(fontSize: 25.0, color: Colors.black),
              eachFieldMargin: EdgeInsets.all(12),
              eachFieldWidth: 55.0,
              eachFieldHeight: 55.0,
              focusNode: _pinPutFocusNode,
              controller: _pinPutController,
              submittedFieldDecoration: pinPutDecoration,
              selectedFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 2,
                  color: COLOR_PRIMARY,
                ),
              ),
              followingFieldDecoration: pinPutDecoration,
              pinAnimationType: PinAnimationType.scale,
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
