import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/widgets/custom_toast.dart';

import '../../../../../../../data/model/base_response.dart';
import '../../../../../../../data/model/home/depo.dart';
import '../../../../../../../data/model/trx/create_trx_request.dart';
import '../../../../../../../data/model/trx/create_trx_response.dart';
import '../../../../../../../data/repository/transaction_repository.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../../../utils/constant.dart';
import '../../../../../../../utils/failure.dart';
import '../../../../../../../utils/singleton.dart';
import '../../../../../../../widgets/general.dart';

class ScanInputController extends GetxController {
  static String typeAdd = "ADD";
  static String typeMinus = "MINUS";

  TransactionRepository transactionRepository = TransactionRepository();
  final TextEditingController controllerLiter =
  TextEditingController(text: "0");
  final TextEditingController controllerDonation =
  TextEditingController(text: "0");

  var liter = 0.0.obs;
  var point = 0.0.obs;
  var donate = 0.0.obs;

  var canSubmit = false.obs;
  var isLoading = false.obs;

  Rx<Depo> depo = Depo().obs;

  @override
  void onReady() {
    super.onReady();
    _getDepoById();
  }

  void checkSubmit() {
    point.value = liter.value * 5000;
    canSubmit.value = liter.value >= 1;

    if (point.value <= donate.value) {
      donate.value = point.value;
      controllerDonation.text = '${donate.value}';
      toEndCursor(controllerDonation);
    }
  }

  void calculate(String type) {
    if (type == typeAdd) {
      liter.value++;
    } else {
      if (liter > 0) liter.value--;
    }
    controllerLiter.text = '${liter.value}';
    checkSubmit();
  }

  void calculateDonate(String type) {
    if (type == typeAdd) {
      if (point.value <= donate.value) {
        CustomToast.showToast(
            'Mohon maaf poin anda tidak cukup untuk melakukan donasi.');
      } else {
        donate.value = donate.value + 1000;
      }
    } else {
      if (donate > 0) donate.value = donate.value - 1000;
    }
    if(donate.value < 0) donate.value = 0;
    controllerDonation.text = '${donate.value}';
  }

  Future<void> _getDepoById() async {
    try {
      _updateIsLoading(true);
      String id = Get.arguments;
      depo.value = await transactionRepository.scanDepo(id);
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessageBack(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessageBack(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessageBack(e.toString());
    } on RequestException catch (e) {
      showMessageBack(e.code!);
    } catch (e) {
      showMessageBack(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  Future<void> createTrx() async {
    try {
      _updateIsLoading(true);
      CreateTrxRequest body = CreateTrxRequest(
        countLiter: liter.value,
        depoId: depo.value.id,
        points: point.value,
        pointsDonation: donate.value * 1000,
      );
      CreateTrxResponse data = await transactionRepository.createTrx(body);
      print(data);
      Get.offNamedUntil(
        Routes.SCAN_SUBMISSION,
        ModalRoute.withName(Routes.DASHBOARD),
      );
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }

  void showMessageBack(String message) {
    showMessage(message);
    Get.back();
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }
}
