import 'package:get/get.dart';

import '../controllers/scan_input_controller.dart';

class ScanInputBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScanInputController>(
      () => ScanInputController(),
    );
  }
}
