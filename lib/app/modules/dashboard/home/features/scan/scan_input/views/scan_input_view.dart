import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/utils.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';
import 'package:jerrycan_master/app/widgets/button.dart';
import 'package:jerrycan_master/app/widgets/progress_loading.dart';

import '../../../../../../../data/model/home/depo.dart';
import '../../../../../../../widgets/general.dart';
import '../controllers/scan_input_controller.dart';

class ScanInputView extends GetView<ScanInputController> {
  final ScanInputController _controller = Get.put(ScanInputController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Scan', () {
        Get.back();
      }),
      body: Obx(() {
        if (_controller.isLoading.value) {
          return ProgressLoading();
        } else {
          return SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(context),
                SizedBox(height: 16),
                _buildContent(context, _controller.depo.value),
              ],
            ),
          );
        }
      }),
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3.0,
            offset: Offset(0.0, 2),
          )
        ],
        color: Colors.white,
      ),
      padding: const EdgeInsets.all(16),
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Image.asset("assets/icons/ic_jerigen_grey.png", width: 36),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Icon(Icons.remove),
                    onPressed: () {
                      unFocus(context);
                      _controller.calculate(ScanInputController.typeMinus);
                    },
                  ),
                  generalCalculateTextForm(
                      _controller.controllerLiter,
                          (value) {
                        double data = value.isEmpty ? 0 : double.parse(value);
                        _controller.liter.value = data;
                        _controller.checkSubmit();
                      },
                      fontSize: 22,
                      fontWeight: FontWeight.bold
                  ),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      unFocus(context);
                      _controller.calculate(ScanInputController.typeAdd);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, Depo data) {
    String location = data.alamat ?? "";
    String image = data.profilePict ?? "";
    String name = data.depoName ?? "";
    String hp = data.noTelephone ?? "";

    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          SizedBox(height: 16),
          Text(
            'Scan Succeed!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 36),
          Text(
            'Anda berada di:',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 24),
          CachedNetworkImage(
            width: 195,
            imageUrl: image,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                ProgressLoadingWithValue(value: downloadProgress.progress),
          ),
          SizedBox(height: 24),
          Text(
            name,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 8),
          Text(
            '$location\n$hp',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(height: 16),
          Container(
            width: 57,
            height: 4,
            decoration: BoxDecoration(
              color: Color(0xFFDEDEDE),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
          SizedBox(height: 24),
          Text(
            'Potensi Poin',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12),
          ),
          SizedBox(height: 16),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(() {
                return Text(
                  "${Utils.toIDR(_controller.point.value)} Poin",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF2C3F58)),
                );
              }),
              SizedBox(width: 20),
              Image.asset("assets/icons/ic_coin.png", width: 24, height: 24),
            ],
          ),
          SizedBox(height: 16),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(width: 1, color: COLOR_PRIMARY),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Text(
                    "Donasi",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF092E36)),
                  ),
                ),
                Container(width: 1, height: 45, color: COLOR_PRIMARY),
                Expanded(
                  flex: 4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(Icons.remove, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller
                              .calculateDonate(ScanInputController.typeMinus);
                        },
                      ),
                      generalCalculateTextForm(
                        _controller.controllerDonation,
                        (value) {
                          double data = value.isEmpty ? 0 : double.parse(value);
                          _controller.donate.value = data;
                          controller.checkSubmit();
                        },
                        donate: true,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                      IconButton(
                        icon: Icon(Icons.add, size: 12),
                        onPressed: () {
                          unFocus(context);
                          _controller
                              .calculateDonate(ScanInputController.typeAdd);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 24),
          Obx(() => TextButtonPrimary(
                isEnable: _controller.canSubmit.value,
                text: 'Submit',
                onPressed: _controller.createTrx,
              )),
        ],
      ),
    );
  }
}
