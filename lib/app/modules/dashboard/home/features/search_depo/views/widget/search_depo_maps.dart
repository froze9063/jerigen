// Created by ferdyhaspin on 26/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../../../../widgets/location_utils.dart';

class SearchDepoMap extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchDepoMapState();
}

class _SearchDepoMapState extends State<SearchDepoMap> {
  static const LatLng _center = const LatLng(-6.200000, 106.816666);
  static final CameraPosition _initCameraPosition = CameraPosition(
    target: LatLng(-6.200000, 106.816666),
    zoom: 14.4746,
  );

  Completer<GoogleMapController> _controller = Completer();
  final List<Marker> _markers = [];
  late GoogleMapController mapController;
  late BitmapDescriptor marker;

  @override
  initState() {
    super.initState();
    _addMarkerIcon();
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      onMapCreated: _onMapCreated,
      zoomControlsEnabled: false,
      markers: Set<Marker>.of(_markers),
      initialCameraPosition: _initCameraPosition,
      onCameraMove: _onCameraMove,
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);

    _markers.clear();
    addMarker(LatLng(-6.330380, 106.816666), "Jagakarsa", "Jagakarsa");
    addMarker(LatLng(-6.402484, 106.794243), "Depok", "Depok");

    _updateCamera();
  }

  Future<void> addMarker(LatLng latlng, String title, String desc) async {
    _markers.add(LocationUtil.addMarker(latlng, title, desc, icon: marker));
  }

  void _onCameraMove(CameraPosition position) {
    // _lastMapPosition = position.target;
    // final coordinates = new Coordinates(
    //     myLocation.latitude, myLocation.longitude);
    // var addresses = await Geocoder.local.findAddressesFromCoordinates(
    //     coordinates);
    // var first = addresses.first;
    // print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    // setState(() {
    //   addMarker(
    //       LatLng(_lastMapPosition.latitude, _lastMapPosition.longitude), "My Location", "-");
    // });
  }

  void _updateCamera() {
    setState(() {
      LatLngBounds bound = LocationUtil.getBounds(_markers);
      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);

      this.mapController.animateCamera(u2).then((void v) {
        LocationUtil.check(u2, this.mapController);
      });
    });
  }

  Future<void> _addMarkerIcon() async {
    final Uint8List? markerIcon =
        await getBytesFromAsset('assets/icons/ic_marker_depo.png', 120);
    if (markerIcon != null) marker =  BitmapDescriptor.fromBytes(markerIcon);
  }

  Future<Uint8List?> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        ?.buffer
        .asUint8List();
  }
}
