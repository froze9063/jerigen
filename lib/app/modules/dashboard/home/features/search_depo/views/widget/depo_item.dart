// Created by ferdyhaspin on 26/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/data/model/home/depo.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

import '../../../../../../../widgets/progress_loading.dart';

class DepoItem extends StatelessWidget {
  final Depo depo;
  final Function(Depo) callback;

  const DepoItem({Key? key, required this.depo, required this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String pic = depo.profilePict ?? "";
    String name = depo.depoName ?? "";
    return InkWell(
      onTap: () {
        callback(depo);
      },
      child: Column(
        children: [
          SizedBox(height: 16),
          Container(height: 1, color: COLOR_PRIMARY),
          SizedBox(height: 16),
          Row(
            children: [
              CachedNetworkImage(
                width: 120,
                imageUrl: pic,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    ProgressLoadingWithValue(value: downloadProgress.progress),
              ),
              SizedBox(width: 16),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          color: COLOR_PRIMARY_TEXT,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        '${depo.alamat}\n${depo.noTelephone}',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
