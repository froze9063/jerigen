import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/data/model/home/depo.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/features/search_depo/views/widget/search_depo_maps.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../../utils/constant.dart';
import '../../../../../../widgets/app_bar.dart';
import '../../../../../../widgets/button.dart';
import '../../../../../../widgets/progress_loading.dart';
import '../controllers/search_depo_controller.dart';
import 'widget/depo_item.dart';
// import 'package:url_launcher/url_launcher.dart';

class SearchDepoView extends GetView<SearchDepoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Cari Depo', () {
        Get.back();
      }),
      body: Column(
        children: [
          _buildInput(context),
          Expanded(child: SearchDepoMap()),
        ],
      ),
    );
  }

  Widget _buildInput(BuildContext context) {
    OutlineInputBorder border = OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      borderSide: BorderSide(width: 1, color: Colors.grey),
    );
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Card(
        elevation: 1,
        child: TextFormField(
          onEditingComplete: () {
            // launch();
            _showSearchResult(context);
          },
          textInputAction: TextInputAction.search,
          decoration: InputDecoration(
            border: border,
            focusedBorder: border,
            prefixIcon: Icon(Icons.search),
            prefixIconColor: Colors.grey,
            focusColor: Colors.grey,
            hintText: 'Cari depo Jerigen terdekat',
            hintStyle: TextStyle(
              fontSize: 14,
              color: Color.fromRGBO(182, 182, 182, 1.0),
            ),
          ),
        ),
      ),
    );
  }

  void _showSearchResult(context) {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 57,
                height: 4,
                decoration: BoxDecoration(
                  color: Color(0xFFDEDEDE),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
              SizedBox(height: 32),
              DepoItem(
                depo: Depo(
                  depoName: 'Depo Pak Udin',
                  alamat: 'Jl. Kemanggisan Ilir No. 7 Palmerah - Jakarta Barat',
                  noTelephone: '021 8972-3095\n0812-9732-8712',
                  profilePict:
                      'https://blog.amartha.com/wp-content/uploads/2020/12/Ini-Tips-Sukses-Membuka-Toko-Kelontong-Modal-Kecil-Banyak-Untung.png',
                ),
                callback: (item) {
                  _showDepoDetail(context, item);
                },
              ),
              DepoItem(
                depo: Depo(
                  depoName: 'Depo Pak Udin',
                  alamat: 'Jl. Kemanggisan Ilir No. 7 Palmerah - Jakarta Barat',
                  noTelephone: '021 8972-3095\n0812-9732-8712',
                  profilePict:
                      'https://blog.amartha.com/wp-content/uploads/2020/12/Ini-Tips-Sukses-Membuka-Toko-Kelontong-Modal-Kecil-Banyak-Untung.png',
                ),
                callback: (item) {
                  _showDepoDetail(context, item);
                },
              ),
              DepoItem(
                depo: Depo(
                  depoName: 'Depo Pak Udin',
                  alamat: 'Jl. Kemanggisan Ilir No. 7 Palmerah - Jakarta Barat',
                  noTelephone: '021 8972-3095\n0812-9732-8712',
                  profilePict:
                      'https://blog.amartha.com/wp-content/uploads/2020/12/Ini-Tips-Sukses-Membuka-Toko-Kelontong-Modal-Kecil-Banyak-Untung.png',
                ),
                callback: (item) {
                  _showDepoDetail(context, item);
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showDepoDetail(context, Depo depo) {
    String pic = depo.profilePict ?? "";
    String name = depo.depoName ?? "";
    // double lat = depo.latitude?.toDouble() ?? 0.0;
    // double long = depo.longitude?.toDouble() ?? 0.0;

    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 57,
                height: 4,
                decoration: BoxDecoration(
                  color: Color(0xFFDEDEDE),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
              SizedBox(height: 32),
              CachedNetworkImage(
                width: 195,
                imageUrl: pic,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    ProgressLoadingWithValue(value: downloadProgress.progress),
              ),
              SizedBox(height: 16),
              Text(
                name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                    color: COLOR_PRIMARY_TEXT),
              ),
              SizedBox(height: 8),
              Text(
                '${depo.alamat}\n${depo.noTelephone}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(height: 16),
              TextButtonPrimary(
                isEnable: true,
                text: 'Submit',
                onPressed: () {
                  _launchDepo(-6.330380, 106.816666);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  void _launchDepo(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
