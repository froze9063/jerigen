import 'package:get/get.dart';

import '../controllers/search_depo_controller.dart';

class SearchDepoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchDepoController>(
      () => SearchDepoController(),
    );
  }
}
