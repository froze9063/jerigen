import 'package:get/get.dart';

class TodayPriceController extends GetxController {
  //TODO: Implement TodayPriceController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
