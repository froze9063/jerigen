import 'package:get/get.dart';

import '../controllers/today_price_controller.dart';

class TodayPriceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TodayPriceController>(
      () => TodayPriceController(),
    );
  }
}
