import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/widgets/app_bar.dart';

import '../../../../../../utils/constant.dart';
import '../controllers/today_price_controller.dart';

class TodayPriceView extends GetView<TodayPriceController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Harga per Kg', () => Get.back()),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    _buildContent(context),
                    SizedBox(height: 16),
                    _buildNote(context),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent(BuildContext context) {
    return Card(
      elevation: 3,
      child: Column(
        children: [
          _buildItem('Jakarta Greater Area', '3.000'),
          _buildItem('Bandung', '2.500'),
          _buildItem('Jogjakarta', '2.000'),
          _buildItem('Surabaya', '3.000'),
          _buildItem('Medan', '3.000'),
        ],
      ),
    );
  }

  Widget _buildItem(String location, String price) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(width: 8),
            Expanded(
              child: Text(
                location,
                style: TextStyle(
                  fontSize: 16,
                  color: COLOR_PRIMARY_TEXT,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(width: 8),
            Container(width: 1, color: COLOR_BORDER, height: 30),
            SizedBox(width: 8),
            Text(
              price,
              style: TextStyle(
                fontSize: 16,
                color: COLOR_PRIMARY_TEXT,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(width: 8),
          ],
        ),
        Container(height: 1, color: COLOR_BORDER),
      ],
    );
  }

  Widget _buildNote(BuildContext context) {
    return Text(
      '''
Catatan:

1. 1 poin = Rp 1,-
2. Minyak goreng bekas yang anda kumpulkan, kami beli sesuai harga poin per Kilogram. Harga minyak goreng bekas dapat berubah sewaktu-waktu, mengikuti harga dunia.
3. Minyak goreng bekas yang kami kumpulkan, akan diolah menjadi biodiesel yang dapat digunakan sebagai bahan bakar kendaraan maupun sumber energi pembangkit listrik.
4. User bisa datang langsung ke Depo Jerigen dan melakukan scan barcode depo, kemudian memasukkan jumlah Kilogram minyak goreng bekas.
5. User dapat melakukan request penjemputan melalui fitur Pick Up. Masukkan alamat, waktu penjemputan, dan jumlah liter (minimum penjemputan 1 jerigen 5 liter).
6. Poin yang didapat dapat diuangkan melalui fitur tarik tunai di menu poin.
7. Poin juga dapat didonasikan melalui fitur donasi. Poin yang didonasikan akan kami salurkan melalui badan amal dan zakat terkemuka di Indonesia.
''',
      style: TextStyle(
        fontSize: 14,
        fontStyle: FontStyle.italic,
        color: COLOR_PRIMARY_TEXT,
        fontWeight: FontWeight.w500,
      ),
    );
  }
}
