// Created by ferdyhaspin on 29/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/utils.dart';

import '../../../../../utils/singleton.dart';

class ProfileHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String pic = Singleton.loginResponse.user?.profilePictUrl ?? "";
    return Container(
      decoration: BoxDecoration(
        color: COLOR_PRIMARY.withOpacity(0.3),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                pic.isNotEmpty
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(10000),
                        child: CachedNetworkImage(
                          width: 56,
                          height: 56,
                          fit: BoxFit.fill,
                          imageUrl: pic,
                        ),
                      )
                    : Image.asset(
                        "assets/images/img_default.png",
                        height: 56,
                        color: Colors.white,
                      ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${Singleton.loginResponse.user!.fullName}",
                        style: TextStyle(
                            fontSize: 16,
                            color: Color.fromRGBO(44, 63, 88, 1.0),
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(height: 4),
                      Text(
                        "${Singleton.loginResponse.user!.idNumber}",
                        style: TextStyle(
                          fontSize: 10,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(100, 111, 126, 1.0),
                        ),
                      ),
                      SizedBox(height: 4),
                      Text(
                        "CLIMATE WARRIOR",
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(100, 111, 126, 1.0),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(width: 16),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset("assets/icons/ic_coin.png", height: 30),
                    SizedBox(height: 8),
                    Text(
                      Utils.toIDR(Singleton.loginResponse.user!.points),
                      style: TextStyle(
                        color: COLOR_PRIMARY_TEXT,
                        fontWeight: FontWeight.w600,
                        fontSize: 10,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: COLOR_PRIMARY,
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "CLIMATE WARRIOR",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                          color: COLOR_PRIMARY_DARK_TEXT,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "CLIMATE HERO",
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                          color: COLOR_PRIMARY_DARK_TEXT,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 8),
                Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 7,
                          child: Container(
                            height: 15,
                            color: COLOR_PRIMARY_EXTRA_DARK,
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            height: 15,
                            color: COLOR_PRIMARY_EXTRA_DARK.withOpacity(0.6),
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      bottom: 0,
                      left: 0,
                      child: Container(
                        // color: Colors.white,
                        child: Center(
                          child: Text(
                            '40Liter/60Liter',
                            style: TextStyle(
                              fontSize: 8,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
