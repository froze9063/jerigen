// Created by ferdyhaspin on 29/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class ProfileItem extends StatelessWidget {
  final String icon, title;
  final GestureTapCallback action;

  const ProfileItem({
    Key? key,
    required this.icon,
    required this.title,
    required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          InkWell(
            onTap: action,
            child: Padding(
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              child: Row(
              children: [
                Image.asset('assets/icons/$icon', width: 24, height: 24),
                SizedBox(width: 12),
                Expanded(
                  child: Text(
                    title,
                    style: TextStyle(
                      color: COLOR_PRIMARY_DARK_TEXT,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey,
            height: 1,
          ),
        ],
    );
  }
}
