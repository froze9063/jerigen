import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:jerrycan_master/app/widgets/general.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../data/model/master/address.dart';
import '../../../../routes/app_pages.dart';
import '../../../../utils/constant.dart';
import '../../../../widgets/account_bank_item.dart';
import '../../../../widgets/app_bar.dart';
import '../../../../widgets/button.dart';
import '../controllers/profile_controller.dart';
import 'widget/profile_header.dart';
import 'widget/profile_item.dart';

class ProfileView extends GetView<ProfileController> {
  // final _controller = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Profile', null, elevation: 1),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ProfileHeader(),
              SizedBox(height: 24),
              ProfileItem(
                icon: 'ic_profile_information.png',
                title: 'Informasi Personal',
                action: () => Get.toNamed(Routes.SETTING_PROFILE),
              ),
              ProfileItem(
                icon: 'ic_location.png',
                title: 'Alamat',
                action: () => _showDialogAddress(context),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16),
                child: Text(
                  'General Settings',
                  style: TextStyle(
                    color: COLOR_PRIMARY_DARK_TEXT,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              ProfileItem(
                icon: 'ic_profile_referral.png',
                title: 'Bagikan Referral Code',
                action: () => _showShareReferral(context),
              ),
              // ProfileItem(
              //   icon: 'ic_location.png',
              //   title: 'Lokasi',
              //   action: () {},
              // ),
              ProfileItem(
                icon: 'ic_notification.png',
                title: 'Notifikasi',
                action: () => Get.toNamed(Routes.SETTING_NOTIFICATION),
              ),
              // ProfileItem(
              //   icon: 'ic_profile_help.png',
              //   title: 'Bantuan',
              //   action: () {},
              // ),
              ProfileItem(
                icon: 'ic_profile_tnc.png',
                title: 'Syarat dan Ketentuan',
                action: () => Get.toNamed(Routes.SETTING_TNC),
              ),
              ProfileItem(
                icon: 'ic_profile_sign_out.png',
                title: 'Keluar',
                action: _doLogout,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _doLogout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(PREFS_LOGIN_DATA, "");

    Get.offAllNamed(Routes.LOGIN);
  }

  void _showDialogAddress(BuildContext context) {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 57,
                height: 4,
                decoration: BoxDecoration(
                  color: Color(0xFFDEDEDE),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
              SizedBox(height: 32),
              Singleton.addresses.length == 0
                  ? Center(
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: defaultText("Tidak ada data Alamat"),
                      ),
                    )
                  : Column(
                      children: [
                        ...List.generate(Singleton.addresses.length, (index) {
                          Address data = Singleton.addresses[index];
                          List<String> addressDesc =
                              (data.addressDesc ?? " - ").split(" - ");
                          return AccountBankItem(
                            title: 'Alamat Tersimpan ${index + 1}',
                            label:
                                '${addressDesc[0]}\nDetail : ${addressDesc[1]}'
                                    .trim(),
                            type: ACCOUNT_BANK_CHOOSE,
                            callback: (type) {
                              switch (type) {
                                case ACCOUNT_BANK_EDIT:
                                  Get.toNamed(Routes.ADD_ADDRESS,
                                      arguments: data);
                                  break;
                              }
                            },
                          );
                        })
                      ],
                    ),
              SizedBox(height: 20),
              TextButtonPrimary(
                isEnable: true,
                text: "Tambah Alamat",
                onPressed: () {
                  Get.back();
                  Get.toNamed(Routes.ADD_ADDRESS, arguments: true);
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showShareReferral(BuildContext context) async {
    showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 57,
                height: 4,
                decoration: BoxDecoration(
                  color: Color(0xFFDEDEDE),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
              ),
              SizedBox(height: 32),
              Text(
                'Bagikan kode referral di bawah ini kepada teman anda. Gunakan kode tersebut di halaman daftar. Dapatkan 10.000 poin bagi setiap kode referral anda yang teman anda gunakan.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  color: COLOR_PRIMARY_TEXT,
                ),
              ),
              SizedBox(height: 16),
              Container(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'G67X1',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                        color: COLOR_PRIMARY_TEXT,
                      ),
                    ),
                    SizedBox(width: 8),
                    IconButton(
                      icon: Icon(Icons.share),
                      color: COLOR_PRIMARY,
                      onPressed: () {
                        Share.share(
                            'Ayo bergabung bersama saya menjadi Agent of Change Jerigen, Agen Perubahan yang membantu mengurangi sampah minyak goreng bekas. Demi bumi yang lebih baik!'
                            '\n\nKlik link di bawah ini untuk install aplikasi Jerigen: https://bit.ly/jerigen-app'
                            '\n\nMasukkan kode referral ketika mendaftar: 82jGc5!');
                      },
                    )
                  ],
                ),
              ),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }
}
