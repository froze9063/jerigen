import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../widgets/app_bar.dart';
import '../controllers/setting_notification_controller.dart';

class SettingNotificationView extends GetView<SettingNotificationController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(
        'Notifikasi',
        () => Get.back(),
        elevation: 1,
      ),
      body: Center(
        child: Text(
          'SettingNotificationView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
