import 'package:get/get.dart';

import '../controllers/setting_tnc_controller.dart';

class SettingTncBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SettingTncController>(
      () => SettingTncController(),
    );
  }
}
