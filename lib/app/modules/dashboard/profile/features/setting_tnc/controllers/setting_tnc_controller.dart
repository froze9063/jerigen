import 'package:get/get.dart';

class SettingTncController extends GetxController {
  //TODO: Implement SettingTncController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
