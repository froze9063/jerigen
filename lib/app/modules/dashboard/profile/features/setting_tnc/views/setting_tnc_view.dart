import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../utils/constant.dart';
import '../../../../../../widgets/app_bar.dart';
import '../controllers/setting_tnc_controller.dart';

class SettingTncView extends GetView<SettingTncController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(
        'Syarat dan Ketentuan',
        () => Get.back(),
        elevation: 1,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Text(
            '''
Jerigen

Terms of Service.

Indonesia

Select Country


Ketentuan Penggunaan Aplikasi Jerigen


KETENTUAN PENGGUNAAN APLIKASI JERIGEN


Selamat datang di Aplikasi Jerigen!


Ketentuan Umum

BACALAH SELURUH KETENTUAN PENGGUNAAN INI SEBELUM MENGAKSES ATAU MENGGUNAKAN APLIKASI Jerigen.


Ketentuan Penggunaan ini adalah perjanjian antara pengguna (“Anda”) dan PT Integrasi Ekonomi Sirkular (“Kami”), sebuah perseroan terbatas yang didirikan dan beroperasi secara sah berdasarkan hukum negara Republik Indonesia dan berdomisili di DKI Jakarta, Indonesia. Ketentuan Penggunaan ini mengatur akses dan penggunaan Anda atas aplikasi, situs web (www.jerigen.id dan situs web lain yang Kami kelola), konten dan produk yang disediakan oleh Kami (selanjutnya, secara bersama-sama disebut sebagai “Aplikasi”), serta pemesanan, pembayaran atau penggunaan layanan yang tersedia pada Aplikasi (“Layanan”).


Dengan menyetujui Ketentuan Penggunaan ini, Anda juga menyetujui Ketentuan Penggunaan tambahan, termasuk Ketentuan Penggunaan pada setiap Layanan, dan perubahannya yang merupakan bagian yang tidak terpisahkan dari Ketentuan Penggunaan ini (selanjutnya, Ketentuan Penggunaan, Ketentuan Penggunaan tambahan, dan perubahannya secara bersama-sama disebut sebagai “Ketentuan Penggunaan”). Meskipun merupakan satu kesatuan, Ketentuan Penggunaan tambahan akan berlaku dalam hal terdapat perbedaan dengan Ketentuan Penggunaan.


Penggunaan Aplikasi dan Layanan

Akses dan penggunaan Aplikasi tunduk pada Ketentuan Penggunaan ini.


Anda mempunyai kebebasan penuh untuk memilih menggunakan Aplikasi atau aplikasi lain, menggunakan Layanan yang tersedia pada Aplikasi atau tidak, atau berhenti menggunakan Aplikasi.


Kami hanya memfasilitasi Anda untuk menemukan berbagai Layanan yang Anda perlukan dengan menyediakan Aplikasi. Semua Layanan disediakan secara langsung oleh pihak ketiga independen yang setuju menjadi penyedia layanan Kami dengan skema kemitraan atau skema lainnya (“Penyedia Layanan”).


Ketika memesan Layanan, Aplikasi akan menghubungkan Anda dengan Penyedia Layanan yang tersedia di sekitar lokasi Anda. Dengan demikian, beberapa Layanan tidak dapat digunakan bila Anda tidak mengaktifkan fitur penentuan lokasi.


Selanjutnya, jika Penyedia Layanan menerima pesanan Anda, Kami akan menginformasikan status pesanan Anda melalui Aplikasi.

Pembukaan dan Pengaksesan Akun Jerigen

Sebelum menggunakan Aplikasi, Anda harus menyetujui Ketentuan Penggunaan ini dan Kebijakan Privasi, dan mendaftarkan diri Anda dengan memberikan informasi yang dibutuhkan oleh Kami. Saat melakukan pendaftaran, Kami akan meminta Anda untuk memberikan nama lengkap, alamat email, dan nomor telepon seluler pribadi Anda yang sah. Anda dapat mengubah informasi data diri Anda pada fitur pengaturan dalam Aplikasi.


Setelah melakukan pendaftaran, sistem Kami akan menghasilkan kode verifikasi secara otomatis dan mengirim kode verifikasi tersebut melalui pesan singkat ke nomor telepon genggam yang Anda berikan. Anda perlu melakukan verifikasi dengan memasukan kode verifikasi tersebut pada halaman pendaftaran di Aplikasi.


Setelah melakukan verifikasi, sistem Kami akan membuatkan akun Jerigen pribadi (“Akun”) untuk Anda yang dapat digunakan untuk menggunakan Aplikasi dan memesan Layanan melalui Aplikasi. Nomor telepon genggam Anda melekat pada Akun Anda sehingga Anda tidak bisa membuat Akun baru dengan nomor telepon genggam yang sudah didaftarkan. Hal yang sama juga berlaku apabila di kemudian hari Anda mengubah nomor telepon genggam Anda pada menu pengaturan di Aplikasi.


Dalam hal Anda telah keluar dari Akun Anda, maka Anda perlu memasukan alamat surat elektronik atau nomor telepon genggam yang ada berikan pada saat mendaftarkan diri Anda dan memasukan kode verifikasi, yang kemudian dikirim secara otomatis oleh sistem Kami ke nomor telepon genggam terdaftar Anda, pada halaman pengaksesan.


KODE VERIFIKASI (ONE TIME PASSWORD/OTP) DIHASILKAN SECARA OTOMATIS OLEH SISTEM KAMI. KAMI TIDAK MENGETAHUI DAN TIDAK PERNAH MEMINTA KODE VERIFIKASI ANDA. JANGAN PERNAH MEMBERITAHUKAN KODE VERIFIKASI ANDA KEPADA SIAPAPUN BAHKAN KEPADA KAMI ATAU PIHAK LAIN YANG MENGAKU SEBAGAI PERWAKILAN KAMI.


Fitur Otentikasi Perangkat

Jika Anda menggunakan perangkat yang mendukung fitur sidik jari (fingerprint) dan / atau pengenalan wajah (facial recognition), Anda dapat mengaktifkan fitur otentikasi perangkat tersebut untuk masuk ke Aplikasi kami.


Ketika Anda mengaktifkan fitur sidik jari dan / atau pengenalan wajah di perangkat seluler Anda untuk keperluan otentikasi di Aplikasi, harap dicatat bahwa Kami tidak menyimpan data biometrik tersebut. Kecuali jika diberitahukan sebaliknya kepada Anda, data tersebut disimpan di perangkat seluler Anda dan juga dapat disimpan oleh pihak ketiga, seperti pabrik pembuat perangkat Anda. Anda setuju dan mengakui bahwa kami tidak bertanggung jawab atas akses atau kehilangan yang tidak sah terhadap data biometrik yang disimpan di perangkat Anda.


Jika Anda mengganti perangkat seluler Anda, OTP masih akan diperlukan bagi Anda untuk masuk ke Aplikasi. Jika Anda ingin mengaktifkan fitur otentikasi perangkat di perangkat baru Anda, Anda dapat melakukannya di pengaturan perangkat seluler Anda.


Akun Anda

Akun Anda hanya dapat digunakan oleh Anda dan tidak bisa dialihkan kepada orang lain dengan alasan apapun. Kami berhak menolak untuk memfasilitasi pesanan jika Kami mengetahui atau mempunyai alasan yang cukup untuk menduga bahwa Anda telah mengalihkan atau membiarkan Akun Anda digunakan oleh orang lain.


Keamanan dan kerahasiaan Akun Anda, termasuk nama terdaftar, alamat surat elektronik terdaftar, nomor telepon genggam terdaftar, rincian pembayaran dan Metode Pembayaran yang Anda pilih, serta kode verifikasi yang dihasilkan dan dikirim oleh sistem Kami atau Penyedia Metode Pembayaran sepenuhnya merupakan tanggung jawab pribadi Anda. Semua kerugian dan risiko yang ada akibat kelalaian Anda menjaga keamanan dan kerahasiaan sebagaimana disebutkan ditanggung oleh Anda sendiri. Dalam hal demikian, Kami akan menganggap setiap penggunaan atau pesanan yang dilakukan melalui Akun Anda sebagai permintaan yang sah dari Anda.


Segera beritahukan Kami jika Anda mengetahui atau menduga bahwa Akun Anda telah digunakan tanpa sepengetahuan dan persetujuan Anda. Kami akan melakukan tindakan yang Kami anggap perlu dan dapat Kami lakukan terhadap penggunaan tanpa persetujuan tersebut.


Pentautan Akun (Account Linking)

Anda dapat menautkan akun Jerigen anda dengan platform pihak ketiga untuk mengakses fitur-fitur tertentu, termasuk pentautan akun, loyalty program, dan mengakses layanan atau produk yang tersedia di platform Kami atau platform pihak ketiga 


Deaktivasi Akun dan Unlinking Akun


Jika Anda ingin melakukan deaktivasi akun Jerigen anda atau melakukan unlink terhadap akun Jerigen anda dan platform pihak ketiga, anda dapat mengakses opsi tersebut dalam Aplikasi Jerigen atau mengirimkan permintaan anda secara langsung ke customerservice@gojek.com.


Informasi Pribadi

Pengumpulan, penyimpanan, pengolahan, penggunaan dan pembagian informasi pribadi Anda, seperti nama, alamat surat elektronik, dan nomor telepon genggam Anda yang Anda berikan ketika Anda membuka Akun tunduk pada Kebijakan Privasi, yang merupakan bagian yang tidak terpisahkan dari Ketentuan Penggunaan ini.


Konten, Informasi dan Promosi

Kami atau pihak lain yang bekerja sama dengan Kami dapat menyediakan Konten Pihak Ketiga yang dapat Anda temukan pada Aplikasi. Dalam hal Konten Pihak Ketiga disediakan oleh pihak lain yang bekerja sama dengan Kami (“Penyedia Konten Pihak Ketiga”), Kami tidak bertanggung jawab atas bagian apapun dari isi Konten Pihak Ketiga. Akses atau penggunaan Anda terhadap Konten Pihak Ketiga tersebut merupakan bentuk persetujuan Anda untuk tunduk pada syarat dan ketentuan yang ditetapkan oleh Kami atau Penyedia Konten Pihak Ketiga, termasuk terhadap Kebijakan Privasi Kami atau Penyedia Konten Pihak Ketiga.


Konten Pihak Ketiga adalah setiap dan/atau seluruh informasi dan penawaran barang dan/atau jasa, yang dibuat dan/atau disusun dan/atau dikembangkan dan/atau dikelola oleh Penyedia Konten Pihak Ketiga termasuk namun tidak terbatas pada teks atau tulisan, gambar, quotes atau kutipan, foto, ilustrasi, animasi, video, rekaman suara atau musik, judul, deskripsi dan/atau setiap data dalam bentuk apapun yang disediakan oleh Penyedia Konten Pihak Ketiga untuk ditampilkan pada Aplikasi Jerigen, termasuk setiap tautan yang menghubungkan kepadanya. Untuk menghindari keraguan, Konten Pihak Ketiga mencakup pula setiap dan/atau seluruh penawaran barang dan/atau jasa, informasi, data, berita aktual, tulisan, gambar, kutipan, foto, ilustrasi, animasi, video, rekaman suara, yang diperoleh Penyedia Konten Pihak Ketiga dari pihak ketiga, dimana Penyedia Konten Pihak Ketiga telah memiliki kewenangan untuk menggunakan dan mendistribusikan konten tersebut.


Kami atau pihak lain yang bekerja sama dengan Kami dapat memberikan penawaran atau promosi (“Penawaran”) yang dapat ditukar dengan barang, Layanan atau manfaat lain terkait dengan penggunaan Aplikasi. Dalam hal Penawaran disediakan oleh pihak lain yang bekerja sama dengan Kami (“Penyedia Penawaran”), Kami tidak bertanggung jawab atas bagian apapun dari isi Penawaran tersebut. Akses atau penggunaan Anda terhadap Penawaran merupakan bentuk persetujuan Anda untuk tunduk pada syarat dan ketentuan yang ditetapkan oleh Kami atau Penyedia Penawaran, termasuk terhadap Kebijakan Privasi Kami atau Penyedia Penawaran.


Semua informasi, Penawaran yang terdapat pada Aplikasi hanya dimaksudkan untuk memberikan Anda pengalaman terbaik ketika menggunakan Aplikasi atau Layanan. Anda tidak boleh menyalahgunakan Penawaran yang Anda terima selama penggunaan Aplikasi atau Layanan.


Anda setuju untuk menggunakan Penawaran tersebut sesuai dengan syarat, ketentuan, dan maksud dari pemberian Penawaran dan tidak akan menyalahgunakan, menggandakan, menguangkan, mengalihkan, menggunakan untuk kepentingan komersial atau mengambil keuntungan dengan tidak wajar dari promosi tersebut dengan bentuk atau cara apapun.


Anda memahami bahwa Penawaran tidak dapat ditukar dengan uang tunai, memiliki masa keberlakuan yang terbatas dan tunduk pada ketentuan yang berlaku untuk setiap Penawaran tersebut.


Biaya dan Pembayaran

Pengunduhan perangkat lunak dan penggunaan Aplikasi adalah bebas biaya. Namun, Kami dapat mengenakan biaya untuk penggunaan fitur tertentu pada Aplikasi di kemudian hari.


Layanan yang tersedia pada Aplikasi dikenakan biaya yang dapat Anda temukan pada Aplikasi sebelum Anda memesan Layanan tersebut. Kami dapat mengubah atau memperbaharui biaya dari waktu ke waktu berdasarkan faktor tertentu, antara lain lokasi, waktu, jenis Layanan dan peraturan perundang-undangan yang berlaku. Kami juga dapat membebankan harga dan/atau biaya dengan besaran tertentu sebagai pembayaran untuk penggunaan Aplikasi yang dapat dipungut oleh Kami, afiliasi Kami, atau Penyedia Layanan.


Anda dapat melakukan pembayaran terhadap Layanan, Konten Pihak Ketiga atau Penawaran yang Anda akses atau gunakan dengan uang tunai atau melalui metode pembayaran elektronik, yang disediakan oleh pihak ketiga independen (“Penyedia Metode Pembayaran”), pada Aplikasi, termasuk uang elektronik, fasilitas pinjaman, tagihan perusahaan (dengan menggunakan Akun pengguna yang sah yang terhubung dengan akun pemberi pekerjaan Anda) kartu debit atau kredit, rekening bank, atau metode pembayaran lain (“Metode Pembayaran”) yang dapat berubah sewaktu-waktu berdasarkan kebijakan kami sepenuhnya.


Untuk dapat melakukan pembayaran melalui Metode Pembayaran, Anda harus terdaftar secara resmi pada Penyedia Metode Pembayaran yang Anda pilih dan menggunakan kredensial pembayaran Anda sendiri. Dalam hal Anda menggunakan kredensial terdaftar pihak lain, Anda bertanggung jawab secara penuh atas semua perizinan yang diperlukan dan semua kerugian atau sengketa yang timbul antara Anda dan pihak lain tersebut, baik karena kelalaian atau kesalahan Anda, Kami, Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran.


Kami berhak menolak atau menunda untuk meneruskan permintaan pembayaran Anda melalui Metode Pembayaran karena alasan tertentu, termasuk namun tidak terbatas pada adanya indikasi atau Kami mempunyai alasan yang cukup untuk menduga adanya kecurangan, penipuan, pelanggaran Ketentuan Penggunaan, pelanggaran atas peraturan perundang-undangan yang berlaku termasuk yang terkait dengan alat pembayaran menggunakan kartu, uang elektronik, pemrosesan transaksi pembayaran, anti pencucian uang, korupsi dan pencegahan pendanaan terorisme, atau tindakan lain yang tidak wajar atau mencurigakan, termasuk belum dipenuhinya kewajiban Anda kepada Kami.


Ketentuan lebih lanjut tentang biaya yang berlaku terhadap Layanan tertentu, Konten Pihak Ketiga, Penawaran dan/atau Metode Pembayaran dapat ditemukan dalam Ketentuan Penggunaan tambahan dari Layanan yang Anda pesan dan syarat dan ketentuan dari Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran dan/atau Penyedia Metode Pembayaran.


Perangkat Lunak Aplikasi

Kami hanya menyediakan perangkat lunak Aplikasi yang resmi pada pasar digital resmi, seperti Google Play Store atau Apple App Store, dan untuk digunakan hanya pada perangkat telepon genggam atau tablet. Mengunduh Aplikasi dari tempat lain selain pasar digital resmi dan/atau ke dalam perangkat lain selain telepon genggam atau tablet merupakan pelanggaran terhadap Ketentuan Penggunaan ini dan terhadap hak kekayaan intelektual Kami.


Kekayaan Intelektual

Aplikasi dan Layanan, termasuk namun tidak terbatas pada, nama, logo, kode program, desain, merek dagang, teknologi, basis data, proses dan model bisnis, dilindungi oleh hak cipta, merek, paten dan hak kekayaan intelektual lainnya yang tersedia berdasarkan hukum Republik Indonesia yang terdaftar baik atas nama Kami ataupun afiliasi Kami. Kami (dan pemberi lisensi Kami) memiliki seluruh hak dan kepentingan atas Aplikasi dan Layanan, termasuk seluruh hak kekayaan intelektual terkait dengan seluruh fitur yang terdapat didalamnya dan hak kekayaan intelektual terkait.


Tunduk pada Ketentuan Penggunaan ini, Kami memberikan Anda lisensi terbatas yang tidak eksklusif, dapat ditarik kembali, tidak dapat dialihkan (tanpa hak sublisensi) untuk (i) mengunduh, mengakses, dan menggunakan Aplikasi, sebagaimana adanya, hanya pada perangkat berupa telepon genggam dan/atau tablet pribadi Anda dan hanya untuk keperluan terkait penggunaan Layanan, dan (ii) mengakses atau menggunakan konten, informasi dan materi terkait yang tersedia pada Aplikasi hanya untuk kepentingan pribadi dan bukan tujuan komersial. Hak dan hak istimewa lainnya yang tidak secara tegas diberikan dalam Ketentuan Penggunaan ini, adalah hak Kami atau pemberi lisensi Kami.


Setiap penggandaan, distribusi, pembuatan karya turunan, penjualan atau penawaran untuk menjual, penampilan baik sebagian atau seluruhnya, serta penggunaan Aplikasi dan/atau Layanan, baik secara digital atau lainnya atau pada perangkat selain telepon genggam atau tablet, termasuk namun tidak terbatas pada penggunaan aplikasi tambahan seperti aplikasi modifikasi, emulator, dan lain-lain, yang menyimpang dari Ketentuan Penggunaan ini, maupun tujuan penggunaan yang ditentukan oleh Kami, merupakan pelanggaran terhadap hak kekayaan intelektual Kami.


Anda tidak boleh:

menghapus setiap pemberitahuan hak cipta, merek dagang atau pemberitahuan hak milik lainnya yang terkandung dalam Aplikasi;
menyalin, memodifikasi, mengadaptasi, menerjemahkan, membuat karya turunan dari, mendistribusikan, memberikan lisensi, menjual, mengalihkan, menampilkan di muka umum baik sebagian maupun seluruhnya, merekayasa balik (reverse engineer), mentransmisikan, memindahkan, menyiarkan, menguraikan, atau membongkar bagian manapun dari atau dengan cara lain mengeksploitasi Aplikasi (termasuk perangkat lunak, fitur dan Layanan di dalamnya);
memberikan lisensi, mensublisensikan, menjual, menjual kembali, memindahkan, mengalihkan, mendistribusikan atau mengeksploitasi secara komersial atau membuat tersedia kepada pihak lain Aplikasi dan/atau perangkat lunak dengan cara menciptakan tautan (link) internet ke Aplikasi atau "frame" atau "mirror" setiap perangkat lunak pada server lain atau perangkat nirkabel atau yang berbasis internet;
meluncurkan program otomatis atau script, termasuk, namun tidak terbatas pada, web spiders, web crawlers, web robots, web ants, web indexers, bots, virus atau worm, atau program apapun yang mungkin membuat beberapa permintaan server per detik, menciptakan beban berat atau menghambat operasi dan/atau kinerja Aplikasi;
menggunakan aplikasi pencarian atau pengambilan kembali situs, perangkat manual atau otomatis lainnya untuk mengambil (scraping), indeks (indexing), survei (surveying), tambang data (data mining), atau dengan cara apapun memperbanyak atau menghindari struktur navigasi atau presentasi dari Aplikasi atau isinya;
menerbitkan, mendistribusikan atau memperbanyak dengan cara apapun materi yang dilindungi hak cipta, merek dagang, atau informasi lain yang Kami miliki tanpa persetujuan tertulis terlebih dahulu dari Kami atau pemilik hak yang melisensikan hak-nya kepada Kami, dan
menggunakan dan/atau mengakses secara tidak resmi Aplikasi untuk (a) merusak, melemahkan atau membahayakan setiap aspek dari Aplikasi, Layanan atau sistem dan jaringan terkait, dan/atau (b) membuat produk atau layanan tandingan serupa menggunakan ide, fitur, fungsi atau grafik menyerupai Aplikasi.
Tanggung Jawab Anda


Anda bertanggung jawab penuh atas keputusan yang Anda buat untuk menggunakan atau mengakses Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran. Anda harus memperlakukan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran dan Penyedia Metode Pembayaran dengan hormat dan tidak boleh terlibat dalam perilaku atau tindakan yang tidak sah, mengancam atau melecehkan ketika menggunakan Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran.


Anda bertanggung jawab secara penuh atas setiap kerugian dan/atau klaim yang timbul dari penggunaan Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran melalui Akun Anda, baik oleh Anda atau pihak lain yang menggunakan Akun Anda, dengan cara yang bertentangan dengan Ketentuan Penggunaan ini, Kebijakan Privasi, termasuk syarat dan ketentuan dan kebijakan privasi yang ditentukan oleh Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran dan Penyedia Metode Pembayaran, atau peraturan perundang-undangan yang berlaku, termasuk namun tidak terbatas untuk tujuan anti pencucian uang, anti pendanaan terorisme, aktivitas kriminal, penipuan dalam bentuk apapun (termasuk namun tidak terbatas pada kegiatan phishing dan/atau social engineering), pelanggaran hak kekayaan intelektual, dan/atau aktivitas lain yang merugikan publik dan/atau pihak lain manapun atau yang dapat atau dianggap dapat merusak reputasi Kami.


Batasan Tanggung Jawab Kami

Kami menyediakan Aplikasi sebagaimana adanya dan Kami tidak menyatakan atau menjamin bahwa keandalan, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi, kelengkapan atau keamanan dari Aplikasi dapat memenuhi kebutuhan dan akan sesuai dengan harapan Anda, termasuk namun tidak terbatas pada Layanan, Konten Pihak Ketiga, Penawaran dan Metode Pembayaran yang sepenuhnya menjadi tanggung jawab Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran dan Penyedia Metode Pembayaran. Kami tidak bertanggung jawab atas setiap kerugian atau kerusakan yang disebabkan oleh setiap kegagalan atau kesalahan yang dilakukan oleh Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran ataupun kegagalan atau kesalahan Anda dalam mematuhi Ketentuan Penggunaan Kami, Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran.


Aplikasi dapat mengalami keterbatasan, penundaan, dan masalah-masalah lain yang terdapat dalam penggunaan internet dan komunikasi elektronik, termasuk perangkat Anda, Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran rusak, tidak terhubung dengan internet, berada di luar jangkauan, dimatikan atau tidak berfungsi. Kami tidak bertanggung jawab atas keterlambatan, kegagalan pengiriman, kerusakan atau kerugian yang diakibatkan oleh masalah-masalah tersebut.


Kami tidak berkewajiban untuk mengawasi akses atau penggunaan Anda atas Aplikasi. Namun, Kami tetap melakukan pengawasan untuk tujuan memastikan kelancaran penggunaan Aplikasi dan untuk memastikan kepatuhan terhadap Ketentuan Penggunaan ini, peraturan perundang-undangan yang berlaku, putusan pengadilan, dan/atau ketentuan lembaga administratif atau badan pemerintahan lainnya.


Kami tidak mempunyai kewajiban apapun, termasuk untuk mengambil tindakan lebih jauh atau tindakan hukum yang dianggap perlu oleh Anda, Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran, terhadap setiap permasalahan atau perselisihan yang timbul antara Anda dan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran. Tetapi, Kami akan memfasilitasi setiap permasalahan atau perselisihan yang timbul antara Anda dan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran dengan upaya wajar yang diperlukan. Ketika Kami memfasilitasi penyelesaian permasalahan atau perselisihan antara Anda dan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran, Kami tidak bertindak sebagai mediator dan hal tersebut tidak menimbulkan kewajiban lebih jauh apapun terhadap Kami.


Penyelesaian Masalah

Apabila Anda mengalami gangguan sistem, mengetahui atau menduga bahwa Akun Anda diretas, digunakan atau disalahgunakan oleh pihak lain, atau apabila perangkat telepon genggam atau tablet pribadi Anda hilang, dicuri, diretas atau terkena virus, segera laporkan kepada Kami sehingga Kami dapat segera mengambil tindakan yang diperlukan untuk menghindari penggunaan, penyalahgunaan, atau kerugian yang timbul atau mungkin timbul lebih lanjut.


Apabila Anda mengalami kendala atau masalah terkait Layanan, Konten Pihak Ketiga, Penawaran atau pembayaran melalui Metode Pembayaran, atau perlakuan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran Anda dapat menyampaikan keluhan Anda melalui fitur yang Kami sediakan, termasuk pemberian peringkat dan komentar, atau dengan menghubungi Kami.


Untuk menyampaikan keluhan, pertanyaan, sanggahan, dan lain-lain (“Laporan”), Anda perlu memberikan informasi yang cukup, termasuk namun tidak terbatas pada, ringkasan fakta yang terjadi, bukti-bukti yang Anda miliki, nomor pesanan, dan informasi pribadi, seperti alamat surat elektronik dan nomor telepon genggam terdaftar.


Untuk menanggapi setiap Laporan yang Anda sampaikan, Kami akan melakukan verifikasi terlebih dahulu dengan mencocokan informasi yang Anda berikan dan informasi pribadi Anda yang terdapat dalam sistem Kami. Jika diperlukan, Kami dapat secara langsung meminta Anda memberikan informasi yang diperlukan untuk tujuan verifikasi.


Kami dapat menolak untuk menindaklanjuti Laporan Anda jika informasi yang Anda berikan tidak cocok dengan informasi pribadi yang terdapat dalam sistem Kami atau apabila Laporan disampaikan terkait, terhadap, atas nama atau oleh pihak lain yang berbeda dengan pemilik Akun yang bersangkutan yang terdaftar secara resmi pada sistem Kami. Kami dapat memberhentikan tindak lanjut terhadap Laporan Anda jika Kami, dengan kebijakan Kami sepenuhnya, menganggap bahwa Laporan Anda tidak didukung oleh fakta-fakta yang cukup dan jelas, atau telah selesai. Kami juga dapat meneruskan Laporan Anda kepada Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran untuk diselesaikan secara langsung oleh Anda dan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran.


Terhadap Laporan tertentu terkait Layanan, Kami dapat, dengan kebijakan Kami sepenuhnya, memfasilitasi pertemuan antara Anda dan Penyedia Layanan untuk tujuan penyelesaian masalah secara damai dan kekeluargaan. Dalam hal demikian, jika Anda merasa tetap perlu mengambil tindakan lain, termasuk tindakan hukum apapun, Anda dapat melakukannya atas tanggung jawab pribadi Anda sepenuhnya.


Terhadap Laporan tertentu terkait Metode Pembayaran, Kami dapat mengembalikan dana Anda melalui cara yang ditentukan oleh Kami atau bersama-sama dengan Penyedia Metode Pembayaran, berdasarkan kebijakan Kami sepenuhnya. Kami tidak akan mengembalikan dana kepada pihak lain yang berbeda dengan kredensial terdaftar pada Akun Jerigen atau Penyedia Metode Pembayaran, termasuk apabila Anda menggunakan kredensial pihak lain untuk melakukan pembayaran melalui Metode Pembayaran.


Pembekuan Sementara dan Pembekuan Permanen Akun Jerigen

Anda dapat menghapus Aplikasi dari telepon genggam dan/atau tablet Anda setiap saat. Kami tidak memiliki kewajiban apapun kepada Anda terhadap hal-hal yang timbul sejak penghapusan Aplikasi, pembekuan sementara atau pembekuan permanen Akun Anda. Akan tetapi, Anda tetap bertanggung jawab untuk menyelesaikan kewajiban-kewajiban Anda yang telah timbul, termasuk namun tidak terbatas pada, setiap kewajiban yang mungkin timbul akibat adanya sengketa, tuntutan, maupun tindakan hukum lainnya yang telah ada, sebelum tanggal penghapusan Aplikasi, pembekuan sementara atau pembekuan permanen Akun Anda.


Akun Anda dapat dibekukan untuk sementara waktu atau dapat dibekukan secara permanen karena hal-hal, termasuk namun tidak terbatas pada, sebagai berikut:


Laporan Anda bahwa Akun Anda digunakan atau diduga digunakan atau disalahgunakan oleh orang lain;

Laporan Anda bahwa telepon genggam atau tablet pribadi Anda hilang, dicuri atau diretas;

Kami mengetahui atau mempunyai alasan yang cukup untuk menduga bahwa Akun Anda telah dialihkan atau digunakan oleh orang lain;

Kami mengetahui atau dengan alasan yang cukup menduga bahwa telah terjadi hal-hal yang menurut pandangan Kami telah atau dapat merugikan Kami, Anda, Penyedia Layanan atau pihak lainnya;

Kami mengetahui atau dengan alasan yang cukup menduga bahwa anda telah mendaftar atau masuk dalam banyak Akun dalam satu perangkat untuk tujuan melanggaran Ketentuan Penggunaan, Kebijakan Privasi atau peraturan dan hukum yang berlaku.

Sistem Kami mendeteksi adanya tindakan yang tidak wajar dari penggunaan Akun Anda atau adanya kewajiban berdasarkan Ketentuan Penggunaan, dan/atau Kebijakan Privasi yang tidak dipenuhi oleh Anda;

Anda telah meninggal dunia, ditempatkan di bawah perwalian atau pengampuan atau mengalami ketidakmampuan lainnya yang menjadikan Anda tidak cakap hukum berdasarkan peraturan perundang-undangan yang berlaku;

Penggunaan Aplikasi atau Layanan oleh Anda atau pihak lain (yang menggunakan Akun Anda) dengan cara yang bertentangan dengan Ketentuan Penggunaan ini, Kebijakan Privasi atau peraturan perundang-undangan yang berlaku; dan/atau

Perintah untuk pembekuan akun, baik sementara atau permanen, yang diterbitkan oleh institusi pemerintah atau moneter terkait atau berdasarkan perintah pengadilan yang diterbitkan sesuai dengan peraturan perundang-undangan yang berlaku.

Jika Akun Anda dibekukan dan Anda memiliki bukti yang jelas bahwa Akun Anda seharusnya tidak dibekukan, Anda dapat membuat Laporan kepada Kami untuk menyampaikan bukti-bukti tersebut. Setelah melakukan pemeriksaan lebih lanjut terhadap Laporan Anda, Kami akan, atas kebijakan Kami sepenuhnya, menentukan untuk mengakhiri atau melanjutkan pembekuan terhadap Akun Anda. Pembekuan tidak akan diteruskan secara tidak wajar apabila Kami memutuskan bahwa hal-hal yang mengakibatkan terjadinya pembekuan telah diselesaikan.



Anda tidak akan menggunakan Aplikasi pada perangkat atau sisterm operasi yang telah dimodifikasi diluar perangkat atau konfigurasi sistem operasi dan konfigurasi Jerigen. Hal ini mencakup perangkat yang telah melalui proses “rooted” atau “jail-broken”. Perangkat rooted atau jail-broken adalah perangkat yang telah dibebaskan dari pembatasan yang dikenakan oleh penyedia layanan perangkat dan yang dimanufaktur tanpa persetujuan penyedia layanan perangkat. Penggunaan Aplikasi pada perangkat rooted atau jail-broken dapat mengkompromisasi keamanan dan berujung pada transaksi penipuan.


Jerigen tidak bertanggung jawab atas pengunduhan dan penggunaan Aplikasi pada perangkat rooted atau jail-broken dan resiko penggunaan anda terhadap perangkat rooted atau jail-broken sepenuhnya adalah resiko anda. Anda mengerti dan setuju bahwa Jerigen tidak bertanggung jawab atas segala kehilangan atau setiap konsekuensi lain yang diderita atau disebabkan oleh anda sebagai akibat dari penggunaan aplikasi Jerigen pada perangkat rooted atau jail-broken dan Kami mempunyai diskresi untuk menghentikan Penggunaan Anda terhadap Aplikasi pada perangkat rooted atau jail-broken dan memblokirperangkat rooted atau jail-broken untuk menggunakan Aplikasi.


Tindakan yang Kami Anggap Perlu

Apabila Kami mengetahui atau mempunyai alasan yang cukup untuk menduga bahwa Anda telah melakukan tindakan asusila, pelanggaran, kejahatan atau tindakan lain yang bertentangan dengan Ketentuan Penggunaan ini dan/atau peraturan perundang-undangan yang berlaku, baik yang dirujuk dalam Ketentuan Penggunaan ini atau tidak, maka Kami berhak untuk dan dapat membekukan Akun, baik sementara atau permanen, atau menghentikan akses Anda terhadap Aplikasi, termasuk Layanan, Konten Pihak Ketiga, Penawaran dan/atau Metode Pembayaran yang terdapat di dalamnya, melakukan pemeriksaan, menuntut ganti kerugian, melaporkan kepada pihak berwenang dan/atau mengambil tindakan lain yang kami anggap perlu, termasuk tindakan hukum pidana maupun perdata.


Kami akan menindaklanjuti dengan melakukan investigasi dan/atau memfasilitasi Penyedia Layanan yang bersangkutan untuk melaporkan kepada pihak yang berwajib apabila Kami menerima Laporan adanya pelanggaran yang Anda lakukan atas Ketentuan Penggunaan ini ataupun pelanggaran terhadap peraturan perundang-undangan yang berlaku, sehubungan dengan:


Pelecehan atau kekerasan verbal, termasuk namun tidak terbatas pada, atas fisik, jenis kelamin, suku, agama dan ras, yang Anda lakukan terhadap Penyedia Layanan;

Tindakan penipuan

Penggunaan Aplikasi pada perangkat rooted atau jail-broken;

Pelecehan atau kekerasan fisik yang Anda lakukan terhadap Penyedia Layanan; dan/atau

Pelecehan atau kekerasan seksual, baik secara verbal maupun fisik, yang Anda lakukan terhadap Penyedia Layanan.

Ketentuan ini juga berlaku sebaliknya apabila Anda mengalami tindakan sebagaimana disebutkan di atas yang dilakukan oleh Penyedia Layanan terhadap Anda.


Pernyataan Anda

Anda menyatakan dan menjamin bahwa Anda telah berusia minimal 18 (delapan belas) tahun atau sudah menikah dan tidak berada di bawah perwalian atau pengampuan dan Anda secara hukum memiliki kapasitas dan berhak untuk mengikatkan diri pada Ketentuan Penggunaan ini. Jika Anda tidak memenuhi ketentuan tersebut namun tetap mengakses atau menggunakan Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran, Anda menyatakan dan menjamin bahwa tindakan Anda membuka, mengakses atau melakukan aktivitas lain dalam Aplikasi telah disetujui oleh orang tua, wali atau pengampu Anda. Anda secara tegas mengesampingkan setiap hak berdasarkan hukum untuk membatalkan atau mencabut setiap dan seluruh persetujuan yang Anda berikan berdasarkan Ketentuan Penggunaan ini pada waktu Anda dianggap telah dewasa secara hukum.


Anda setuju untuk mengakses atau menggunakan Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran hanya untuk tujuan sebagaimana ditentukan dalam Ketentuan Penggunaan ini dan tidak menyalahgunakan atau menggunakan Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran atau Metode Pembayaran untuk tujuan penipuan, menyebabkan ketidaknyamanan kepada orang lain, melakukan pemesanan palsu atau yang tindakan-tindakan lain yang dapat atau dianggap dapat menimbulkan kerugian dalam bentuk apapun terhadap orang lain.


Anda memahami dan menyetujui bahwa seluruh resiko yang timbul dari penggunaan Aplikasi, Layanan, Konten Pihak Ketiga, Penawaran dan Metode Pembayaran sepenuhnya menjadi tanggung jawab Anda dan Anda dengan ini setuju untuk melepaskan Kami dari segala tuntutan apapun sehubungan dengan kerusakan, gangguan, atau bentuk lain dari gangguan sistem yang disebabkan oleh akses tidak resmi oleh pihak lain.


Anda memahami bahwa Aplikasi menggunakan data dari layanan dan konten Google Maps, dan dengan menggunakan Aplikasi, anda setuju untuk mematuhi ketentuan layanan Google Maps berikut: 


(i) Google Maps/ Google Earth Additional Terms of Services at https://maps.google.com/help/terms_maps.html;
(ii) Google Privacy Policy at https://www.google.com/policies/privacy/; and 
(iii)Google Acceptable Service Policy at https://cloud.google.com/maps-platform/terms/aup/.

Anda secara tegas membebaskan Kami, termasuk namun tidak terbatas pada, pejabat, direktur, komisaris, karyawan dan agen Kami, dari dari setiap dan semua kewajiban, konsekuensi, kerugian baik materiil atau immateriil, tuntutan, biaya-biaya (termasuk biaya advokat) atau tanggung jawab hukum lainnya yang timbul atau mungkin timbul akibat pelanggaran Anda terhadap Ketentuan Penggunaan ini maupun sehubungan dengan tindakan Penyedia Layanan, Penyedia Konten Pihak Ketiga, Penyedia Penawaran atau Penyedia Metode Pembayaran.

Dikecualikan dari ketentuan di atas, jika terdapat kerugian langsung akibat pelanggaran Kami terhadap Ketentuan Penggunaan ini, maka Anda setuju dan secara tegas membatasi jumlah tuntutan Anda sebesar jumlah keseluruhan yang senyatanya ditimbulkan atau telah dibayarkan sehubungan dengan peristiwa tersebut.

ANDA MENGAKUI BAHWA KAMI ADALAH PERUSAHAAN TEKNOLOGI, BUKAN PERUSAHAAN TRANSPORTASI, KURIR, POS, JASA PENGANTARAN, LOGISTIK, KEUANGAN DAN LAIN-LAIN DAN KAMI TIDAK MEMBERIKAN LAYANAN TRANSPORTASI, KURIR, POS, JASA PENGANTARAN, LOGISTIK, KEUANGAN DAN LAYANAN LAIN. SEMUA LAYANAN TRANSPORTASI, KURIR, POS, JASA PENGANTARAN, LOGISTIK, KEUANGAN DAN LAYANAN LAIN DISEDIAKAN PIHAK KETIGA INDEPENDEN YANG TIDAK DIPEKERJAKAN OLEH KAMI.

DENGAN MELANJUTKAN AKSES ATAU PENGGUNAAN TERHADAP APLIKASI JERIGEN DAN/ATAU LAYANAN, ANDA SETUJU UNTUK TUNDUK DAN MEMATUHI SEMUA KETENTUAN PERATURAN PERUNDANG-UNDANGAN TERKAIT DAN KETENTUAN PENGGUNAAN INI, TERMASUK SEMUA PERUBAHANNYA DAN KETENTUAN PENGGUNAAN DARI SETIAP PENYEDIA LAYANAN, PENYEDIA KONTEN PIHAK KETIGA, PENYEDIA PENAWARAN ATAU PENYEDIA METODE PEMBAYARAN. SEGERA HENTIKAN AKSES ATAU PENGGUNAAN APLIKASI JERIGEN DAN/ATAU LAYANAN JIKA ANDA TIDAK SETUJU DENGAN BAGIAN APAPUN DARI KETENTUAN PENGGUNAAN INI.

Keadaan Kahar

Aplikasi dapat diinterupsi oleh kejadian di luar kewenangan atau kontrol Kami (“Keadaan Kahar”), termasuk namun tidak terbatas pada bencana alam, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, dan lain-lain. Anda setuju untuk membebaskan Kami dari setiap tuntutan dan tanggung jawab, jika Kami tidak dapat memfasilitasi Layanan, termasuk memenuhi instruksi yang Anda berikan melalui Aplikasi, baik sebagian maupun seluruhnya, karena suatu Keadaan Kahar.


Lain-lain

Anda mengerti dan setuju bahwa Ketentuan Penggunaan ini merupakan perjanjian dalam bentuk elektronik dan tindakan Anda menekan tombol ‘daftar’ saat pembukaan Akun atau tombol ‘masuk’ saat akan mengakses Akun Anda merupakan persetujuan aktif Anda untuk mengikatkan diri dalam perjanjian dengan Kami sehingga keberlakuan Ketentuan Penggunaan ini dan Kebijakan Privasi adalah sah dan mengikat secara hukum dan terus berlaku sepanjang penggunaan Aplikasi dan Layanan oleh Anda.


Anda tidak akan mengajukan tuntutan atau keberatan apapun terhadap keabsahan dari Ketentuan Penggunaan atau Kebijakan Privasi yang dibuat dalam bentuk elektronik.


Anda tidak dapat mengalihkan hak Anda berdasarkan Ketentuan Penggunaan ini tanpa persetujuan tertulis sebelumnya dari Kami. Namun, Kami dapat mengalihkan hak Kami berdasarkan Ketentuan Penggunaan ini setiap saat kepada pihak lain tanpa perlu mendapatkan persetujuan terlebih dahulu dari atau memberikan pemberitahuan sebelumya kepada Anda.


Bila Anda tidak mematuhi atau melanggar ketentuan dalam Ketentuan Penggunaan ini, dan Kami tidak mengambil tindakan secara langsung, bukan berarti Kami mengesampingkan hak Kami untuk mengambil tindakan yang diperlukan di kemudian hari.


Ketentuan ini tetap berlaku bahkan setelah pembekuan sementara, pembekuan permanen, penghapusan Aplikasi atau setelah berakhirnya perjanjian ini antara Anda dan Kami.


Jika salah satu dari ketentuan dalam Ketentuan Penggunaan ini tidak dapat diberlakukan, hal tersebut tidak akan memengaruhi ketentuan lainnya.


Kami membuat Ketentuan Penggunaan ini dalam bahasa Indonesia dan Inggris. Setiap perbedaan akan diartikan sesuai dengan bahasa Indonesia.


Ketentuan Penggunaan ini diatur berdasarkan hukum Republik Indonesia. Setiap dan seluruh perselisihan yang timbul dari penggunaan Aplikasi atau Layanan tunduk pada yurisdiksi eksklusif Pengadilan Negeri Jakarta Selatan.


Cara Menghubungi Kami

Anda dapat menghubungi Kami melalui surat elektronik ke halo@jerigen.id atau melalui telepon ke nomor (021) 5084 9000. Semua korespondensi Anda akan dicatat, direkam dan disimpan untuk arsip Kami.


Saya telah membaca dan mengerti seluruh Ketentuan Penggunaan ini dan konsekuensinya dan dengan ini menerima setiap hak, kewajiban, dan ketentuan yang diatur di dalamnya.
            ''',
            textAlign: TextAlign.justify,
            style: TextStyle(
              color: COLOR_PRIMARY_DARK_TEXT,
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}
