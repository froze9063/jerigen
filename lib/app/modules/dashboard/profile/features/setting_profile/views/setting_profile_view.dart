import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jerrycan_master/app/widgets/progress_loading.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../../../utils/constant.dart';
import '../../../../../../widgets/app_bar.dart';
import '../../../../../../widgets/button.dart';
import '../../../../../../widgets/general.dart';
import '../controllers/setting_profile_controller.dart';

class SettingProfileView extends GetView<SettingProfileController> {
  final SettingProfileController _controller =
      Get.put(SettingProfileController());

  final _focus1 = FocusNode();
  final _focus2 = FocusNode();
  final _focus3 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar(
        'Informasi Personal',
        () => Get.back(),
        elevation: 1,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(24),
          child: Column(
            children: [
              _buildAvatar(context),
              SizedBox(height: 8),
              Text(
                _controller.user?.idNumber ?? "",
                style: TextStyle(
                  color: COLOR_PRIMARY_TEXT,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
              ),
              SizedBox(height: 24),
              _buildForm(context),
              Obx(
                () => _controller.isLoading.value
                    ? CircularProgressIndicator()
                    : Obx(
                        () => TextButtonPrimary(
                          isEnable: _controller.isPressing.value,
                          text: "Simpan",
                          onPressed: () {
                            _controller.updateProfile();
                          },
                        ),
                      ),
              ),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAvatar(BuildContext context) {
    var pic = _controller.user?.profilePictUrl ?? "";
    return Center(
      child: Stack(
        children: [
          Positioned(
            right: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10000),
              child: Material(
                child: InkWell(
                  onTap: () {
                    _showDialog(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Image.asset(
                      "assets/icons/ic_edit.png",
                      width: 28,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Obx(
              () => _controller.uploadLoading.value
                  ? ProgressLoading()
                  : _controller.image.value.isNotEmpty
                      ? _circleImage(context, _controller.image.value)
                      : pic.isNotEmpty
                          ? _circleImage(
                              context,
                              _controller.user?.profilePictUrl ?? "",
                            )
                          : Image.asset(
                              "assets/images/img_default.png",
                              height: 130,
                            ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _circleImage(BuildContext context, String url) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10000),
      child: Material(
        color: Colors.grey,
        child: InkWell(
          onTap: () {
            _showImage(
              context,
              CachedNetworkImageProvider(url),
            );
          },
          child: CachedNetworkImage(
            height: 130,
            width: 130,
            fit: BoxFit.fill,
            imageUrl: url,
          ),
        ),
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return defaultForm(_controller.formKey, [
      defaultTextFormField(
        "Nama Lengkap",
        _controller.name,
        _onChange,
        _focus1,
        _focus2,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Nomor Handphone",
        _controller.hp,
        _onChange,
        _focus2,
        _focus3,
        inputType: TextInputType.number,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Email",
        _controller.email,
        _onChange,
        _focus3,
        null,
        inputType: TextInputType.emailAddress,
      ),
      SizedBox(height: 16),
    ]);
  }

  _onChange(value) {
    _controller.checkEnable();
  }

  _showImage(BuildContext context, ImageProvider provider) {
    showCupertinoModalBottomSheet(
      expand: false,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Scaffold(
        appBar: AppBar(),
        body: Container(child: Container()
            // PhotoView(imageProvider: provider),
            ),
      ),
    );
  }

  _showDialog(context) {
    final ImagePicker _picker = ImagePicker();
    showCupertinoModalBottomSheet(
      expand: false,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        child: Material(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildOptionButton('Kamera', () async {
                XFile? picked =
                    await _picker.pickImage(source: ImageSource.camera);
                if (picked != null) {
                  _cropImage(context, picked);
                }
              }),
              Container(height: 1, color: Colors.grey[200]),
              _buildOptionButton('Galeri', () async {
                XFile? picked =
                    await _picker.pickImage(source: ImageSource.gallery);
                if (picked != null) {
                  _cropImage(context, picked);
                }
              }),
              Container(height: 1, color: Colors.grey[200]),
              _buildOptionButton('Cancel', () => Navigator.pop(context)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildOptionButton(String title, GestureTapCallback? onTap) {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 16),
        ),
      ),
      onTap: onTap,
    );
  }

  Future<Null> _cropImage(BuildContext context, XFile image) async {
    print(image.path);
    Get.back();

    File? croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
      compressQuality: 50,
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Crop Gambar',
          toolbarColor: COLOR_PRIMARY,
          toolbarWidgetColor: Colors.white,
          lockAspectRatio: true),
      iosUiSettings: IOSUiSettings(
          title: 'Crop Gambar',
          aspectRatioLockEnabled: true,
          rotateButtonsHidden: true),
    );

    if (croppedFile != null) {
      print("cropped : " + croppedFile.path);
      File? compressed = await _doCompressed(croppedFile);
      if (compressed != null) {
        print("compressed : " + compressed.path);
        _controller.postImage(compressed);
      }
    }
  }

  Future<File?> _doCompressed(File file) async {
    final String filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
    var result = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path, outPath,
        quality: 50);
    return result;
  }
}
