import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';

import '../../../../../../data/model/base_response.dart';
import '../../../../../../data/model/register/register_request.dart';
import '../../../../../../data/repository/user_repository.dart';
import '../../../../../../routes/app_pages.dart';
import '../../../../../../utils/constant.dart';
import '../../../../../../utils/failure.dart';
import '../../../../../../widgets/custom_toast.dart';

class SettingProfileController extends GetxController {
  final formKey = GlobalKey<FormState>();
  late TextEditingController name;
  late TextEditingController email;
  late TextEditingController hp;

  final RxBool isPressing = false.obs;
  final RxBool isLoading = false.obs;
  final RxBool uploadLoading = false.obs;
  final RxString image = "".obs;

  final user = Singleton.loginResponse.user;

  final UserRepository _userRepository = UserRepository();

  @override
  void onInit() {
    super.onInit();
    name = TextEditingController(text: user?.fullName);
    email = TextEditingController(text: user?.userEmail);
    hp = TextEditingController(text: user?.phoneNumber);

    checkEnable();
  }

  @override
  void onClose() {
    name.dispose();
    hp.dispose();
    email.dispose();
  }

  void checkEnable() {
    isPressing.value = name.value.text.isNotEmpty &&
        email.value.text.isNotEmpty &&
        hp.value.text.isNotEmpty;
  }

  Future<void> postImage(File file) async {
    try {
      uploadLoading.value = true;

      List<String> data = await _userRepository.postMedia('user', [file]);
      if (data.isNotEmpty)
        image.value = data[0];
      else
        showMessage("Gagal mengupload gambar");
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      uploadLoading.value = false;
    }
  }

  Future<void> updateProfile() async {
    try {
      _updateIsLoading(true);

      RegisterRequest body = image.value.isNotEmpty
          ? RegisterRequest(
              fullName: name.value.text,
              userEmail: email.value.text,
              phoneNumber: hp.value.text,
              profilePictUrl: image.value)
          : RegisterRequest(
              fullName: name.value.text,
              userEmail: email.value.text,
              phoneNumber: hp.value.text);

      await _userRepository.updateProfile(body).then((response) {
        if (response) {
          showMessage("Update Profile Berhasil");
          Get.offNamed(Routes.DASHBOARD);
        } else {
          showMessage(DEFAULT_ERROR);
        }
      });
    } on DioError catch (e) {
      DioErrorType errorType = e.type;
      if (errorType == DioErrorType.connectTimeout ||
          errorType == DioErrorType.receiveTimeout ||
          errorType == DioErrorType.sendTimeout) {
        showMessage(ERROR_TIMEOUT);
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(e.response!.data);
        showMessage(baseResponse.message!);
      }
    } on Exception catch (e) {
      showMessage(e.toString());
    } on RequestException catch (e) {
      showMessage(e.code!);
    } catch (e) {
      showMessage(DEFAULT_ERROR);
    } finally {
      _updateIsLoading(false);
    }
  }

  void _updateIsLoading(bool currentStatus) {
    isLoading.value = currentStatus;
  }

  void showMessage(String message) {
    CustomToast.showToast(message);
  }
}
