// Created by ferdyhaspin on 21/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/dashboard/home/views/home_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/messages/views/messages_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/points/features/point_new/views/point_new_view.dart';
import 'package:jerrycan_master/app/modules/dashboard/profile/views/profile_view.dart';
import 'package:jerrycan_master/app/routes/app_pages.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/constant.dart';
import '../../widgets/button.dart';
import 'activity/views/activity_view.dart';

class DashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _selectedIndex = 0;

  List<Widget> _widgetOptions = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  initState() {
    super.initState();
    _initMenu();
    _showSuccess();
    Singleton.getAddressList();
  }

  void _initMenu() {
    _widgetOptions = <Widget>[
      HomeView(_onItemTapped),
      ActivityView(),
      PointNewView(),
      MessagesView(),
      ProfileView(),
    ];
  }

  void _showSuccess() {
    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => _showSuccessCreated(context));
  }

  Future<bool> _onWillPop() async {
    if (_selectedIndex != 0) {
      _onItemTapped(0);
      return false;
    } else {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: IndexedStack(
          index: _selectedIndex,
          children: _widgetOptions,
        ),
        bottomNavigationBar: _bottomNavBar(),
      ),
    );
  }

  Widget _bottomNavBar() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Image.asset(
            _selectedIndex == 0
                ? "assets/icons/ic_home_bold.png"
                : "assets/icons/ic_grey_home.png",
            height: 24,
            width: 24,
          ),
          label: "Home",
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            _selectedIndex == 1
                ? "assets/icons/ic_blue_activity.png"
                : "assets/icons/ic_grey_activity.png",
            height: 24,
            width: 24,
          ),
          label: "Activity",
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            _selectedIndex == 2
                ? "assets/icons/ic_blue_points.png"
                : "assets/icons/ic_points.png",
            height: 24,
            width: 24,
          ),
          label: "Poin",
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            _selectedIndex == 3
                ? "assets/icons/ic_blue_message.png"
                : "assets/icons/ic_messages.png",
            height: 24,
            width: 24,
          ),
          label: "Messages",
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            _selectedIndex == 4
                ? "assets/icons/ic_blue_profile.png"
                : "assets/icons/ic_profile.png",
            height: 24,
            width: 24,
          ),
          label: "Profile",
        ),
      ],
      currentIndex: _selectedIndex,
      unselectedItemColor: Color.fromRGBO(147, 147, 147, 1.0),
      selectedItemColor: Color.fromRGBO(92, 225, 230, 1.0),
      onTap: _onItemTapped,
      selectedLabelStyle: TextStyle(fontSize: 10),
      unselectedLabelStyle: TextStyle(fontSize: 10),
      showSelectedLabels: true,
      showUnselectedLabels: true,
    );
  }

  Future<void> _showSuccessCreated(context) async {
    bool isFirstLogin = Singleton.loginResponse.user?.isFirstLogin ?? false;
    if (!isFirstLogin) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      bool doShow = preferences.getBool(PREFS_DO_SHARE_REFERRAL) ?? false;
      if (!doShow) {
        preferences.setBool(PREFS_DO_SHARE_REFERRAL, true);

        showCupertinoModalBottomSheet(
          context: context,
          isDismissible: false,
          enableDrag: false,
          backgroundColor: Colors.transparent,
          builder: (context) => WillPopScope(
            onWillPop: () async => false,
            child: Material(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 57,
                      height: 4,
                      decoration: BoxDecoration(
                        color: Color(0xFFDEDEDE),
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                    ),
                    SizedBox(height: 32),
                    Text(
                      'Akun anda berhasil dibuat!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: COLOR_PRIMARY_TEXT,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Selamat bergabung menjadi Agent of Change, ${Singleton.loginResponse.user?.idNumber}!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: COLOR_PRIMARY_TEXT,
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      'Apakah anda ingin jerigen kosong 5 liter, GRATIS?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: COLOR_PRIMARY_TEXT,
                      ),
                    ),
                    SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Get.width / 2 - 25,
                          height: 50,
                          child: TextButtonPrimaryWrap(
                            isEnable: true,
                            text: 'Iya',
                            onPressed: () {
                              Get.back();
                              _showShareReferral(context);
                            },
                          ),
                        ),
                        SizedBox(
                          width: Get.width / 2 - 25,
                          height: 50,
                          child: TextButtonGreyWrap(
                            text: 'Tidak',
                            onPressed: () {
                              Get.back();
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 16),
                  ],
                ),
              ),
            ),
          ),
        );
      }
    }
  }

  Future<void> _showShareReferral(context) async {
    showCupertinoModalBottomSheet(
      isDismissible: false,
      enableDrag: false,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => WillPopScope(
        onWillPop: () async => false,
        child: Material(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 32),
                Text(
                  'Bagikan kode referral di bawah ini kepada satu orang teman anda. Gunakan kode tersebut di halaman daftar.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: COLOR_PRIMARY_TEXT,
                  ),
                ),
                SizedBox(height: 8),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'G67X1',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                          color: COLOR_PRIMARY_TEXT,
                        ),
                      ),
                      SizedBox(width: 8),
                      IconButton(
                        icon: Icon(Icons.share),
                        color: Colors.grey,
                        onPressed: () {
                          Share.share(
                              'Ayo bergabung bersama saya menjadi Agent of Change Jerigen, Agen Perubahan yang membantu mengurangi sampah minyak goreng bekas. Demi bumi yang lebih baik!'
                              '\n\nKlik link di bawah ini untuk install aplikasi Jerigen: https://bit.ly/jerigen-app'
                              '\n\nMasukkan kode referral ketika mendaftar: 82jGc5!');
                        },
                      )
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  'Masukkan alamat anda. Transporter kami akan mengirimkan jerigen kosong 5 liter ke alamat anda setelah kode tersebut digunakan.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: COLOR_PRIMARY_TEXT,
                  ),
                ),
                SizedBox(height: 16),
                TextButtonPrimary(
                  isEnable: true,
                  text: 'Tambah Alamat',
                  onPressed: () {
                    Get.back();
                    Get.toNamed(Routes.ADD_ADDRESS);
                  },
                ),
                SizedBox(height: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
