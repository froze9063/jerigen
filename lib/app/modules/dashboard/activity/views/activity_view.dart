import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

import '../../../../data/model/home/activity.dart';
import '../../../../widgets/app_bar.dart';
import 'widget/activity_content.dart';

class ActivityView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ActivityViewState();
}

class _ActivityViewState extends State<ActivityView>
    with SingleTickerProviderStateMixin {
  late TabController controller;

  final List<Activity> _ongoing = [
    Activity(
        type: 'Request Pick Up',
        address: 'JL. Anggrek Nelimurni VI Blok AB3',
        addressDetail: 'Jakarta Greater Area',
        liter: '180 Kilogram = 10 Jerigen',
        point: '1080 Potensi Poin',
        transporter: 'Transporter: 300 Poin',
        donasi: 'Donasi: 5 Poin',
        time: '21/08 09.37'),
  ];
  final List<Activity> _upcoming = [];
  final List<Activity> _history = [
    Activity(
        type: 'Picked Up',
        address: 'JL. Anggrek Nelimurni VI Blok AB3',
        addressDetail: 'Jakarta Greater Area',
        liter: '36 Kilogram = 2 Jerigen',
        point: '216 Potensi Poin',
        transporter: 'Transporter: 30 Poin',
        donasi: 'Donasi: 5 Poin',
        time: '21/08 13.21'),
    Activity(
        type: 'Cancelled',
        address: 'JL. Anggrek Nelimurni VI Blok AB3',
        addressDetail: 'Jakarta Greater Area',
        liter: '18 Kilogram = 1 Jerigen',
        point: '108 Potensi Poin',
        transporter: 'Transporter: 30 Poin',
        donasi: 'Donasi: 0 Poin',
        time: '21/08 10.15'),
  ];

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: defaultAppBar('Activity', null, elevation: 0),
      body: Column(
        children: [
          _buildTabs(context),
          Expanded(
            child: _buildTabBarViews(context),
          ),
        ],
      ),
    );
  }

  Widget _buildTabs(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 2,
      child: TabBar(
        labelColor: COLOR_PRIMARY_DARK_TEXT,
        unselectedLabelColor: COLOR_PRIMARY_TEXT,
        indicatorColor: COLOR_INDICATOR,
        labelStyle: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w300,
        ),
        controller: controller,
        tabs: <Widget>[
          Tab(text: 'On Going'),
          Tab(text: 'Upcoming'),
          Tab(text: 'History'),
        ],
      ),
    );
  }

  Widget _buildTabBarViews(BuildContext context) {
    return TabBarView(
      controller: controller,
      children: <Widget>[
        ActivityContent(contents: _ongoing),
        ActivityContent(contents: _upcoming),
        ActivityContent(contents: _history),
      ],
    );
  }
}
