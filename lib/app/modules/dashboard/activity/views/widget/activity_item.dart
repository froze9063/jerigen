// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/data/model/home/activity.dart';

import '../../../../../utils/constant.dart';

class ActivityItem extends StatelessWidget {
  final Activity activity;

  const ActivityItem({Key? key, required this.activity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(height: 1, color: COLOR_PRIMARY),
          SizedBox(height: 12),
          Stack(
            children: [
              Container(
                width: double.infinity,
                margin: const EdgeInsets.only(top: 8),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: COLOR_PRIMARY),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  children: [
                    Text(
                      '${activity.address}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: COLOR_PRIMARY_DARK_TEXT,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 4),
                    Text(
                      '${activity.addressDetail}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: COLOR_PRIMARY_TEXT,
                        fontWeight: FontWeight.w700,
                        fontSize: 10,
                      ),
                    ),
                    SizedBox(height: 8),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${activity.liter}',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: COLOR_PRIMARY_TEXT,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                ),
                              ),
                              Text(
                                '${activity.point}',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: COLOR_PRIMARY_TEXT,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${activity.transporter}',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: COLOR_PRIMARY_TEXT,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                ),
                              ),
                              Text(
                                '${activity.donasi}',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: COLOR_PRIMARY_TEXT,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Positioned(
                top: 0,
                right: 10,
                child: Text(
                  '  ${activity.type}  ',
                  style: TextStyle(
                    color: COLOR_PRIMARY_TEXT,
                    backgroundColor: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 12),
          Text(
            '${activity.time}',
            textAlign: TextAlign.right,
            style: TextStyle(
              color: COLOR_PRIMARY_TEXT,
              fontWeight: FontWeight.w600,
              fontSize: 12,
            ),
          )
        ],
      ),
    );
  }
}
