// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/utils/constant.dart';

class ActivityItemNotAvailable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(36),
      width: double.infinity,
      height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/icons/ic_empty_data.png', width: 62),
          SizedBox(height: 24),
          Text(
            'Data tidak tersedia',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: COLOR_PRIMARY_EXTRA_DARK_TEXT,
            ),
          ),
          SizedBox(height: 12),
          Text(
            'Mulai aktifitas anda dengan mengumpulkan minyak bekas anda pakai',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 12,
              color: COLOR_PRIMARY_TEXT,
            ),
          ),
        ],
      ),
    );
  }
}
