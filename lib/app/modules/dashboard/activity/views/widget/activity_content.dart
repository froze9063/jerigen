// Created by ferdyhaspin on 30/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:flutter/material.dart';
import 'package:jerrycan_master/app/data/model/home/activity.dart';

import 'activity_item.dart';
import 'activity_item_not_available.dart';

class ActivityContent extends StatelessWidget {
  final List<Activity> contents;

  const ActivityContent({Key? key, required this.contents}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (contents.isEmpty) {
      return ActivityItemNotAvailable();
    } else {
      return _buildDataAvailable();
    }
  }

  Widget _buildDataAvailable() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          children: [
            ...List.generate(contents.length, (index) {
              return ActivityItem(activity: contents[index]);
            }),
          ],
        ),
      ),
    );
  }
}
