import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jerrycan_master/app/modules/register/views/register_view.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/widgets/button.dart';
import 'package:jerrycan_master/app/widgets/general.dart';

import '../../../widgets/status_bar_primary.dart';
import '../controllers/login_controller.dart';

class LoginView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final LoginController _controller = Get.put(LoginController());
  final _focus1 = FocusNode();
  final _focus2 = FocusNode();

  @override
  Widget build(BuildContext context) {
    return StatusBarPrimary(
      child: Scaffold(
        backgroundColor: COLOR_PRIMARY,
        body: _body(),
      ),
    );
  }

  Widget _body() {
    return LayoutBuilder(
      builder: (context, constraint) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child:Column(
                children: [
                  SizedBox(height: 30),
                  Image.asset("assets/images/img_welcome.png"),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(24),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16),
                          topRight: Radius.circular(16),
                        ),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "Login",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 24),
                          _buildForm(),
                          SizedBox(height: 16),
                          Obx(() => _controller.isLoading.value
                              ? CircularProgressIndicator()
                              : Obx(() => TextButtonPrimary(
                            isEnable: _controller.isPressing.value,
                            text: "Masuk",
                            onPressed: _controller.doLogin,
                          ))),
                          SizedBox(height: 24),
                          RichText(
                            text: TextSpan(
                              text: 'Belum terdaftar? ',
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.black,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Daftar',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: COLOR_PRIMARY,
                                  ),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () => Get.to(() => RegisterView()),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildForm() {
    return defaultForm(_controller.formKey, [
      defaultTextFormField(
        "Email / Username",
        _controller.email,
        _onChanged,
        _focus1,
        _focus2,
        inputType: TextInputType.emailAddress,
      ),
      SizedBox(height: 16),
      defaultTextFormField(
        "Password",
        _controller.password,
        _onChanged,
        _focus2,
        null,
        action: TextInputAction.done,
        obscureText: true,
      ),
    ]);
  }

  void _onChanged(value) {
    _controller.checkEnable();
  }
}
