/// data : [{"id":1,"city_name":"Jakarta Barat","city_desc":"Jakarta Barat","city_photos":null,"province_id":0},{"id":2,"city_name":"Jakarta Selatan","city_desc":"Jakarta Selatan","city_photos":null,"province_id":0},{"id":3,"city_name":"Jakarta Pusat","city_desc":"Jakarta Pusat","city_photos":null,"province_id":0},{"id":4,"city_name":"Jakarta Timur","city_desc":"Jakarta Timur","city_photos":null,"province_id":0},{"id":5,"city_name":"Jakarta Utara","city_desc":"Jakarta Utara","city_photos":null,"province_id":0},{"id":6,"city_name":"Bekasi","city_desc":"Bekasi","city_photos":null,"province_id":0},{"id":7,"city_name":"Depok","city_desc":"Depok","city_photos":null,"province_id":0},{"id":8,"city_name":"Bogor","city_desc":"Bogor","city_photos":null,"province_id":0}]
/// paginator : {"current_page":1,"limit_per_page":8,"back_page":1,"next_page":1,"total_records":8,"total_pages":1}

class ChooseLocationResponse {
  ChooseLocationResponse({
      List<ChooseListLocation>? data,
      Paginator? paginator,}){
    _data = data;
    _paginator = paginator;
}

  ChooseLocationResponse.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(ChooseListLocation.fromJson(v));
      });
    }
    _paginator = json['paginator'] != null ? Paginator.fromJson(json['paginator']) : null;
  }
  List<ChooseListLocation>? _data;
  Paginator? _paginator;

  List<ChooseListLocation>? get data => _data;
  Paginator? get paginator => _paginator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    if (_paginator != null) {
      map['paginator'] = _paginator?.toJson();
    }
    return map;
  }

}

/// current_page : 1
/// limit_per_page : 8
/// back_page : 1
/// next_page : 1
/// total_records : 8
/// total_pages : 1

class Paginator {
  Paginator({
      int? currentPage,
      int? limitPerPage,
      int? backPage,
      int? nextPage,
      int? totalRecords,
      int? totalPages,}){
    _currentPage = currentPage;
    _limitPerPage = limitPerPage;
    _backPage = backPage;
    _nextPage = nextPage;
    _totalRecords = totalRecords;
    _totalPages = totalPages;
}

  Paginator.fromJson(dynamic json) {
    _currentPage = json['current_page'];
    _limitPerPage = json['limit_per_page'];
    _backPage = json['back_page'];
    _nextPage = json['next_page'];
    _totalRecords = json['total_records'];
    _totalPages = json['total_pages'];
  }
  int? _currentPage;
  int? _limitPerPage;
  int? _backPage;
  int? _nextPage;
  int? _totalRecords;
  int? _totalPages;

  int? get currentPage => _currentPage;
  int? get limitPerPage => _limitPerPage;
  int? get backPage => _backPage;
  int? get nextPage => _nextPage;
  int? get totalRecords => _totalRecords;
  int? get totalPages => _totalPages;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['current_page'] = _currentPage;
    map['limit_per_page'] = _limitPerPage;
    map['back_page'] = _backPage;
    map['next_page'] = _nextPage;
    map['total_records'] = _totalRecords;
    map['total_pages'] = _totalPages;
    return map;
  }

}

/// id : 1
/// city_name : "Jakarta Barat"
/// city_desc : "Jakarta Barat"
/// city_photos : null
/// province_id : 0

class ChooseListLocation {
  ChooseListLocation({
      int? id,
      String? cityName,
      String? cityDesc,
      dynamic cityPhotos,
      int? provinceId,}){
    _id = id;
    _cityName = cityName;
    _cityDesc = cityDesc;
    _cityPhotos = cityPhotos;
    _provinceId = provinceId;
}

  ChooseListLocation.fromJson(dynamic json) {
    _id = json['id'];
    _cityName = json['city_name'];
    _cityDesc = json['city_desc'];
    _cityPhotos = json['city_photos'];
    _provinceId = json['province_id'];
  }
  int? _id;
  String? _cityName;
  String? _cityDesc;
  dynamic _cityPhotos;
  int? _provinceId;

  int? get id => _id;
  String? get cityName => _cityName;
  String? get cityDesc => _cityDesc;
  dynamic get cityPhotos => _cityPhotos;
  int? get provinceId => _provinceId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['city_name'] = _cityName;
    map['city_desc'] = _cityDesc;
    map['city_photos'] = _cityPhotos;
    map['province_id'] = _provinceId;
    return map;
  }

}