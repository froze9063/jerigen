// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

class BaseResponse {
  int? code;
  String? desc;
  String? message;
  String? error;
  dynamic data;

  BaseResponse({this.code, this.error, this.message, this.desc, this.data});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    code = json['status_code'];
    desc = json['status_desc'];
    error = json['errors'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['status_code'] = this.code;
    data['status_desc'] = this.desc;
    data['errors'] = this.error;
    data['message'] = this.message;
    return data;
  }
}
