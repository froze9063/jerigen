/// address : "string"
/// dob : "string"
/// full_name : "string"
/// gender : 0
/// id_number : "string"
/// password : "string"
/// phone_number : "string"
/// profile_pict_url : "string"
/// referral_code : "string"
/// role : 0
/// user_email : "string"

class RegisterRequest {
  RegisterRequest({
      String? address, 
      String? dob, 
      String? fullName, 
      int? gender, 
      String? idNumber, 
      String? password, 
      String? phoneNumber, 
      String? profilePictUrl, 
      String? referralCode, 
      int? role, 
      String? userEmail,}){
    _address = address;
    _dob = dob;
    _fullName = fullName;
    _gender = gender;
    _idNumber = idNumber;
    _password = password;
    _phoneNumber = phoneNumber;
    _profilePictUrl = profilePictUrl;
    _referralCode = referralCode;
    _role = role;
    _userEmail = userEmail;
}

  RegisterRequest.fromJson(dynamic json) {
    _address = json['address'];
    _dob = json['dob'];
    _fullName = json['full_name'];
    _gender = json['gender'];
    _idNumber = json['id_number'];
    _password = json['password'];
    _phoneNumber = json['phone_number'];
    _profilePictUrl = json['profile_pict_url'];
    _referralCode = json['referral_code'];
    _role = json['role'];
    _userEmail = json['user_email'];
  }
  String? _address;
  String? _dob;
  String? _fullName;
  int? _gender;
  String? _idNumber;
  String? _password;
  String? _phoneNumber;
  String? _profilePictUrl;
  String? _referralCode;
  int? _role;
  String? _userEmail;

  String? get address => _address;
  String? get dob => _dob;
  String? get fullName => _fullName;
  int? get gender => _gender;
  String? get idNumber => _idNumber;
  String? get password => _password;
  String? get phoneNumber => _phoneNumber;
  String? get profilePictUrl => _profilePictUrl;
  String? get referralCode => _referralCode;
  int? get role => _role;
  String? get userEmail => _userEmail;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['address'] = _address;
    map['dob'] = _dob;
    map['full_name'] = _fullName;
    map['gender'] = _gender;
    map['id_number'] = _idNumber;
    map['password'] = _password;
    map['phone_number'] = _phoneNumber;
    map['profile_pict_url'] = _profilePictUrl;
    map['referral_code'] = _referralCode;
    map['role'] = _role;
    map['user_email'] = _userEmail;
    return map;
  }

}