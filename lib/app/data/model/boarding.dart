/// image : "assets/images/img_boarding_4.png"
/// title : "Dijemput Transporter"
/// description : "Atau Transporter kami akan datang menjemput dengan menggunakan fitur Pick Up di aplikasi ini."

class Boarding {
  Boarding({
    required String image,
    required String title,
    required String description,
  }) {
    _image = image;
    _title = title;
    _description = description;
  }

  Boarding.fromJson(dynamic json) {
    _image = json['image'];
    _title = json['title'];
    _description = json['description'];
  }

  late String _image;
  late String _title;
  late String _description;

  String get image => _image;

  String get title => _title;

  String get description => _description;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['image'] = _image;
    map['title'] = _title;
    map['description'] = _description;
    return map;
  }
}
