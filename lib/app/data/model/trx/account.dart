/// data : [{"account_name":"string","account_no":"string","id":0,"user_id":"string"}]

class AccountList {
  AccountList({
    List<Account>? data,
  }) {
    _data = data;
  }

  AccountList.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Account.fromJson(v));
      });
    }
  }

  List<Account>? _data;

  List<Account>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// account_name : "string"
/// account_no : "string"
/// id : 0
/// user_id : "string"

class Account {
  Account({
    String? accountName,
    String? accountNo,
    String? bankName,
    int? id,
    String? userId,
  }) {
    _accountName = accountName;
    _accountNo = accountNo;
    _bankName = bankName;
    _id = id;
    _userId = userId;
  }

  Account.fromJson(dynamic json) {
    _accountName = json['account_name'];
    _accountNo = json['account_no'];
    _bankName = json['bank_name'];
    _id = json['id'];
    _userId = json['user_id'];
  }

  String? _accountName;
  String? _accountNo;
  String? _bankName;
  int? _id;
  String? _userId;

  String? get accountName => _accountName;

  String? get accountNo => _accountNo;

  String? get bankName => _bankName;

  int? get id => _id;

  String? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['account_name'] = _accountName;
    map['account_no'] = _accountNo;
    map['bank_name'] = _bankName;
    map['id'] = _id;
    map['user_id'] = _userId;
    return map;
  }
}
