/// count_liter : 0
/// depo_id : "string"
/// points : 0
/// points_donation : 0
/// transportation_id : ["string"]

class CreateTrxRequest {
  CreateTrxRequest({
    double? countLiter,
    String? depoId,
    double? points,
    double? pointsDonation,
    List<String>? transportationId,
  }) {
    _countLiter = countLiter;
    _depoId = depoId;
    _points = points;
    _pointsDonation = pointsDonation;
    _transportationId = transportationId;
  }

  CreateTrxRequest.fromJson(dynamic json) {
    _countLiter = json['count_liter'];
    _depoId = json['depo_id'];
    _points = json['points'];
    _pointsDonation = json['points_donation'];
    _transportationId = json['transportation_id'] != null
        ? json['transportation_id'].cast<String>()
        : [];
  }

  double? _countLiter;
  String? _depoId;
  double? _points;
  double? _pointsDonation;
  List<String>? _transportationId;

  double? get countLiter => _countLiter;

  String? get depoId => _depoId;

  double? get points => _points;

  double? get pointsDonation => _pointsDonation;

  List<String>? get transportationId => _transportationId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count_liter'] = _countLiter;
    map['depo_id'] = _depoId;
    map['points'] = _points;
    map['points_donation'] = _pointsDonation;
    map['transportation_id'] = _transportationId;
    return map;
  }
}
