/// id : "b8f7f687-e925-4acb-87fa-6068f8267bb2"
/// user_id : "25c74199-9c06-491c-8a0c-d8ad7eaf288d"
/// full_name : "Radya Driver 2"
/// phone_number : "085776762837"
/// address : "Ciawi"
/// type : 1
/// max_liter : 100
/// current_liter : 0
/// longitude : 106.827
/// latitude : -6.30051

class TransporterResponse {
  TransporterResponse({
      String? id, 
      String? userId, 
      String? fullName, 
      String? phoneNumber, 
      String? address, 
      int? type, 
      int? maxLiter, 
      int? currentLiter, 
      int? pickupLiter,
      double? longitude,
      double? latitude,}){
    _id = id;
    _userId = userId;
    _fullName = fullName;
    _phoneNumber = phoneNumber;
    _address = address;
    _type = type;
    _maxLiter = maxLiter;
    _currentLiter = currentLiter;
    _pickUpLiter = pickupLiter;
    _longitude = longitude;
    _latitude = latitude;
}

  TransporterResponse.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _fullName = json['full_name'];
    _phoneNumber = json['phone_number'];
    _address = json['address'];
    _type = json['type'];
    _maxLiter = json['max_liter'];
    _currentLiter = json['current_liter'];
    _pickUpLiter = json['pick_up_liter'];
    _longitude = json['longitude'];
    _latitude = json['latitude'];
  }
  String? _id;
  String? _userId;
  String? _fullName;
  String? _phoneNumber;
  String? _address;
  int? _type;
  int? _maxLiter;
  int? _currentLiter;
  int? _pickUpLiter;
  double? _longitude;
  double? _latitude;

  String? get id => _id;
  String? get userId => _userId;
  String? get fullName => _fullName;
  String? get phoneNumber => _phoneNumber;
  String? get address => _address;
  int? get type => _type;
  int? get maxLiter => _maxLiter;
  int? get currentLiter => _currentLiter;
  int? get pickupLiter => _pickUpLiter;
  double? get longitude => _longitude;
  double? get latitude => _latitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['full_name'] = _fullName;
    map['phone_number'] = _phoneNumber;
    map['address'] = _address;
    map['type'] = _type;
    map['max_liter'] = _maxLiter;
    map['current_liter'] = _currentLiter;
    map['pick_up_liter'] = _pickUpLiter;
    map['longitude'] = _longitude;
    map['latitude'] = _latitude;
    return map;
  }

}