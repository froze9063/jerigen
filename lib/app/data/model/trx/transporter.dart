// Created by ferdyhaspin on 25/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

class Transporter {
  late String queue;
  late String name;
  late String type;
  late String pick;
  late String total;
  late String status;

  Transporter({
    required this.queue,
    required this.name,
    required this.type,
    required this.pick,
    required this.total,
    required this.status,
  });
}
