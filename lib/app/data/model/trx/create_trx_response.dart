/// count_liter : 0
/// depo_id : "string"
/// id : "string"
/// order_id : "string"
/// points : 0
/// points_donation : 0

class CreateTrxResponse {
  CreateTrxResponse({
      int? countLiter, 
      String? depoId, 
      String? id, 
      String? orderId, 
      int? points, 
      int? pointsDonation,}){
    _countLiter = countLiter;
    _depoId = depoId;
    _id = id;
    _orderId = orderId;
    _points = points;
    _pointsDonation = pointsDonation;
}

  CreateTrxResponse.fromJson(dynamic json) {
    _countLiter = json['count_liter'];
    _depoId = json['depo_id'];
    _id = json['id'];
    _orderId = json['order_id'];
    _points = json['points'];
    _pointsDonation = json['points_donation'];
  }
  int? _countLiter;
  String? _depoId;
  String? _id;
  String? _orderId;
  int? _points;
  int? _pointsDonation;

  int? get countLiter => _countLiter;
  String? get depoId => _depoId;
  String? get id => _id;
  String? get orderId => _orderId;
  int? get points => _points;
  int? get pointsDonation => _pointsDonation;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count_liter'] = _countLiter;
    map['depo_id'] = _depoId;
    map['id'] = _id;
    map['order_id'] = _orderId;
    map['points'] = _points;
    map['points_donation'] = _pointsDonation;
    return map;
  }

}