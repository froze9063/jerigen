/// id : "7cf88fee-e343-40b6-b2e2-473e3e0ab561"
/// depo_code : "D000001"
/// depo_name : "Depo Pak Udin"
/// alamat : "Jl.Slipi"
/// longitude : 106.827
/// latitude : -6.30051
/// no_telephone : "081572364512"
/// profile_pict : "https://akcdn.detik.net.id/visual/2019/10/16/b2efefd4-15f8-4233-bd44-c0d4d0813591_169.jpeg?w=650"
/// barcode : "https://jerigenstorage.blob.core.windows.net/jerigen/barcode.gif"
/// distance : null

class Depo {
  Depo({
    String? id,
    String? depoCode,
    String? depoName,
    String? alamat,
    double? longitude,
    double? latitude,
    String? noTelephone,
    String? profilePict,
    String? barcode,
    dynamic distance,}){
    _id = id;
    _depoCode = depoCode;
    _depoName = depoName;
    _alamat = alamat;
    _longitude = longitude;
    _latitude = latitude;
    _noTelephone = noTelephone;
    _profilePict = profilePict;
    _barcode = barcode;
    _distance = distance;
  }

  Depo.fromJson(dynamic json) {
    _id = json['id'];
    _depoCode = json['depo_code'];
    _depoName = json['depo_name'];
    _alamat = json['alamat'];
    _longitude = json['longitude'];
    _latitude = json['latitude'];
    _noTelephone = json['no_telephone'];
    _profilePict = json['profile_pict'];
    _barcode = json['barcode'];
    _distance = json['distance'];
  }
  String? _id;
  String? _depoCode;
  String? _depoName;
  String? _alamat;
  double? _longitude;
  double? _latitude;
  String? _noTelephone;
  String? _profilePict;
  String? _barcode;
  dynamic _distance;

  String? get id => _id;
  String? get depoCode => _depoCode;
  String? get depoName => _depoName;
  String? get alamat => _alamat;
  double? get longitude => _longitude;
  double? get latitude => _latitude;
  String? get noTelephone => _noTelephone;
  String? get profilePict => _profilePict;
  String? get barcode => _barcode;
  dynamic get distance => _distance;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['depo_code'] = _depoCode;
    map['depo_name'] = _depoName;
    map['alamat'] = _alamat;
    map['longitude'] = _longitude;
    map['latitude'] = _latitude;
    map['no_telephone'] = _noTelephone;
    map['profile_pict'] = _profilePict;
    map['barcode'] = _barcode;
    map['distance'] = _distance;
    return map;
  }

}