/// type : "Request Pick Up"
/// address : "JL. Anggrek Nelimurni VI Blok AB3"
/// address_detail : "Jakarta Great Area"
/// liter : "string"
/// point : "string"
/// transporter : "string"
/// donasi : "string"
/// time : "string"

class Activity {
  Activity({
      String? type, 
      String? address, 
      String? addressDetail, 
      String? liter, 
      String? point, 
      String? transporter, 
      String? donasi, 
      String? time,}){
    _type = type;
    _address = address;
    _addressDetail = addressDetail;
    _liter = liter;
    _point = point;
    _transporter = transporter;
    _donasi = donasi;
    _time = time;
}

  Activity.fromJson(dynamic json) {
    _type = json['type'];
    _address = json['address'];
    _addressDetail = json['address_detail'];
    _liter = json['liter'];
    _point = json['point'];
    _transporter = json['transporter'];
    _donasi = json['donasi'];
    _time = json['time'];
  }
  String? _type;
  String? _address;
  String? _addressDetail;
  String? _liter;
  String? _point;
  String? _transporter;
  String? _donasi;
  String? _time;

  String? get type => _type;
  String? get address => _address;
  String? get addressDetail => _addressDetail;
  String? get liter => _liter;
  String? get point => _point;
  String? get transporter => _transporter;
  String? get donasi => _donasi;
  String? get time => _time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['address'] = _address;
    map['address_detail'] = _addressDetail;
    map['liter'] = _liter;
    map['point'] = _point;
    map['transporter'] = _transporter;
    map['donasi'] = _donasi;
    map['time'] = _time;
    return map;
  }

}