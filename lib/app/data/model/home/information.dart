/// title : "Tentang Kami"
/// data : [{"section":"Latar Belakang","content":[{"image":"https://drive.google.com/uc?id=1uQEpfrVxNJ8DQT6MtbLeDRUTqKccNPRR","description":"Sampah telah menjadi masalah tak terkendali yang sangat sulit untuk dipecahkan, tak terkecuali sampah minyak goreng bekas. Kami hadir dengan misi menciptakan ekosistem berbasis masyarakat untuk pengelolaan sampah minyak goreng bekas yang melibatkan mereka secara proaktif serta membuka mata pencaharian baru bagi masyarakat lainnya."}]}]

class Information {
  Information({
      String? title, 
      List<Data>? data,}){
    _title = title;
    _data = data;
}

  Information.fromJson(dynamic json) {
    _title = json['title'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  String? _title;
  List<Data>? _data;

  String? get title => _title;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = _title;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// section : "Latar Belakang"
/// content : [{"image":"https://drive.google.com/uc?id=1uQEpfrVxNJ8DQT6MtbLeDRUTqKccNPRR","description":"Sampah telah menjadi masalah tak terkendali yang sangat sulit untuk dipecahkan, tak terkecuali sampah minyak goreng bekas. Kami hadir dengan misi menciptakan ekosistem berbasis masyarakat untuk pengelolaan sampah minyak goreng bekas yang melibatkan mereka secara proaktif serta membuka mata pencaharian baru bagi masyarakat lainnya."}]

class Data {
  Data({
      String? section, 
      List<Content>? content,}){
    _section = section;
    _content = content;
}

  Data.fromJson(dynamic json) {
    _section = json['section'];
    if (json['content'] != null) {
      _content = [];
      json['content'].forEach((v) {
        _content?.add(Content.fromJson(v));
      });
    }
  }
  String? _section;
  List<Content>? _content;

  String? get section => _section;
  List<Content>? get content => _content;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['section'] = _section;
    if (_content != null) {
      map['content'] = _content?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// image : "https://drive.google.com/uc?id=1uQEpfrVxNJ8DQT6MtbLeDRUTqKccNPRR"
/// description : "Sampah telah menjadi masalah tak terkendali yang sangat sulit untuk dipecahkan, tak terkecuali sampah minyak goreng bekas. Kami hadir dengan misi menciptakan ekosistem berbasis masyarakat untuk pengelolaan sampah minyak goreng bekas yang melibatkan mereka secara proaktif serta membuka mata pencaharian baru bagi masyarakat lainnya."

class Content {
  Content({
      String? image, 
      String? description,}){
    _image = image;
    _description = description;
}

  Content.fromJson(dynamic json) {
    _image = json['image'];
    _description = json['description'];
  }
  String? _image;
  String? _description;

  String? get image => _image;
  String? get description => _description;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['image'] = _image;
    map['description'] = _description;
    return map;
  }

}