/// access : "NULL"
/// expire : "2021-08-19T20:57:16.67270056+07:00"
/// token : "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUp3WlhKdWNpSTZJakF3T1RrNU9UazVJaXdpYzI1aGJXVWlPaUpWYzJWeUlFMWhiblJ5YVNCQ1VrbENjbUZwYmlJc0luSnZiR1VpT2lKTlFVNVVVa2tpTENKa1pYTmpjbWx3ZEdsdmJsOHhJam9pU21GcllYSjBZU0lzSW1SbGMyTnlhWEIwYVc5dVh6SWlPaUl5SWl3aWIzSm5aV2hmZEhnaU9pSlZUa2xVSUUxQlRWQkJUa2NnVUZKQlVFRlVRVTRpTENKemRHVnNiRjkwZUNJNklrcFZUa2xQVWlCQlUxTlBRMGxCVkVVZ1RVRk9WRkpKSWl3aWNtZGtaWE5qSWpvaVMyRnVkRzl5SUZkcGJHRjVZV2dnUzJGdWRHOXlJRmRwYkdGNVlXZ2dTbUZyWVhKMFlTQXlJaXdpYldKa1pYTmpJam9pUzJGdWRHOXlJRU5oWW1GdVp5QkxZVzUwYjNJZ1EyRmlZVzVuSUVwaGEyRnlkR0VnVUdGellYSWdUV2x1WjJkMUlpd2lZbkprWlhOaklqb2lWVTVKVkNCQ1JVNUVRU0JTUVZsQklGQkJVMEZTSUUxSlRrZEhWU0lzSW5KbFoybHZiaUk2SWtraUxDSnRZV2x1WW5JaU9qTXpPU3dpWW5KaGJtTm9Jam80TXpRc0ltcGxibWx6WDJ0bGJHRnRhVzRpT2lKUUlpd2lZV05qWlhOelgyeGxkbVZzSWpvaVRsVk1UQ0lzSW1WNGNHbHlaV1JmZEc5clpXNGlPakUyTWprek9ERTBNellzSW1WNGNDSTZNVFl5T1RRMk5ESXpOaXdpYW5ScElqb2lOakF6TVRrME1HSXRNelF3WmkwMFl6QTFMV0UyTm1JdE9EQXdaVEZpWkRCbE56WmpJaXdpYVdGMElqb3hOakk1TXpjM09ETTJMQ0pwYzNNaU9pSkNVa2xDVWtGSlRpSjkuam5BQjUyZFNzTk9jRC1HLWpYeDdKZFhtZHpLeFhTVm9EVk5Kbmh4aGtTZw=="
/// user : {"address":"string","dob":"string","fcm_token":"string","full_name":"string","gender":0,"id_number":"string","id_type":0,"phone_number":"string","points":0,"profile_pict_url":"string","referral_code":"string","role":"string","user_email":"string","user_id":"string","verification_code":"string","verification_send_date":"string"}

class LoginResponse {
  LoginResponse({
    String? expire,
    String? token,
    User? user,
  }) {
    _expire = expire;
    _token = token;
    _user = user;
  }

  LoginResponse.fromJson(dynamic json) {
    _expire = json['expire'];
    _token = json['token'];
    _user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  String? _expire;
  String? _token;
  User? _user;

  String? get expire => _expire;

  String? get token => _token;

  User? get user => _user;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['expire'] = _expire;
    map['token'] = _token;
    if (_user != null) {
      map['user'] = _user?.toJson();
    }
    return map;
  }
}

/// address : "string"
/// dob : "string"
/// fcm_token : "string"
/// full_name : "string"
/// gender : 0
/// id_number : "string"
/// id_type : 0
/// phone_number : "string"
/// points : 0
/// profile_pict_url : "string"
/// referral_code : "string"
/// role : "string"
/// user_email : "string"
/// user_id : "string"
/// verification_code : "string"
/// verification_send_date : "string"

class User {
  User({
    String? address,
    String? dob,
    String? fcmToken,
    String? fullName,
    int? gender,
    String? idNumber,
    int? idType,
    String? phoneNumber,
    int? points,
    String? profilePictUrl,
    String? referralCode,
    String? role,
    String? userEmail,
    String? userId,
    String? verificationCode,
    String? verificationSendDate,
    bool? isFirstLogin,
  }) {
    _address = address;
    _dob = dob;
    _fcmToken = fcmToken;
    _fullName = fullName;
    _gender = gender;
    _idNumber = idNumber;
    _idType = idType;
    _phoneNumber = phoneNumber;
    _points = points;
    _profilePictUrl = profilePictUrl;
    _referralCode = referralCode;
    _role = role;
    _userEmail = userEmail;
    _userId = userId;
    _verificationCode = verificationCode;
    _verificationSendDate = verificationSendDate;
    _isFirstLogin = isFirstLogin;
  }

  User.fromJson(dynamic json) {
    _address = json['address'];
    _dob = json['dob'];
    _fcmToken = json['fcm_token'];
    _fullName = json['full_name'];
    _gender = json['gender'];
    _idNumber = json['id_number'];
    _idType = json['id_type'];
    _phoneNumber = json['phone_number'];
    _points = json['points'];
    _profilePictUrl = json['profile_pict_url'];
    _referralCode = json['referral_code'];
    _role = json['role'];
    _userEmail = json['user_email'];
    _userId = json['user_id'];
    _verificationCode = json['verification_code'];
    _verificationSendDate = json['verification_send_date'];
    _isFirstLogin = json['is_first_login'];
  }

  String? _address;
  String? _dob;
  String? _fcmToken;
  String? _fullName;
  int? _gender;
  String? _idNumber;
  int? _idType;
  String? _phoneNumber;
  int? _points;
  String? _profilePictUrl;
  String? _referralCode;
  String? _role;
  String? _userEmail;
  String? _userId;
  String? _verificationCode;
  String? _verificationSendDate;
  bool? _isFirstLogin;

  String? get address => _address;

  String? get dob => _dob;

  String? get fcmToken => _fcmToken;

  String? get fullName => _fullName;

  int? get gender => _gender;

  String? get idNumber => _idNumber;

  int? get idType => _idType;

  String? get phoneNumber => _phoneNumber;

  int? get points => _points;

  String? get profilePictUrl => _profilePictUrl;

  String? get referralCode => _referralCode;

  String? get role => _role;

  String? get userEmail => _userEmail;

  String? get userId => _userId;

  String? get verificationCode => _verificationCode;

  String? get verificationSendDate => _verificationSendDate;

  bool? get isFirstLogin => _isFirstLogin;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['address'] = _address;
    map['dob'] = _dob;
    map['fcm_token'] = _fcmToken;
    map['full_name'] = _fullName;
    map['gender'] = _gender;
    map['id_number'] = _idNumber;
    map['id_type'] = _idType;
    map['phone_number'] = _phoneNumber;
    map['points'] = _points;
    map['profile_pict_url'] = _profilePictUrl;
    map['referral_code'] = _referralCode;
    map['role'] = _role;
    map['user_email'] = _userEmail;
    map['user_id'] = _userId;
    map['verification_code'] = _verificationCode;
    map['verification_send_date'] = _verificationSendDate;
    map['is_first_login'] = _isFirstLogin;
    return map;
  }
}
