/// email : "mohradyatama24@gmail.com"
/// password : "YWRtaW4xMjM="
/// role : 1

class LoginRequest {
  LoginRequest({
      String? email, 
      String? password, 
      int? role,}){
    _email = email;
    _password = password;
    _role = role;
}

  LoginRequest.fromJson(dynamic json) {
    _email = json['email'];
    _password = json['password'];
    _role = json['role'];
  }
  String? _email;
  String? _password;
  int? _role;

  String? get email => _email;
  String? get password => _password;
  int? get role => _role;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['email'] = _email;
    map['password'] = _password;
    map['role'] = _role;
    return map;
  }

}