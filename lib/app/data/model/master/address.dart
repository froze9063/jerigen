/// address_desc : "string"
/// id : 0
/// latitude : 0
/// longitude : 0
/// user_id : "string"

class AddressList {
  AddressList({
    List<Address>? data,
    Paginator? paginator,}){
    _data = data;
    _paginator = paginator;
  }

  AddressList.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Address.fromJson(v));
      });
    }
    _paginator = json['paginator'] != null ? Paginator.fromJson(json['paginator']) : null;
  }
  List<Address>? _data;
  Paginator? _paginator;

  List<Address>? get data => _data;
  Paginator? get paginator => _paginator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    if (_paginator != null) {
      map['paginator'] = _paginator?.toJson();
    }
    return map;
  }

}

class Address {
  Address({
    String? addressDesc,
    int? id,
    double? latitude,
    double? longitude,
    String? userId,}){
    _addressDesc = addressDesc;
    _id = id;
    _latitude = latitude;
    _longitude = longitude;
    _userId = userId;
  }

  Address.fromJson(dynamic json) {
    _addressDesc = json['address_desc'];
    _id = json['id'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _userId = json['user_id'];
  }
  String? _addressDesc;
  int? _id;
  double? _latitude;
  double? _longitude;
  String? _userId;

  String? get addressDesc => _addressDesc;
  int? get id => _id;
  double? get latitude => _latitude;
  double? get longitude => _longitude;
  String? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['address_desc'] = _addressDesc;
    map['id'] = _id;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['user_id'] = _userId;
    return map;
  }

}

class Paginator {
  Paginator({
    int? backPage,
    int? currentPage,
    int? limitPerPage,
    int? nextPage,
    int? totalPages,
    int? totalRecords,}){
    _backPage = backPage;
    _currentPage = currentPage;
    _limitPerPage = limitPerPage;
    _nextPage = nextPage;
    _totalPages = totalPages;
    _totalRecords = totalRecords;
  }

  Paginator.fromJson(dynamic json) {
    _backPage = json['back_page'];
    _currentPage = json['current_page'];
    _limitPerPage = json['limit_per_page'];
    _nextPage = json['next_page'];
    _totalPages = json['total_pages'];
    _totalRecords = json['total_records'];
  }
  int? _backPage;
  int? _currentPage;
  int? _limitPerPage;
  int? _nextPage;
  int? _totalPages;
  int? _totalRecords;

  int? get backPage => _backPage;
  int? get currentPage => _currentPage;
  int? get limitPerPage => _limitPerPage;
  int? get nextPage => _nextPage;
  int? get totalPages => _totalPages;
  int? get totalRecords => _totalRecords;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['back_page'] = _backPage;
    map['current_page'] = _currentPage;
    map['limit_per_page'] = _limitPerPage;
    map['next_page'] = _nextPage;
    map['total_pages'] = _totalPages;
    map['total_records'] = _totalRecords;
    return map;
  }

}