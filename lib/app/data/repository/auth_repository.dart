// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/auth/login_request.dart';
import 'package:jerrycan_master/app/data/model/auth/login_response.dart';
import 'package:jerrycan_master/app/data/model/base_response.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/failure.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository {
  Future<LoginResponse> postLogin(LoginRequest body) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    Response response = await dio.post(PATH_LOGIN, data: body);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        var loginResponse = LoginResponse.fromJson(baseResponse.data);
        _saveLoginData(loginResponse);
        return loginResponse;
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<LoginResponse> refreshToken() async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Response response = await dio.post(PATH_REFRESH_TOKEN);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        var loginResponse = LoginResponse.fromJson(baseResponse.data);
        _saveLoginData(loginResponse);
        return loginResponse;
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<void> _saveLoginData(LoginResponse response) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(PREFS_LOGIN_DATA, jsonEncode(response));

    Singleton.loginResponse = response;
  }
}
