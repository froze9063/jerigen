// Created by ferdyhaspin on 23/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/base_response.dart';
import 'package:jerrycan_master/app/data/model/choose_location/choose_location_response.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/failure.dart';

class LocationRepository {
  Future<List<ChooseListLocation>> getListCity() async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    Response response = await dio.get(PATH_LIST_CITY);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return ChooseLocationResponse.fromJson(baseResponse.data).data ?? [];
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }
}
