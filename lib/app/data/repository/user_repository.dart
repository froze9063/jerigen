// Created by ferdyhaspin on 22/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/base_response.dart';
import 'package:jerrycan_master/app/data/model/register/register_request.dart';
import 'package:jerrycan_master/app/utils/constant.dart';
import 'package:jerrycan_master/app/utils/failure.dart';
import 'package:jerrycan_master/app/utils/singleton.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/auth/login_response.dart';

class UserRepository {
  Future<bool> postRegister(RegisterRequest body) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    Response response = await dio.post(PATH_CREATE_USER, data: body);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return true;
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<List<String>> postMedia(String folder, List<File> files) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());

    Map<String, dynamic> params = Map();
    params['folder'] = folder;
    for (var file in files) {
      params['image'] = await MultipartFile.fromFile(file.path);
    }

    var formData = FormData.fromMap(params);

    Response response = await dio.post(PATH_MEDIA_UPLOAD_FILE, data: formData);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return (baseResponse.data as List)
            .map((item) => item.toString())
            .toList();
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<bool> updateProfile(RegisterRequest body) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    var id = Singleton.loginResponse.user?.userId ?? "";
    Response response = await dio.put(PATH_UPDATE_USER + id, data: body);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        User user = User.fromJson(baseResponse.data);
        LoginResponse data = LoginResponse(
          expire: Singleton.loginResponse.token,
          token: Singleton.loginResponse.token,
          user: user,
        );
        await _saveLoginData(data);
        return true;
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<void> _saveLoginData(LoginResponse response) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(PREFS_LOGIN_DATA, jsonEncode(response));

    Singleton.loginResponse = response;
  }
}
