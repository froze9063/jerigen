// Created by ferdyhaspin on 25/01/22.
// Copyright (c) 2022 Jerigen All rights reserved.

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/master/address.dart';
import 'package:jerrycan_master/app/data/model/trx/account.dart';

import '../../utils/constant.dart';
import '../../utils/failure.dart';
import '../../utils/singleton.dart';
import '../model/base_response.dart';

class MasterRepository {
  Future<Account> accountDoCreate(
      String name, String account, String bank) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Map<String, dynamic> params = Map();
    params['account_name'] = name;
    params['account_no'] = account;
    params['bank_name'] = bank;
    Response response = await dio.post(PATH_BANK_ACCOUNT_CREATE, data: params);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return Account.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<Account> accountDoUpdate(
    int id,
    String name,
    String account,
    String bank,
  ) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";
    print("${Singleton.loginResponse.token}");
    Map<String, dynamic> params = Map();
    params['account_name'] = name;
    params['account_no'] = account;
    params['bank_name'] = bank;
    Response response =
        await dio.put(PATH_BANK_ACCOUNT_UPDATE + id.toString(), data: params);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return Account.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<List<Account>> getAccountList() async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Response response = await dio.get(PATH_BANK_ACCOUNT_LIST);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        var responseData = AccountList.fromJson(baseResponse.data).data;
        if (responseData == null) {
          throw RequestException(baseResponse.message);
        } else {
          return responseData;
        }
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<Address> createAddress(
    double lat,
    double long,
    String desc,
  ) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Map<String, dynamic> params = Map();
    params['latitude'] = lat;
    params['longitude'] = long;
    params['address_desc'] = desc;
    Response response = await dio.post(PATH_ADDRESS_CREATE, data: params);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return Address.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<Address> updateAddress(
    int id,
    double lat,
    double long,
    String desc,
  ) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";
    print("${Singleton.loginResponse.token}");

    Map<String, dynamic> params = Map();
    params['latitude'] = lat;
    params['longitude'] = long;
    params['address_desc'] = desc;
    Response response =
        await dio.put(PATH_ADDRESS_UPDATE + id.toString(), data: params);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return Address.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<List<Address>> getAddresses() async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Response response = await dio.get(PATH_ADDRESS_LIST);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        var responseData = AddressList.fromJson(baseResponse.data).data;
        if (responseData == null) {
          throw RequestException(baseResponse.message);
        } else {
          return responseData;
        }
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }
}
