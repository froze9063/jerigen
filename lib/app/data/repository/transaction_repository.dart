// Created by ferdyhaspin on 28/11/21.
// Copyright (c) 2021 Jerigen All rights reserved.

import 'package:dio/dio.dart';
import 'package:jerrycan_master/app/data/model/home/depo.dart';
import 'package:jerrycan_master/app/data/model/trx/create_trx_request.dart';
import 'package:jerrycan_master/app/data/model/trx/create_trx_response.dart';

import '../../utils/constant.dart';
import '../../utils/failure.dart';
import '../../utils/singleton.dart';
import '../model/base_response.dart';
import '../model/trx/transporter_response.dart';
import 'auth_repository.dart';

class TransactionRepository {
  final CancelToken _cancelToken = CancelToken();

  Future<List<TransporterResponse>> getTransporterList(
    String liter,
    String distance,
    String latitude,
    String longitude,
  ) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Map<String, dynamic> params = Map();
    params['liter'] = liter;
    params['distance'] = distance;
    params['latitude'] = latitude;
    params['longitude'] = longitude;

    Response response = await dio.get(
      PATH_LIST_TRANSPORTER,
      queryParameters: params,
      cancelToken: _cancelToken,
    );

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return (baseResponse.data as List)
            .map((item) => TransporterResponse.fromJson(item))
            .toList();
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<Depo> scanDepo(String code) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Response response = await dio.get(PATH_SCAN_DEPO + code);

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        return Depo.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  Future<CreateTrxResponse> createTrx(CreateTrxRequest body) async {
    Dio dio = Dio(ConstConfig.dioBaseOptions());
    dio.interceptors.add(ConstConfig.dioPrettyLog());
    dio.options.headers["Authorization"] = "${Singleton.loginResponse.token}";

    Response response = await dio.post(
      PATH_CREATE_TRX,
      data: body,
      cancelToken: _cancelToken,
    );

    if (response.statusCode == 200) {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      if (baseResponse.code == RESPONSE_OK) {
        AuthRepository().refreshToken();
        return CreateTrxResponse.fromJson(baseResponse.data);
      } else {
        throw RequestException(baseResponse.message);
      }
    } else {
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      throw RequestException(baseResponse.message);
    }
  }

  void cancelRequest() {
    _cancelToken.cancel('Cancel finding transporter');
  }
}
